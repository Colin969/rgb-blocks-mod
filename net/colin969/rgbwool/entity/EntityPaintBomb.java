package net.colin969.rgbwool.entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import net.colin969.rgbwool.RGBBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockHalfSlab;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.projectile.EntityPotion;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityPaintBomb extends EntityPotion{

	public int color = 16777215;
	
	public EntityPaintBomb(World par1World, EntityLiving par2EntityLiving,
			ItemStack par3ItemStack) {
		super(par1World, par2EntityLiving, par3ItemStack);
		if(par3ItemStack.getTagCompound() != null){
			this.color = par3ItemStack.getTagCompound().getInteger("color");
		}
	}
	
    protected void onImpact(MovingObjectPosition pos)
    {
        if (!this.worldObj.isRemote)
        {
        	int xMin = -3;
        	int xMax = 3;
        	int yMin = -3;
        	int yMax = 3;
        	int zMin = -3;
        	int zMax = 3;
        	int posX1;
        	int posY1;
        	int posZ1;
        	
        	switch(pos.sideHit){
        	case 0:
        		pos.blockY -= 1;
        		break;
        	case 1:
        		pos.blockY += 1;
        		break;
        	case 2:
        		pos.blockX -= 1;
        		break;
        	case 3:
        		pos.blockX += 1;
        		break;
        	case 4:
        		pos.blockZ -= 1;
        		break;
        	case 5:
        		pos.blockZ += 1;
        	}
        	
        	if(pos.typeOfHit == pos.typeOfHit.ENTITY){
        		Entity entity = pos.entityHit;
        		posX1 = (int) (Math.floor(entity.posX));
        		posY1 = (int) (Math.floor(entity.posY));
        		posZ1 = (int) (Math.floor(entity.posZ));
        	} else {
        		posX1 = pos.blockX;
        		posY1 = pos.blockY;
        		posZ1 = pos.blockZ;
        	}
        	
        	for(int i = -1; i > -4;i--){
        		if(this.worldObj.getBlockId(pos.blockX + i, pos.blockY, pos.blockZ) != 0){
        			xMin = i;
        			break;
        		}
        	}
        	for(int i = -1; i > -4;i--){
        		if(this.worldObj.getBlockId(pos.blockX, pos.blockY + i, pos.blockZ) != 0){
        			yMin = i;
        			break;
        		}
        	}
        	for(int i = -1; i > -4;i--){
        		if(this.worldObj.getBlockId(pos.blockX, pos.blockY, pos.blockZ + i) != 0){
        			zMin = i;
        			break;
        		}
        	}
        	for(int i = 1; i < 4;i++){
        		if(this.worldObj.getBlockId(pos.blockX + i, pos.blockY, pos.blockZ) != 0){
        			xMax = i;
        			break;
        		}
        	}
        	for(int i = 1; i < 4;i++){
        		if(this.worldObj.getBlockId(pos.blockX, pos.blockY + i, pos.blockZ) != 0){
        			yMax = i;
        			break;
        		}
        	}
        	for(int i = 1; i < 4;i++){
        		if(this.worldObj.getBlockId(pos.blockX, pos.blockY, pos.blockZ + i) != 0){
        			zMax = i;
        			break;
        		}
        	}
        	for(int x = -3; x < 4; x++){
        		for(int y = -3; y < 4; y++){
        			for(int z = -3; z < 4; z++){
        	        	int posX = posX1 + x;
        	        	int posY = posY1 + y;
        	        	int posZ = posZ1 + z;
        				if(coordsValid(xMin, xMax, x) && coordsValid(yMin, yMax, y) && coordsValid(zMin, zMax, z) && comboValid(x,y,z) && !worldObj.isAirBlock(posX, posY, posZ)){
        					int blockId = worldObj.getBlockId(posX, posY, posZ);
        					int blockMetadata = worldObj.getBlockMetadata(posX, posY, posZ);
        					if(blockId == RGBBlock.RGBBlockID){
            					TileEntityRGB te = (TileEntityRGB) worldObj.getBlockTileEntity(posX, posY, posZ);
            						if(te != null){
            							for(int i = 0; i < 6; i++)
            							te.setSideColor(i, color);
            						}
            					worldObj.markBlockForUpdate(posX, posY, posZ);
        					} else {

        					if(isBlockValid(blockId, blockMetadata)){
        						worldObj.setBlock(posX, posY, posZ, RGBBlock.RGBBlockID);
        						TileEntityRGB te = (TileEntityRGB) worldObj.getBlockTileEntity(posX, posY, posZ);
        						if(te != null){
        							te.blockMetadata = blockMetadata;
        							te.blockId = blockId;
        							for(int i = 0; i < 6; i++)
        							te.setSideColor(i, color);
        						}
        						worldObj.markBlockForUpdate(posX, posY, posZ);
        					}
        					}
        				}
        			}
        		}
        	}
        	Random rand = new Random();
        	this.worldObj.playAuxSFX(2002, (int)Math.round(this.posX), (int)Math.round(this.posY), (int)Math.round(this.posZ), this.getPotionDamage());
        	this.setDead();
        }
    }
    
    private boolean comboValid(int x, int y, int z) {
    	
    	if(x < 0){
    		x -= x * 2;
    	}
    	if(y < 0){
    		y -= y * 2;
    	}
    	if(z < 0){
    		z -= z * 2;
    	}
    	
    	if(x + y + z <= 5)
    		return true;
    	return false;
	}

	public boolean coordsValid(int min, int max, int num) {
    	if(num >= min && num <= max)
    		return true;
		return false;
	}

	public boolean isSlab(int blockId) {
        if (Block.blocksList[blockId] instanceof BlockHalfSlab){
        	if(!Block.blocksList[blockId].renderAsNormalBlock())
        		return true;
        	else return false;
        }
        return false;
    }
    
    public boolean isBlockValid(int blockId, int blockMetadata){
    	Block block = Block.blocksList[blockId];
    	if (blockId == RGBBlock.RGBBlockID)
    		return false;
    	if (blockId != RGBBlock.RGBBlockID && blockId != Block.glass.blockID) {
    		if (!isSlab(blockId) && (block.renderAsNormalBlock() == false || block.hasTileEntity(blockMetadata)))
            	return false;
    	}
    	return true;
    }
    
}
