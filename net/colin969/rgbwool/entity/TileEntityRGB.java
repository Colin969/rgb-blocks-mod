package net.colin969.rgbwool.entity;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.ForgeDirection;

public class TileEntityRGB extends TileEntity {
    public int Top;
    public int Bottom;
    public int North;
    public int South;
    public int East;
    public int West;
    public int blockId;
    String Hex = "FFFFFF";

    public TileEntityRGB() {
        Top = -1;
        Bottom = -1;
        North = -1;
        South = -1;
        East = -1;
        West = -1;
        blockId = 0;
        blockMetadata = 0;
    }

    @Override
    public void readFromNBT(NBTTagCompound NBTcompound) {
        super.readFromNBT(NBTcompound);
        Top = NBTcompound.getInteger("Top");
        Bottom = NBTcompound.getInteger("Bottom");
        North = NBTcompound.getInteger("North");
        South = NBTcompound.getInteger("South");
        East = NBTcompound.getInteger("East");
        West = NBTcompound.getInteger("West");
        blockId = NBTcompound.getInteger("blockId");
        blockMetadata = NBTcompound.getInteger("blockMetadata");

    }

    @Override
    public void writeToNBT(NBTTagCompound NBTcompound) {
        super.writeToNBT(NBTcompound);
        NBTcompound.setInteger("Top", Top);
        NBTcompound.setInteger("Bottom", Bottom);
        NBTcompound.setInteger("North", North);
        NBTcompound.setInteger("South", South);
        NBTcompound.setInteger("East", East);
        NBTcompound.setInteger("West", West);
        NBTcompound.setInteger("blockId", blockId);
        NBTcompound.setInteger("blockMetadata", blockMetadata);
    }

    public void updateEntity(int BlockId, int BlockMetadata, int LightValue) {
        blockId = BlockId;
        blockMetadata = BlockMetadata;
        updateEntity();
    }

    public String getTwoCharHex(int num) {
        if (num < 16) {
            return "0" + Integer.toHexString(num);
        } else {
            return Integer.toHexString(num);
        }
    }

    public int getSideColor(int Side) {
        int color = -1;

        switch (Side) {
        case 1:
            color = Top;
            break;
        case 0:
            color = Bottom;
            break;
        case 4:
            color = North;
            break;
        case 5:
            color = South;
            break;
        case 2:
            color = East;
            break;
        case 3:
            color = West;
        }
        return color;
    }

    public void setSideColor(int Side, int color) {
        switch (Side) {
        case 1:
            Top = color;
            break;
        case 0:
            Bottom = color;
            break;
        case 4:
            North = color;
            break;
        case 5:
            South = color;
            break;
        case 2:
            East = color;
            break;
        case 3:
            West = color;
            break;
        }
    }

    @Override
    public Packet getDescriptionPacket() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(8);
        DataOutputStream os = new DataOutputStream(bos);
        try {
            os.writeInt(xCoord);
            os.writeInt(yCoord);
            os.writeInt(zCoord);
            os.writeInt(Top);
            os.writeInt(Bottom);
            os.writeInt(North);
            os.writeInt(South);
            os.writeInt(East);
            os.writeInt(West);
            os.writeInt(blockId);
            os.writeInt(blockMetadata);
            // os.writeInt(lightValue);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Packet250CustomPayload packet = new Packet250CustomPayload();
        packet.channel = "WoolDescription";
        packet.data = bos.toByteArray();
        packet.length = bos.size();
        return packet;
    }

    public boolean renderTop() {
        if (blockId == 44 || blockId == 126) {
            if ((blockMetadata & 8) != 0) {
                return true;
            }
        }
        return false;
    }

    public boolean canPlace(ForgeDirection side) {
        if (blockId == 44 || blockId == 126) {
            if (side == ForgeDirection.NORTH || side == ForgeDirection.SOUTH || side == ForgeDirection.EAST || side == ForgeDirection.WEST) {
                return false;
            }
            if (side == ForgeDirection.UP) {
                if ((blockMetadata & 8) != 0) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public boolean isBlockValid() {
        if (blockId > 0 && blockId <= Block.blocksList.length && Block.blocksList[blockId] != null) {
            return this.blockMetadata >= 0;
        }
        return false;
    }
}
