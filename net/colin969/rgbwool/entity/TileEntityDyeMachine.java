package net.colin969.rgbwool.entity;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.colin969.rgbwool.Logger;
import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.container.ContainerDyeMachine;
import net.colin969.rgbwool.item.ItemBlockInjector;
import net.colin969.rgbwool.item.ItemRGBBlock;
import net.colin969.rgbwool.item.ItemPaintBomb;
import net.minecraft.item.ItemCloth;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemGlassBottle;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.NetHandler;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.ILiquidTank;
import net.minecraftforge.liquids.ITankContainer;
import net.minecraftforge.liquids.LiquidStack;
import net.minecraftforge.liquids.LiquidTank;

public class TileEntityDyeMachine extends TileEntity implements ISidedInventory, ITankContainer{

        protected ItemStack[] inv;
        protected LiquidTank liquidTank;
        protected int transferTicks;
        protected int fuelTransferTicks;
        protected boolean inFuelTransfer;
        protected boolean inTransfer;
        protected static final int maxTicks = 16;
        protected static final int maxFactoryTicks = 4;
        protected int[] inSlots = {0};
        protected int[] fuelSlots = {2};
        protected int[] outSlots = {1};
    	public int color;
    	public boolean FactoryMode;
		protected boolean[] sides = new boolean[6];

        public TileEntityDyeMachine(){
        		for(int i = 0; i < 6;i++){
        			sides[i] = false;
        		}
        		if(this.blockMetadata == 8)
        			FactoryMode = true;
        		else
        			FactoryMode = false;
        		inv = new ItemStack[3];
                transferTicks = 0;
                fuelTransferTicks = 0;
                inTransfer = false;
                inFuelTransfer = false;
                this.color = 0;
                if(!FactoryMode){
	                liquidTank = new LiquidTank(8000);
	                liquidTank.setTankPressure(-1);
                }else{
	                liquidTank = new LiquidTank(2000);
	                liquidTank.setTankPressure(1);
                }
        }
        
        @Override
        public Packet getDescriptionPacket()
        {
        	NBTTagCompound tag = new NBTTagCompound();
        	writeToNBT(tag);
            return new Packet132TileEntityData(this.xCoord, this.yCoord, this.zCoord, 123, tag);
        }
        
        @Override
        public void onDataPacket(INetworkManager net ,Packet132TileEntityData packet){
        	NBTTagCompound tag = packet.customParam1;
        	if(worldObj.isRemote){
        		if(tag.getInteger("color") != color)
        			this.worldObj.markBlockForRenderUpdate(xCoord, yCoord, zCoord);
        	}
        	readFromNBT(tag);
        }
        
        public int getTranferTicks(){
        	return transferTicks;
        }
        
        public void setTransferTicks(int i){
        	transferTicks = i;
        }
        
        public void setInTransfer(boolean fuel, boolean state){
        	if(fuel)
        		this.inFuelTransfer = state;
        	else
        		this.inTransfer = state;
        }
        
        public void updateTransfer(boolean fuel){
        	if(fuel && this.inFuelTransfer)
        		fuelTransferTicks += 1;
        	else if (!fuel && this.inTransfer)
        		transferTicks += 1;
        }
        
        public boolean canTransfer(boolean factory, boolean fuel){
        	int l;
        	if(factory)
        		l = this.maxFactoryTicks;
        	else
        		l = this.maxTicks;
        	
        	if(factory){
        		return this.fuelTransferTicks >= l ? true : false;
        	} else {
        		return this.transferTicks >= l ? true : false;
        	}
        	
        }
        
        public void updateEntity(){	
    		ItemStack inputStack = inv[0];
    		ItemStack outputStack = inv[1];
    		ItemStack fuelStack = inv[2];
    		setInTransfer(false,false);
    		setInTransfer(true,false);
    		if(!FactoryMode){
	    		if(inputStack != null){
	    			if (isItemValid(inputStack.itemID, inputStack.getItemDamage())){
	    				if(outputStack == null && this.getDye() >= 12)
	    					setInTransfer(false,true);
	    				else if (outputStack != null) {
	    					NBTTagCompound nbt = outputStack.getTagCompound();
	    					if(nbt.getInteger("Color") == this.color && nbt.getInteger("blockId") == inputStack.itemID && nbt.getInteger("blockMetadata") == inputStack.getItemDamage() && this.getDye() >= 12)
	    						setInTransfer(false,true);
	    				}
	    			} else if(inputStack.getItem() instanceof ItemRGBBlock){
	    				NBTTagCompound nbt = inputStack.getTagCompound();
	    				
	    				if(outputStack == null){
	    					if(getDye() >= RGBBlock.dyeValue && nbt.getInteger("Color") != this.color)
	    						setInTransfer(false,true);
	    				} else if(outputStack.getItem() instanceof ItemRGBBlock){
	    					NBTTagCompound nbt2 = outputStack.getTagCompound();
	    					if(nbt2.getInteger("Color") == this.color && nbt.getInteger("Color") != this.color && nbt.getInteger("blockId") == nbt2.getInteger("blockId") && nbt.getInteger("blockMetadata") == nbt2.getInteger("blockMetadata") && getDye() >= 12){
	    						setInTransfer(false,true);
	    					}
	    				}
	    			} else if(inputStack.getItem() instanceof ItemGlassBottle){
	    				if(outputStack == null){
	    					if(getDye() >= 160)
	    						setInTransfer(false,true);
	    				} else if(outputStack.getItem() instanceof ItemPaintBomb)
	    					if(getDye() >= 160)
	    					if (outputStack.stackSize < 16)
								if(outputStack.getTagCompound().getInteger("color") == color)
									setInTransfer(false,true);
	    			} else if(inputStack.getItem() instanceof ItemPaintBomb){
	    				if(outputStack == null){
	    					if(inputStack.getTagCompound().getInteger("color") != color)
	    						setInTransfer(false,true);
	    				} else if (outputStack.stackSize < 16){
	    					if(outputStack.getItem() instanceof ItemPaintBomb){
	    						if(outputStack.getTagCompound().getInteger("color") == color)
	    							if(inputStack.getTagCompound().getInteger("color") != color)
	    								setInTransfer(false,true);
	    					}
	    				}
	    			}
	    		}
	    			
	    		this.updateTransfer(false);
	    		
	    		if(this.canTransfer(false, false) && inputStack != null){
	    			this.transferTicks = 0;
	    			if (isItemValid(inputStack.itemID, inputStack.getItemDamage())) {
	    				boolean flag = false;
	    				if(outputStack == null)
	    					flag = true;
	    				if(!flag)
	    					if(outputStack.getItem() instanceof ItemRGBBlock){
	    						NBTTagCompound nbt = outputStack.getTagCompound();
	    						if(nbt.getInteger("Color") == this.color && nbt.getInteger("blockId") == inputStack.itemID && nbt.getInteger("blockMetadata") == inputStack.getItemDamage())
	    							flag = true;
	    					}
	    				if(flag){
	    					if(outputStack == null){
	    						this.delDye(6);
	    						outputStack = new ItemStack(RGBBlock.BlockRGBItem, 1);
	    						NBTTagCompound nbt = new NBTTagCompound();
	    						nbt.setInteger("Color", this.color);
	    						nbt.setInteger("blockId", inputStack.itemID);
	    						nbt.setInteger("blockMetadata", inputStack.getItemDamage());
	    						outputStack.setTagCompound(nbt);
	    						this.setInventorySlotContents(1, outputStack);
	    						this.decrStackSize(0, 1);
	    					}
	    					else if(outputStack.stackSize != 64){
	    						this.delDye(6);
	    						outputStack.stackSize++;
	    						this.setInventorySlotContents(1, outputStack);
	    						this.decrStackSize(0, 1);
	    					}
	    				}
	    			} else if(inputStack.getItem() instanceof ItemRGBBlock){
	    				NBTTagCompound nbt;
	    				
	    				if(outputStack == null){
	    					this.delDye(12);
	    					outputStack = inputStack.copy();
	    					outputStack.stackSize = 1;
	    					nbt = outputStack.getTagCompound();
	    					nbt.setInteger("Color", this.color);
	    					outputStack.setTagCompound(nbt);
							this.setInventorySlotContents(1, outputStack);
							this.decrStackSize(0, 1);
	    				} else if(outputStack.getItem() instanceof ItemRGBBlock){
	    					nbt = inputStack.getTagCompound();
	    					NBTTagCompound nbt2 = outputStack.getTagCompound();
	    					if(nbt2.getInteger("Color") == this.color && nbt.getInteger("blockId") == nbt2.getInteger("blockId") && nbt.getInteger("blockMetadata") == nbt2.getInteger("blockMetadata")){
	    						this.delDye(12);
								outputStack.stackSize++;
								this.setInventorySlotContents(1, outputStack);
								this.decrStackSize(0, 1);
	    					}
	    				}
	    			} else if(inputStack.getItem() instanceof ItemGlassBottle){
	    				if(outputStack == null){
	    					if(getDye() >= 160){
	    						this.delDye(160);
	    						outputStack = new ItemStack(RGBBlock.PaintBomb);
	    						NBTTagCompound nbt = new NBTTagCompound();
	    						nbt.setInteger("color", color);
	    						outputStack.setTagCompound(nbt);
	    						this.setInventorySlotContents(1, outputStack);
	    						this.decrStackSize(0, 1);
	    					}
	    				} else if (outputStack.stackSize < 16){
	    					if(getDye() >= 160){
	    						this.delDye(160);
	    						outputStack.stackSize++;
	    						this.setInventorySlotContents(1, outputStack);
	    						this.decrStackSize(0, 1);
	    					}
	    				}
	    			} else if(inputStack.getItem() instanceof ItemPaintBomb){
	    				if(outputStack == null){
							outputStack = new ItemStack(RGBBlock.PaintBomb);
							NBTTagCompound nbt = new NBTTagCompound();
							nbt.setInteger("color", color);
							outputStack.setTagCompound(nbt);
							this.setInventorySlotContents(1, outputStack);
							this.decrStackSize(0, 1);
	    				} else if (outputStack.stackSize < 16){
	    					outputStack.stackSize++;
	    					this.setInventorySlotContents(1, outputStack);
	    					this.decrStackSize(0, 1);
	    				}
	    			}
	    		}
	    		
	    		if(fuelStack != null){
	    			
	    			if(fuelStack.getItem() instanceof ItemDye && !this.isDyeFull(1)){
	    				setInTransfer(true,true);
	    			}
	    			else if(fuelStack.getItem() instanceof ItemCloth && fuelStack.getItemDamage() != 0 && !this.isDyeFull(1)){
						boolean flag = false;
						if(outputStack == null)
							flag = true;
						if(!flag)
							if(outputStack.getItem() instanceof ItemCloth)
								if(outputStack.getItemDamage() == 0)
									flag = true;
						if(flag){
							if(outputStack == null){
								setInTransfer(true,true);
							}
							else if(outputStack.stackSize != 64){
								setInTransfer(true,true);
							}
						}
					} else if(fuelStack.getItem() instanceof ItemRGBBlock && !this.isDyeFull(1)){
	    				NBTTagCompound nbt;
	    				
	    				if(outputStack == null){
	    					setInTransfer(true,true);
	    				} else {
	    					nbt = fuelStack.getTagCompound();
	    					ItemStack newStack = new ItemStack(nbt.getInteger("blockId"), 1, nbt.getInteger("blockMetadata"));
	    					if(outputStack.getItem() == newStack.getItem()){
	    						setInTransfer(true,true);
	    					}
	    				}
					} else if(fuelStack.getItem() instanceof ItemPaintBomb){
	    				NBTTagCompound nbt;
	    				
	    				if(outputStack == null){
	    					setInTransfer(true,true);
	    				} else {
	    					if(outputStack.getItem() instanceof ItemGlassBottle){
	    						setInTransfer(true,true);
	    					}
	    				}
					}
	    				
	    			updateTransfer(true);
	    				
	    			if(canTransfer(false, true)){
	    				this.worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
	    				this.fuelTransferTicks = 0;
		    			if(fuelStack.getItem() instanceof ItemDye && !this.isDyeFull(1)){
							this.addDye(1);
							this.decrStackSize(2, 1);
		    			}
		    			else if(fuelStack.getItem() instanceof ItemCloth && fuelStack.getItemDamage() != 0 && !this.isDyeFull(1)){
							boolean flag = false;
							if(outputStack == null)
								flag = true;
							if(!flag)
								if(outputStack.getItem() instanceof ItemCloth)
									if(outputStack.getItemDamage() == 0)
										flag = true;
							if(flag){
								if(outputStack == null){
									this.addDye(2,6);
									outputStack = new ItemStack(Block.cloth, 1);
									this.setInventorySlotContents(1, outputStack);
									this.decrStackSize(2, 1);
								}
								else if(outputStack.stackSize != 64){
									this.addDye(2,6);
									outputStack.stackSize++;
									this.setInventorySlotContents(1, outputStack);
									this.decrStackSize(2, 1);
								}
							}
						} else if(fuelStack.getItem() instanceof ItemRGBBlock && !this.isDyeFull(1)){
		    				NBTTagCompound nbt;
		    				
		    				if(outputStack == null){
		    					this.addDye(2,6);
		    					nbt = fuelStack.getTagCompound();
		    					outputStack = new ItemStack(nbt.getInteger("blockId"), 1, nbt.getInteger("blockMetadata"));
		    					outputStack.setTagCompound(nbt);
								this.setInventorySlotContents(1, outputStack);
								this.decrStackSize(2, 1);
		    				} else {
		    					nbt = fuelStack.getTagCompound();
		    					ItemStack newStack = new ItemStack(nbt.getInteger("blockId"), 1, nbt.getInteger("blockMetadata"));
		    					if(outputStack.getItem() == newStack.getItem()){
		    						this.decrStackSize(2, 1);
		    						this.addDye(2,6);
									outputStack.stackSize++;
									this.setInventorySlotContents(1, outputStack);
		    					}
		    				}
						} else if(fuelStack.getItem() instanceof ItemPaintBomb){
		    				NBTTagCompound nbt;
		    				
		    				if(outputStack == null){
		    					this.addDye(2,80);
		    					outputStack = new ItemStack(Item.glassBottle);
								this.setInventorySlotContents(1, outputStack);
								this.decrStackSize(2, 1);
		    				} else {
		    					if(outputStack.getItem() instanceof ItemGlassBottle){
		    						this.decrStackSize(2, 1);
		    						this.addDye(2,80);
									outputStack.stackSize++;
									this.setInventorySlotContents(1, outputStack);
		    					}
		    				}
						}
	    			}
	    		}
	    		
	    		if(inputStack != null){
	    			if(inputStack.getItem() instanceof ItemBlockInjector){
	    				int itemDamage = inputStack.getItemDamage();
	    				if(itemDamage != 0 && this.getDye() != 0){
	    					if(this.getDye() == 1)
	    						inputStack.setItemDamage(itemDamage - 1); 
	    					else
	    						inputStack.setItemDamage(itemDamage - 2);
	    					this.delDye(2);
	    					inv[1] = outputStack;
	    				}
	    			}
	    		}
    		} else {
    			if(fuelStack != null){
    				this.setInTransfer(true, true);
    				
    				updateTransfer(true);
    				
    				if(canTransfer(true,true)){
	    				this.addDye(1);
	    				decrStackSize(2, 1);
    				}
    			}
    		}
        }
        
        
        private boolean isItemValid(int itemID, int metadata) {
        	Block block = null;
        	if(itemID < Block.blocksList.length)
        		block = Block.blocksList[itemID];
        	ItemStack outputStack = this.getStackInSlot(1);
        	boolean flag = false;
        	if(outputStack == null)
        		flag = true;
        	if(!flag)
        		if(outputStack.getItem() instanceof ItemRGBBlock)
        			flag = true;
        	if(block != null && flag){
        		if(block.renderAsNormalBlock() && !block.hasTileEntity(metadata) &&  !block.getLocalizedName().startsWith("tile."))
        			return true;
        	}
			return false;
		}

		@Override
        public int getSizeInventory() {
                return inv.length;
        }

        @Override
        public ItemStack getStackInSlot(int slot) {
        		return inv[slot];
        }

        @Override
        public void setInventorySlotContents(int slot, ItemStack stack) {
        		if(slot < 3)
        			inv[slot] = stack;
                if (stack != null && stack.stackSize > getInventoryStackLimit()) {
                        stack.stackSize = getInventoryStackLimit();
                }
                this.onInventoryChanged();
        }

        @Override
        public ItemStack decrStackSize(int slot, int amt) {
                ItemStack stack = getStackInSlot(slot);
                if (stack != null) {
                        if (stack.stackSize <= amt) {
                                setInventorySlotContents(slot, null);
                        } else {
                        		stack.stackSize -= amt;
                                this.setInventorySlotContents(slot, stack);
                        }
                }
                return stack;
        }

        @Override
        public ItemStack getStackInSlotOnClosing(int slot) {
                ItemStack stack = getStackInSlot(slot);
                if (stack != null) {
                       setInventorySlotContents(slot, null);
                }
                return stack;
        }

        @Override
        public int getInventoryStackLimit() {
                return 64;
        }

        @Override
        public boolean isUseableByPlayer(EntityPlayer player) {
                return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) == this &&
                player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
        }

        @Override
        public void openChest() {}

        @Override
        public void closeChest() {}

        @Override
        public void readFromNBT(NBTTagCompound tagCompound) {
                super.readFromNBT(tagCompound);

                NBTTagList tagList = tagCompound.getTagList("Inventory");
                for (int i = 0; i < tagList.tagCount(); i++) {
                        NBTTagCompound tag = (NBTTagCompound) tagList.tagAt(i);
                        byte slot = tag.getByte("Slot");
                        if (slot >= 0 && slot < inv.length) {
                                inv[slot] = ItemStack.loadItemStackFromNBT(tag);
                        }
                }
                color = tagCompound.getInteger("color");
                liquidTank.setLiquid(LiquidStack.loadLiquidStackFromNBT(tagCompound.getCompoundTag("tank")));
                FactoryMode = tagCompound.getBoolean("factory");
                for(int i = 0; i < 6; i++){
                	sides[i] = tagCompound.getBoolean("side" + String.valueOf(i));
                }
                this.readExtraFromNBT(tagCompound);
        }
        
        public void readExtraFromNBT(NBTTagCompound tagCompound){};
        public void writeExtraToNBT(NBTTagCompound tagCompound){};
        
        public void triggerUpdate(){
        	worldObj.updateTileEntityChunkAndDoNothing(xCoord, yCoord, zCoord, this);
        }

        @Override
        public void writeToNBT(NBTTagCompound tagCompound) {
                super.writeToNBT(tagCompound);

                NBTTagList itemList = new NBTTagList();
                for (int i = 0; i < inv.length; i++) {
                        ItemStack stack = inv[i];
                        if (stack != null) {
                                NBTTagCompound tag = new NBTTagCompound();
                                tag.setByte("Slot", (byte) i);
                                stack.writeToNBT(tag);
                                itemList.appendTag(tag);
                        }
                }
                tagCompound.setTag("Inventory", itemList);
                tagCompound.setInteger("color", color);
                if(liquidTank.getLiquid() != null)
                	tagCompound.setCompoundTag("tank", liquidTank.getLiquid().writeToNBT(new NBTTagCompound()));
                tagCompound.setBoolean("factory", FactoryMode);
                for(int i = 0; i < 6; i++){
                	tagCompound.setBoolean("side" + String.valueOf(i), sides[i]);
                }
                writeExtraToNBT(tagCompound);
        }

                @Override
                public String getInvName() {
                        return "net.colin969.RGBBlock.dyemachine";
                }

				@Override
				public int[] getAccessibleSlotsFromSide(int var1) {
					if(FactoryMode)
						return fuelSlots;
					if(var1 == 1) return inSlots; else if(var1 == 0) return fuelSlots; else return outSlots;
				}
				

				@Override
				public boolean canInsertItem(int i, ItemStack itemstack, int j) {
					return true;
				}

				@Override
				public boolean canExtractItem(int i, ItemStack itemstack, int j) {
					if(!FactoryMode)
						return true;
					return false;
				}

				@Override
				public boolean isInvNameLocalized() {
					return false;
				}

				public int getDye() {
					if(liquidTank.getLiquid() != null)
						return liquidTank.getLiquid().amount;
					else
						return 0;
				}

				public boolean isDyeFull(int i) {
					if(getDye() > getMaxDye() - (i * RGBBlock.dyeValue))
						return true;
					return false;
				}

				public void addDye(int i) {
					if(liquidTank.getLiquid() != null)
						liquidTank.getLiquid().amount += (i * RGBBlock.dyeValue);
					else
						liquidTank.setLiquid(new LiquidStack(RGBBlock.LiquidDyeStill, i * RGBBlock.dyeValue));
				}
				
				public void addDye(int i, int j) {
					if(liquidTank.getLiquid() != null)
						liquidTank.getLiquid().amount += (i * j);
					else
						liquidTank.setLiquid(new LiquidStack(RGBBlock.LiquidDyeStill, i * j));
				}

				@Override
				public boolean isStackValidForSlot(int i, ItemStack itemstack) {
					return true;
				}

				public void delDye(int i) {
					if(getDye() < i)
						liquidTank.setLiquid(null);
					else
						liquidTank.getLiquid().amount -= i;
				}

				public double getMaxDye() {
					return liquidTank.getCapacity();
				}

				public double getMaxTicks() {
					return maxTicks;
				}

				@Override
				public int fill(ForgeDirection from, LiquidStack resource,
						boolean doFill) {
					if(resource.itemID == RGBBlock.LiquidDyeFlowingID || resource.itemID == RGBBlock.LiquidDyeStillID){
						if(!worldObj.isRemote)
							this.worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
						else
							this.worldObj.markBlockForRenderUpdate(xCoord, yCoord, zCoord);
						return liquidTank.fill(resource, doFill);
					}
					return 0;
				}

				@Override
				public int fill(int tankIndex, LiquidStack resource,
						boolean doFill) {
					if(tankIndex == 0 && resource.itemID == RGBBlock.LiquidDyeFlowingID || resource.itemID == RGBBlock.LiquidDyeStillID){
						if(!worldObj.isRemote)
							this.worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
						else
							this.worldObj.markBlockForRenderUpdate(xCoord, yCoord, zCoord);
						return liquidTank.fill(resource, doFill);
					}else
						return 0;
				}

				@Override
				public LiquidStack drain(ForgeDirection from, int maxDrain,
						boolean doDrain) {
						if(!worldObj.isRemote)
							this.worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
						else
							this.worldObj.markBlockForRenderUpdate(xCoord, yCoord, zCoord);
						return liquidTank.drain(maxDrain, doDrain);	
				}

				@Override
				public LiquidStack drain(int tankIndex, int maxDrain,
						boolean doDrain) {
					if(tankIndex == 0){
						if(!worldObj.isRemote)
							this.worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
						else
							this.worldObj.markBlockForRenderUpdate(xCoord, yCoord, zCoord);
						return liquidTank.drain(maxDrain, doDrain);
					}else
						return null;
				}

				@Override
				public ILiquidTank[] getTanks(ForgeDirection direction) {
					
					ILiquidTank[] tanks = {getTank(direction,null)};
					return tanks;
				}

				@Override
				public ILiquidTank getTank(ForgeDirection direction,
						LiquidStack type) {
						return liquidTank;
				}
				
				public void updateConnectedSides(){
					for(int i = 0; i < 6; i++){
						sides[i] = this.isSideContainer(i);
					}
					this.worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
				}

				public boolean[] getConnectedSides() {
					return sides;
				}

				private boolean isSideContainer(int i) {
					TileEntity te;
					
					switch(i){
					case 0:
						te = this.worldObj.getBlockTileEntity(xCoord, yCoord - 1, zCoord);
						if(te != null){
							if(te instanceof ITankContainer || te instanceof ISidedInventory)
								return true;
							return false;
						} else { return false; }
					case 1:
						te = this.worldObj.getBlockTileEntity(xCoord, yCoord + 1, zCoord);
						if(te != null){
							if(te instanceof ITankContainer || te instanceof ISidedInventory)
								return true;
							return false;
						} else { return false; }
					case 2:
						te = this.worldObj.getBlockTileEntity(xCoord + 1, yCoord, zCoord);
						if(te != null){
							if(te instanceof ITankContainer || te instanceof ISidedInventory)
								return true;
							return false;
						} else { return false; }
					case 3:
						te = this.worldObj.getBlockTileEntity(xCoord - 1, yCoord, zCoord);
						if(te != null){
							if(te instanceof ITankContainer || te instanceof ISidedInventory)
								return true;
							return false;
						} else { return false; }
					case 4:
						te = this.worldObj.getBlockTileEntity(xCoord, yCoord, zCoord + 1);
						if(te != null){
							if(te instanceof ITankContainer || te instanceof ISidedInventory)
								return true;
							return false;
						} else { return false; }
					case 5:
						te = this.worldObj.getBlockTileEntity(xCoord, yCoord, zCoord - 1);
						if(te != null){
							if(te instanceof ITankContainer || te instanceof ISidedInventory)
								return true;
							return false;
						} else { return false; }
					}
					return false;
				}
}