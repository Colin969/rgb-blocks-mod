package net.colin969.rgbwool.entity;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.client.FMLClientHandler;

import net.colin969.rgbwool.model.ModelFactoryBlock;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;

public class TileEntitySpecialFactory extends TileEntitySpecialRenderer{
    private ModelFactoryBlock modelFactoryBlock = new ModelFactoryBlock();
    
    @Override
    public void renderTileEntityAt(TileEntity tileEntity,
                                   double x,
                                   double y,
                                   double z,
                                   float f) {
 
    	if(tileEntity.getBlockMetadata() == 8){
    			GL11.glPushMatrix();
    			GL11.glTranslatef((float) x  + 0.1475F, (float) y + 0.8475F, (float) z + 0.8550F);
    			GL11.glScalef(0.8475f, -0.8475f, -0.8475f); 
	 
    			boolean[] sides = ((TileEntityDyeMachine) tileEntity).getConnectedSides();
    			
	            this.bindTextureByName("/mods/RGBBlocks/textures/blocks/ModelFactoryBlock.png");
	 
	            modelFactoryBlock.render(sides);
	            GL11.glPopMatrix();
    	}
    }
}
