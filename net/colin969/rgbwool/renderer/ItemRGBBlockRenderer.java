package net.colin969.rgbwool.renderer;

import static net.minecraftforge.client.IItemRenderer.ItemRenderType.EQUIPPED;
import static net.minecraftforge.client.IItemRenderer.ItemRenderType.FIRST_PERSON_MAP;

import java.awt.Window.Type;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.TextureLoader;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAnvil;
import net.minecraft.block.BlockHopper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.MapItemRenderer;
import net.minecraft.client.renderer.ChestItemRenderHelper;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.EntityLiving;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemMap;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.src.FMLRenderAccessLibrary;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.storage.MapData;
import net.minecraftforge.client.ForgeHooksClient;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;

public class ItemRGBBlockRenderer implements IItemRenderer{

	Minecraft mc = Minecraft.getMinecraft();
	
    private float equippedProgress = 0.0F;
    private float prevEquippedProgress = 0.0F;
	private ItemStack itemToRender = null;
    public final MapItemRenderer mapItemRenderer;
	
    private int currentTexture = 0;
    
    public ItemRGBBlockRenderer(){
		this.mapItemRenderer = new MapItemRenderer(mc.fontRenderer, mc.gameSettings, mc.renderEngine);
    }
    
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
			ItemRendererHelper helper) {
		if(type == type.ENTITY || type == type.FIRST_PERSON_MAP)
			return true;
		return false;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		
        NBTTagCompound nbt = item.getTagCompound();
        
        Block block = null;
        if(nbt != null)
        	block = Block.blocksList[nbt.getInteger("blockId")];
        
        GL11.glPushMatrix();
        
        if (block != null)
        {
        	RenderBlocks rbi = (RenderBlocks) data[0];
        	RenderItem ri = new RenderItem();
        		this.mc.renderEngine.bindTexture("/terrain.png");
        		renderBlockAsItem(block, nbt.getInteger("blockMetadata"), 1.0F, rbi, nbt.getInteger("Color"), type);
        }
        
        GL11.glPopMatrix();
	}
	
    public void setStandardTexture() {
        if (currentTexture != 0) {
            currentTexture = 0;
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, Minecraft.getMinecraft().renderEngine.textureMapBlocks.getTexture().getGlTextureId());
        }
    }

    public void setGreyTexture() {
        if (currentTexture != 1) {
            if (TextureLoader.getGlTextureID() > 0) {
                currentTexture = 1;
                GL11.glBindTexture(GL11.GL_TEXTURE_2D, TextureLoader.getGreyTerrainID());
            }
        }
    }

	
    public void renderBlockAsItem(Block par1Block, int par2, float par3, RenderBlocks renderer, int colorcode, ItemRenderType type)
    {
        setGreyTexture();
        Tessellator tessellator = Tessellator.instance;
        boolean flag = par1Block.blockID == Block.grass.blockID;

        renderer.setRenderBoundsFromBlock(par1Block);

        par1Block.setBlockBoundsForItemRender();
        renderer.setRenderBoundsFromBlock(par1Block);
        if(type == type.FIRST_PERSON_MAP || type != type.EQUIPPED){
        	GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
        	GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
        } else {
        	GL11.glRotatef(0F, 234F, 53F, 23F);
        }
            
        if(type == type.INVENTORY){
        	GL11.glRotatef(53.8F, -35.0F, 119.0F, 90.0F);
        	GL11.glRotatef(180F,1F,0F,0F);
        	GL11.glRotatef(15F,0F,0F,0F);
        	GL11.glScalef(10.0F, 10.0F, 10.0F);
        	GL11.glTranslatef(-2.725F, -3F, 0.4F);
        }
        
        if(type == type.ENTITY){
        	GL11.glScalef(0.5F, 0.5F, 0.5F);
        	GL11.glTranslatef(0.0F, 0.625F, 0.0F);
        }
        
        if(type == type.EQUIPPED){
        	GL11.glScalef(0.475F, 0.475F, 0.475F);
            GL11.glTranslatef(1F, 0.5F, -0.5F);
        }
            
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, -1.0F, 0.0F);
        tessellator.setColorOpaque_I(colorcode);
        tessellator.setBrightness(1600000);
        renderer.renderFaceYNeg(par1Block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(par1Block, 0, par2));
        tessellator.draw();

        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 1.0F, 0.0F);
        tessellator.setColorOpaque_I(colorcode);
        tessellator.setBrightness(1000000);
        renderer.renderFaceYPos(par1Block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(par1Block, 1, par2));
        tessellator.draw();

        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 0.0F, -1.0F);
        tessellator.setColorOpaque_I(colorcode);
        tessellator.setBrightness(1600000);
        renderer.renderFaceZPos(par1Block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(par1Block, 2, par2));
        tessellator.draw();
            
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 0.0F, 1.0F);
        tessellator.setColorOpaque_I(colorcode);
        renderer.renderFaceZNeg(par1Block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(par1Block, 3, par2));
        tessellator.draw();
            
        tessellator.startDrawingQuads();
        tessellator.setNormal(-1.0F, 0.0F, 0.0F);
        tessellator.setColorOpaque_I(colorcode);
        renderer.renderFaceXPos(par1Block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(par1Block, 4, par2));
        tessellator.draw();
            
        tessellator.startDrawingQuads();
        tessellator.setNormal(1.0F, 0.0F, 0.0F);
        tessellator.setColorOpaque_I(colorcode);
        renderer.renderFaceXNeg(par1Block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(par1Block, 5, par2));
        tessellator.draw();
            
        GL11.glTranslatef(0.5F, 0.5F, 0.5F);
            
        setStandardTexture();
    }
}
