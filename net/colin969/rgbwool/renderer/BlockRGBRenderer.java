package net.colin969.rgbwool.renderer;

import org.lwjgl.opengl.GL11;

import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.TextureLoader;
import net.colin969.rgbwool.entity.TileEntityRGB;
import net.colin969.rgbwool.proxy.ClientProxy;
import net.minecraft.block.Block;
import net.minecraft.block.BlockGrass;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

public class BlockRGBRenderer implements ISimpleBlockRenderingHandler {

    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks useless) {

    }

    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
        if (block == RGBBlock.BlockRGB) {
            TileEntityRGB te = (TileEntityRGB) world.getBlockTileEntity(x, y, z);
            if (te.isBlockValid()) {
            	Icon icon = Block.blocksList[te.blockId].getBlockTextureFromSide(0);
            	if(icon != null){
	                if (!Minecraft.isAmbientOcclusionEnabled())
	                    renderStandardBlockWithColorMultiplier(block, x, y, z, renderer, te);
	                else
	                    renderStandardBlockWithAmbientOcclusion(block, x, y, z, renderer, te);
            	}
            }
            return true;
        }
        return false;
    }

    int currentTexture = 0;

    public void setStandardTexture() {
        if (currentTexture != 0) {
            Tessellator tessellator = Tessellator.instance;
            tessellator.draw();
            tessellator.startDrawingQuads();
            currentTexture = 0;
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, Minecraft.getMinecraft().renderEngine.textureMapBlocks.getTexture().getGlTextureId());
        }
    }

    public void setGreyTexture() {
        if (currentTexture != 1) {
            if (TextureLoader.getGlTextureID() > 0) {
                Tessellator tessellator = Tessellator.instance;
                tessellator.draw();
                tessellator.startDrawingQuads();
                currentTexture = 1;
                GL11.glBindTexture(GL11.GL_TEXTURE_2D, TextureLoader.getGreyTerrainID());
            }
        }
    }

    public boolean renderStandardBlockWithAmbientOcclusion(Block par1Block, int par2, int par3, int par4, RenderBlocks renderer, TileEntityRGB TileEnt) {
        renderer.enableAO = true;
        boolean flag = false;
        boolean isGrass = false;
        float par5 = 0.5F;
        float par6 = 0.5F;
        float par7 = 0.5F;
        Block blockModel = Block.blocksList[TileEnt.blockId];
        Icon topFace = blockModel.getIcon(1, TileEnt.blockMetadata);
        Icon bottomFace = blockModel.getIcon(0, TileEnt.blockMetadata);
        Icon northFace = blockModel.getIcon(4, TileEnt.blockMetadata);
        Icon southFace = blockModel.getIcon(5, TileEnt.blockMetadata);
        Icon eastFace = blockModel.getIcon(2, TileEnt.blockMetadata);
        Icon westFace = blockModel.getIcon(3, TileEnt.blockMetadata);

        int colorMulti = par1Block.colorMultiplier(renderer.blockAccess, par2, par3, par4);
        float r = (float)(colorMulti >> 16 & 255) / 255.0F;
        float g = (float)(colorMulti >> 8 & 255) / 255.0F;
        float b = (float)(colorMulti & 255) / 255.0F;
        
        int colorTop = TileEnt.Top;
        int colorBot = TileEnt.Bottom;
        int colorNor = TileEnt.North;
        int colorSou = TileEnt.South;
        int colorEas = TileEnt.East;
        int colorWes = TileEnt.West;

        float fRTop = ((float) ((colorTop >> 16) & 0xFF) / 255F);
        float fGTop = ((float) ((colorTop >> 8) & 0xFF) / 255F);
        float fBTop = ((float) ((colorTop) & 0xFF) / 255F);
        float fRBot = ((float) ((colorBot >> 16) & 0xFF) / 255F);
        float fGBot = ((float) ((colorBot >> 8) & 0xFF) / 255F);
        float fBBot = ((float) ((colorBot) & 0xFF) / 255F);
        float fRNor = ((float) ((colorNor >> 16) & 0xFF) / 255F);
        float fGNor = ((float) ((colorNor >> 8) & 0xFF) / 255F);
        float fBNor = ((float) ((colorNor) & 0xFF) / 255F);
        float fRSou = ((float) ((colorSou >> 16) & 0xFF) / 255F);
        float fGSou = ((float) ((colorSou >> 8) & 0xFF) / 255F);
        float fBSou = ((float) ((colorSou) & 0xFF) / 255F);
        float fREas = ((float) ((colorEas >> 16) & 0xFF) / 255F);
        float fGEas = ((float) ((colorEas >> 8) & 0xFF) / 255F);
        float fBEas = ((float) ((colorEas) & 0xFF) / 255F);
        float fRWes = ((float) ((colorWes >> 16) & 0xFF) / 255F);
        float fGWes = ((float) ((colorWes >> 8) & 0xFF) / 255F);
        float fBWes = ((float) ((colorWes) & 0xFF) / 255F);
        float f3 = 0.0F;
        float f4 = 0.0F;
        float f5 = 0.0F;
        float f6 = 0.0F;
        boolean flag1 = true;
        int l = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4);
        Tessellator tessellator = Tessellator.instance;
        tessellator.setBrightness(983055);

        if (TileEnt.Top == -1) {
            fRTop = r;
            fGTop = g;
            fBTop = b;
        }

        if (TileEnt.Bottom == -1) {
            fRBot = r;
            fGBot = g;
            fBBot = b;
        }

        if (TileEnt.North == -1) {
            fRNor = r;
            fGNor = g;
            fBNor = b;
        }

        if (TileEnt.South == -1) {
            fRSou = r;
            fGSou = g;
            fBSou = b;
        }

        if (TileEnt.East == -1) {
            fREas = r;
            fGEas = g;
            fBEas = b;
        }

        if (TileEnt.West == -1) {
            fRWes = r;
            fGWes = g;
            fBWes = b;
        }

        if (TileEnt.blockId == Block.grass.blockID)
            isGrass = true;

        if (renderer.getBlockIcon(par1Block).getIconName().equals("grass_top")) {
            flag1 = false;
        } else if (renderer.hasOverrideBlockTexture()) {
            flag1 = false;
        }

        boolean flag2;
        boolean flag3;
        boolean flag4;
        boolean flag5;
        float f7;
        int i1;

        if (renderer.renderAllFaces || par1Block.shouldSideBeRendered(renderer.blockAccess, par2, par3 - 1, par4, 0)) {
            if (renderer.renderMinY <= 0.0D) {
                --par3;
            }

            renderer.aoBrightnessXYNN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3, par4);
            renderer.aoBrightnessYZNN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4 - 1);
            renderer.aoBrightnessYZNP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4 + 1);
            renderer.aoBrightnessXYPN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3, par4);
            renderer.aoLightValueScratchXYNN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3, par4);
            renderer.aoLightValueScratchYZNN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3, par4 - 1);
            renderer.aoLightValueScratchYZNP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3, par4 + 1);
            renderer.aoLightValueScratchXYPN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3, par4);
            flag3 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 + 1, par3 - 1, par4)];
            flag2 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 - 1, par3 - 1, par4)];
            flag5 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2, par3 - 1, par4 + 1)];
            flag4 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2, par3 - 1, par4 - 1)];

            if (!flag4 && !flag2) {
                renderer.aoLightValueScratchXYZNNN = renderer.aoLightValueScratchXYNN;
                renderer.aoBrightnessXYZNNN = renderer.aoBrightnessXYNN;
            } else {
                renderer.aoLightValueScratchXYZNNN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3, par4 - 1);
                renderer.aoBrightnessXYZNNN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3, par4 - 1);
            }

            if (!flag5 && !flag2) {
                renderer.aoLightValueScratchXYZNNP = renderer.aoLightValueScratchXYNN;
                renderer.aoBrightnessXYZNNP = renderer.aoBrightnessXYNN;
            } else {
                renderer.aoLightValueScratchXYZNNP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3, par4 + 1);
                renderer.aoBrightnessXYZNNP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3, par4 + 1);
            }

            if (!flag4 && !flag3) {
                renderer.aoLightValueScratchXYZPNN = renderer.aoLightValueScratchXYPN;
                renderer.aoBrightnessXYZPNN = renderer.aoBrightnessXYPN;
            } else {
                renderer.aoLightValueScratchXYZPNN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3, par4 - 1);
                renderer.aoBrightnessXYZPNN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3, par4 - 1);
            }

            if (!flag5 && !flag3) {
                renderer.aoLightValueScratchXYZPNP = renderer.aoLightValueScratchXYPN;
                renderer.aoBrightnessXYZPNP = renderer.aoBrightnessXYPN;
            } else {
                renderer.aoLightValueScratchXYZPNP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3, par4 + 1);
                renderer.aoBrightnessXYZPNP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3, par4 + 1);
            }

            if (renderer.renderMinY <= 0.0D) {
                ++par3;
            }

            i1 = l;

            if (renderer.renderMinY <= 0.0D || !renderer.blockAccess.isBlockOpaqueCube(par2, par3 - 1, par4)) {
                i1 = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 - 1, par4);
            }

            f7 = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 - 1, par4);
            f3 = (renderer.aoLightValueScratchXYZNNP + renderer.aoLightValueScratchXYNN + renderer.aoLightValueScratchYZNP + f7) / 4.0F;
            f6 = (renderer.aoLightValueScratchYZNP + f7 + renderer.aoLightValueScratchXYZPNP + renderer.aoLightValueScratchXYPN) / 4.0F;
            f5 = (f7 + renderer.aoLightValueScratchYZNN + renderer.aoLightValueScratchXYPN + renderer.aoLightValueScratchXYZPNN) / 4.0F;
            f4 = (renderer.aoLightValueScratchXYNN + renderer.aoLightValueScratchXYZNNN + f7 + renderer.aoLightValueScratchYZNN) / 4.0F;
            renderer.brightnessTopLeft = renderer.getAoBrightness(renderer.aoBrightnessXYZNNP, renderer.aoBrightnessXYNN, renderer.aoBrightnessYZNP, i1);
            renderer.brightnessTopRight = renderer.getAoBrightness(renderer.aoBrightnessYZNP, renderer.aoBrightnessXYZPNP, renderer.aoBrightnessXYPN, i1);
            renderer.brightnessBottomRight = renderer.getAoBrightness(renderer.aoBrightnessYZNN, renderer.aoBrightnessXYPN, renderer.aoBrightnessXYZPNN, i1);
            renderer.brightnessBottomLeft = renderer.getAoBrightness(renderer.aoBrightnessXYNN, renderer.aoBrightnessXYZNNN, renderer.aoBrightnessYZNN, i1);

            if (flag1) {
                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = fRBot * 0.5F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = fGBot * 0.5F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = fBBot * 0.5F;
            } else {
                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = 0.5F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = 0.5F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = 0.5F;
            }

            renderer.colorRedTopLeft *= f3;
            renderer.colorGreenTopLeft *= f3;
            renderer.colorBlueTopLeft *= f3;
            renderer.colorRedBottomLeft *= f4;
            renderer.colorGreenBottomLeft *= f4;
            renderer.colorBlueBottomLeft *= f4;
            renderer.colorRedBottomRight *= f5;
            renderer.colorGreenBottomRight *= f5;
            renderer.colorBlueBottomRight *= f5;
            renderer.colorRedTopRight *= f6;
            renderer.colorGreenTopRight *= f6;
            renderer.colorBlueTopRight *= f6;
            if (TileEnt.Bottom == -1) {
                setStandardTexture();
            } else {
                setGreyTexture();
            }
            renderBottomFace(par1Block, (double) par2, (double) par3, (double) par4, bottomFace, renderer);
            flag = true;
        }

        if (renderer.renderAllFaces || par1Block.shouldSideBeRendered(renderer.blockAccess, par2, par3 + 1, par4, 1)) {
            if (renderer.renderMaxY >= 1.0D) {
                ++par3;
            }

            renderer.aoBrightnessXYNP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3, par4);
            renderer.aoBrightnessXYPP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3, par4);
            renderer.aoBrightnessYZPN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4 - 1);
            renderer.aoBrightnessYZPP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4 + 1);
            renderer.aoLightValueScratchXYNP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3, par4);
            renderer.aoLightValueScratchXYPP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3, par4);
            renderer.aoLightValueScratchYZPN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3, par4 - 1);
            renderer.aoLightValueScratchYZPP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3, par4 + 1);
            flag3 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 + 1, par3 + 1, par4)];
            flag2 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 - 1, par3 + 1, par4)];
            flag5 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2, par3 + 1, par4 + 1)];
            flag4 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2, par3 + 1, par4 - 1)];

            if (!flag4 && !flag2) {
                renderer.aoLightValueScratchXYZNPN = renderer.aoLightValueScratchXYNP;
                renderer.aoBrightnessXYZNPN = renderer.aoBrightnessXYNP;
            } else {
                renderer.aoLightValueScratchXYZNPN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3, par4 - 1);
                renderer.aoBrightnessXYZNPN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3, par4 - 1);
            }

            if (!flag4 && !flag3) {
                renderer.aoLightValueScratchXYZPPN = renderer.aoLightValueScratchXYPP;
                renderer.aoBrightnessXYZPPN = renderer.aoBrightnessXYPP;
            } else {
                renderer.aoLightValueScratchXYZPPN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3, par4 - 1);
                renderer.aoBrightnessXYZPPN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3, par4 - 1);
            }

            if (!flag5 && !flag2) {
                renderer.aoLightValueScratchXYZNPP = renderer.aoLightValueScratchXYNP;
                renderer.aoBrightnessXYZNPP = renderer.aoBrightnessXYNP;
            } else {
                renderer.aoLightValueScratchXYZNPP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3, par4 + 1);
                renderer.aoBrightnessXYZNPP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3, par4 + 1);
            }

            if (!flag5 && !flag3) {
                renderer.aoLightValueScratchXYZPPP = renderer.aoLightValueScratchXYPP;
                renderer.aoBrightnessXYZPPP = renderer.aoBrightnessXYPP;
            } else {
                renderer.aoLightValueScratchXYZPPP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3, par4 + 1);
                renderer.aoBrightnessXYZPPP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3, par4 + 1);
            }

            if (renderer.renderMaxY >= 1.0D) {
                --par3;
            }

            i1 = l;

            if (renderer.renderMaxY >= 1.0D || !renderer.blockAccess.isBlockOpaqueCube(par2, par3 + 1, par4)) {
                i1 = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 + 1, par4);
            }

            f7 = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 + 1, par4);
            f6 = (renderer.aoLightValueScratchXYZNPP + renderer.aoLightValueScratchXYNP + renderer.aoLightValueScratchYZPP + f7) / 4.0F;
            f3 = (renderer.aoLightValueScratchYZPP + f7 + renderer.aoLightValueScratchXYZPPP + renderer.aoLightValueScratchXYPP) / 4.0F;
            f4 = (f7 + renderer.aoLightValueScratchYZPN + renderer.aoLightValueScratchXYPP + renderer.aoLightValueScratchXYZPPN) / 4.0F;
            f5 = (renderer.aoLightValueScratchXYNP + renderer.aoLightValueScratchXYZNPN + f7 + renderer.aoLightValueScratchYZPN) / 4.0F;

            renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = fRTop;
            renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = fGTop;
            renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = fBTop;

            renderer.brightnessTopRight = renderer.getAoBrightness(renderer.aoBrightnessXYZNPP, renderer.aoBrightnessXYNP, renderer.aoBrightnessYZPP, i1);
            renderer.brightnessTopLeft = renderer.getAoBrightness(renderer.aoBrightnessYZPP, renderer.aoBrightnessXYZPPP, renderer.aoBrightnessXYPP, i1);
            renderer.brightnessBottomLeft = renderer.getAoBrightness(renderer.aoBrightnessYZPN, renderer.aoBrightnessXYPP, renderer.aoBrightnessXYZPPN, i1);
            renderer.brightnessBottomRight = renderer.getAoBrightness(renderer.aoBrightnessXYNP, renderer.aoBrightnessXYZNPN, renderer.aoBrightnessYZPN, i1);
            renderer.colorRedTopLeft *= f3;
            renderer.colorGreenTopLeft *= f3;
            renderer.colorBlueTopLeft *= f3;
            renderer.colorRedBottomLeft *= f4;
            renderer.colorGreenBottomLeft *= f4;
            renderer.colorBlueBottomLeft *= f4;
            renderer.colorRedBottomRight *= f5;
            renderer.colorGreenBottomRight *= f5;
            renderer.colorBlueBottomRight *= f5;
            renderer.colorRedTopRight *= f6;
            renderer.colorGreenTopRight *= f6;
            renderer.colorBlueTopRight *= f6;
            if (TileEnt.Top == -1) {
                setStandardTexture();
            } else {
                setGreyTexture();
            }

            if (isGrass && colorTop == -1) {
                int col = renderer.blockAccess.getBiomeGenForCoords(par2, par4).getBiomeGrassColor();
                float f = (float) (col >> 16 & 255) / 255.0F;
                float f1 = (float) (col >> 8 & 255) / 255.0F;
                float f2 = (float) (col & 255) / 255.0F;

                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = f;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = f1;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = f2;

                renderer.colorRedTopLeft *= f3;
                renderer.colorGreenTopLeft *= f3;
                renderer.colorBlueTopLeft *= f3;
                renderer.colorRedBottomLeft *= f4;
                renderer.colorGreenBottomLeft *= f4;
                renderer.colorBlueBottomLeft *= f4;
                renderer.colorRedBottomRight *= f5;
                renderer.colorGreenBottomRight *= f5;
                renderer.colorBlueBottomRight *= f5;
                renderer.colorRedTopRight *= f6;
                renderer.colorGreenTopRight *= f6;
                renderer.colorBlueTopRight *= f6;

                renderer.renderFaceYPos(par1Block, (double) par2, (double) par3, (double) par4, topFace);
            } else {
                renderer.renderFaceYPos(par1Block, (double) par2, (double) par3, (double) par4, topFace);
            }
            flag = true;
        }

        Icon icon;

        if (renderer.renderAllFaces || par1Block.shouldSideBeRendered(renderer.blockAccess, par2, par3, par4 - 1, 2)) {

            if (renderer.renderMinZ <= 0.0D) {
                --par4;
            }

            renderer.aoLightValueScratchXZNN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3, par4);
            renderer.aoLightValueScratchYZNN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 - 1, par4);
            renderer.aoLightValueScratchYZPN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 + 1, par4);
            renderer.aoLightValueScratchXZPN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3, par4);
            renderer.aoBrightnessXZNN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3, par4);
            renderer.aoBrightnessYZNN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 - 1, par4);
            renderer.aoBrightnessYZPN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 + 1, par4);
            renderer.aoBrightnessXZPN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3, par4);
            flag3 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 + 1, par3, par4 - 1)];
            flag2 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 - 1, par3, par4 - 1)];
            flag5 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2, par3 + 1, par4 - 1)];
            flag4 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2, par3 - 1, par4 - 1)];

            if (!flag2 && !flag4) {
                renderer.aoLightValueScratchXYZNNN = renderer.aoLightValueScratchXZNN;
                renderer.aoBrightnessXYZNNN = renderer.aoBrightnessXZNN;
            } else {
                renderer.aoLightValueScratchXYZNNN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3 - 1, par4);
                renderer.aoBrightnessXYZNNN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3 - 1, par4);
            }

            if (!flag2 && !flag5) {
                renderer.aoLightValueScratchXYZNPN = renderer.aoLightValueScratchXZNN;
                renderer.aoBrightnessXYZNPN = renderer.aoBrightnessXZNN;
            } else {
                renderer.aoLightValueScratchXYZNPN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3 + 1, par4);
                renderer.aoBrightnessXYZNPN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3 + 1, par4);
            }

            if (!flag3 && !flag4) {
                renderer.aoLightValueScratchXYZPNN = renderer.aoLightValueScratchXZPN;
                renderer.aoBrightnessXYZPNN = renderer.aoBrightnessXZPN;
            } else {
                renderer.aoLightValueScratchXYZPNN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3 - 1, par4);
                renderer.aoBrightnessXYZPNN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3 - 1, par4);
            }

            if (!flag3 && !flag5) {
                renderer.aoLightValueScratchXYZPPN = renderer.aoLightValueScratchXZPN;
                renderer.aoBrightnessXYZPPN = renderer.aoBrightnessXZPN;
            } else {
                renderer.aoLightValueScratchXYZPPN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3 + 1, par4);
                renderer.aoBrightnessXYZPPN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3 + 1, par4);
            }

            if (renderer.renderMinZ <= 0.0D) {
                ++par4;
            }

            i1 = l;

            if (renderer.renderMinZ <= 0.0D || !renderer.blockAccess.isBlockOpaqueCube(par2, par3, par4 - 1)) {
                i1 = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4 - 1);
            }

            f7 = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3, par4 - 1);
            f3 = (renderer.aoLightValueScratchXZNN + renderer.aoLightValueScratchXYZNPN + f7 + renderer.aoLightValueScratchYZPN) / 4.0F;
            f4 = (f7 + renderer.aoLightValueScratchYZPN + renderer.aoLightValueScratchXZPN + renderer.aoLightValueScratchXYZPPN) / 4.0F;
            f5 = (renderer.aoLightValueScratchYZNN + f7 + renderer.aoLightValueScratchXYZPNN + renderer.aoLightValueScratchXZPN) / 4.0F;
            f6 = (renderer.aoLightValueScratchXYZNNN + renderer.aoLightValueScratchXZNN + renderer.aoLightValueScratchYZNN + f7) / 4.0F;
            renderer.brightnessTopLeft = renderer.getAoBrightness(renderer.aoBrightnessXZNN, renderer.aoBrightnessXYZNPN, renderer.aoBrightnessYZPN, i1);
            renderer.brightnessBottomLeft = renderer.getAoBrightness(renderer.aoBrightnessYZPN, renderer.aoBrightnessXZPN, renderer.aoBrightnessXYZPPN, i1);
            renderer.brightnessBottomRight = renderer.getAoBrightness(renderer.aoBrightnessYZNN, renderer.aoBrightnessXYZPNN, renderer.aoBrightnessXZPN, i1);
            renderer.brightnessTopRight = renderer.getAoBrightness(renderer.aoBrightnessXYZNNN, renderer.aoBrightnessXZNN, renderer.aoBrightnessYZNN, i1);

            if (flag1) {
                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = fREas * 0.8F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = fGEas * 0.8F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = fBEas * 0.8F;
            } else {
                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = 0.8F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = 0.8F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = 0.8F;
            }

            renderer.colorRedTopLeft *= f3;
            renderer.colorGreenTopLeft *= f3;
            renderer.colorBlueTopLeft *= f3;
            renderer.colorRedBottomLeft *= f4;
            renderer.colorGreenBottomLeft *= f4;
            renderer.colorBlueBottomLeft *= f4;
            renderer.colorRedBottomRight *= f5;
            renderer.colorGreenBottomRight *= f5;
            renderer.colorBlueBottomRight *= f5;
            renderer.colorRedTopRight *= f6;
            renderer.colorGreenTopRight *= f6;
            renderer.colorBlueTopRight *= f6;
            if (TileEnt.East == -1) {
                setStandardTexture();
            } else {
                setGreyTexture();
            }
            renderer.renderFaceZNeg(par1Block, (double) par2, (double) par3, (double) par4, eastFace);

            if (RenderBlocks.fancyGrass && isGrass && colorEas == -1) {
                int col = renderer.blockAccess.getBiomeGenForCoords(par2, par4).getBiomeGrassColor();
                float f = (float) (col >> 16 & 255) / 255.0F;
                float f1 = (float) (col >> 8 & 255) / 255.0F;
                float f2 = (float) (col & 255) / 255.0F;

                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = f * 0.8F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = f1 * 0.8F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = f2 * 0.8F;

                renderer.colorRedTopLeft *= f3;
                renderer.colorGreenTopLeft *= f3;
                renderer.colorBlueTopLeft *= f3;
                renderer.colorRedBottomLeft *= f4;
                renderer.colorGreenBottomLeft *= f4;
                renderer.colorBlueBottomLeft *= f4;
                renderer.colorRedBottomRight *= f5;
                renderer.colorGreenBottomRight *= f5;
                renderer.colorBlueBottomRight *= f5;
                renderer.colorRedTopRight *= f6;
                renderer.colorGreenTopRight *= f6;
                renderer.colorBlueTopRight *= f6;

                renderer.renderFaceZNeg(par1Block, (double) par2, (double) par3, (double) par4, BlockGrass.getIconSideOverlay());
            }

            flag = true;
        }

        if (renderer.renderAllFaces || par1Block.shouldSideBeRendered(renderer.blockAccess, par2, par3, par4 + 1, 3)) {
            if (renderer.renderMaxZ >= 1.0D) {
                ++par4;
            }

            renderer.aoLightValueScratchXZNP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3, par4);
            renderer.aoLightValueScratchXZPP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3, par4);
            renderer.aoLightValueScratchYZNP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 - 1, par4);
            renderer.aoLightValueScratchYZPP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 + 1, par4);
            renderer.aoBrightnessXZNP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3, par4);
            renderer.aoBrightnessXZPP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3, par4);
            renderer.aoBrightnessYZNP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 - 1, par4);
            renderer.aoBrightnessYZPP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 + 1, par4);
            flag3 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 + 1, par3, par4 + 1)];
            flag2 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 - 1, par3, par4 + 1)];
            flag5 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2, par3 + 1, par4 + 1)];
            flag4 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2, par3 - 1, par4 + 1)];

            if (!flag2 && !flag4) {
                renderer.aoLightValueScratchXYZNNP = renderer.aoLightValueScratchXZNP;
                renderer.aoBrightnessXYZNNP = renderer.aoBrightnessXZNP;
            } else {
                renderer.aoLightValueScratchXYZNNP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3 - 1, par4);
                renderer.aoBrightnessXYZNNP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3 - 1, par4);
            }

            if (!flag2 && !flag5) {
                renderer.aoLightValueScratchXYZNPP = renderer.aoLightValueScratchXZNP;
                renderer.aoBrightnessXYZNPP = renderer.aoBrightnessXZNP;
            } else {
                renderer.aoLightValueScratchXYZNPP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3 + 1, par4);
                renderer.aoBrightnessXYZNPP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3 + 1, par4);
            }

            if (!flag3 && !flag4) {
                renderer.aoLightValueScratchXYZPNP = renderer.aoLightValueScratchXZPP;
                renderer.aoBrightnessXYZPNP = renderer.aoBrightnessXZPP;
            } else {
                renderer.aoLightValueScratchXYZPNP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3 - 1, par4);
                renderer.aoBrightnessXYZPNP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3 - 1, par4);
            }

            if (!flag3 && !flag5) {
                renderer.aoLightValueScratchXYZPPP = renderer.aoLightValueScratchXZPP;
                renderer.aoBrightnessXYZPPP = renderer.aoBrightnessXZPP;
            } else {
                renderer.aoLightValueScratchXYZPPP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3 + 1, par4);
                renderer.aoBrightnessXYZPPP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3 + 1, par4);
            }

            if (renderer.renderMaxZ >= 1.0D) {
                --par4;
            }

            i1 = l;

            if (renderer.renderMaxZ >= 1.0D || !renderer.blockAccess.isBlockOpaqueCube(par2, par3, par4 + 1)) {
                i1 = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4 + 1);
            }

            f7 = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3, par4 + 1);
            f3 = (renderer.aoLightValueScratchXZNP + renderer.aoLightValueScratchXYZNPP + f7 + renderer.aoLightValueScratchYZPP) / 4.0F;
            f6 = (f7 + renderer.aoLightValueScratchYZPP + renderer.aoLightValueScratchXZPP + renderer.aoLightValueScratchXYZPPP) / 4.0F;
            f5 = (renderer.aoLightValueScratchYZNP + f7 + renderer.aoLightValueScratchXYZPNP + renderer.aoLightValueScratchXZPP) / 4.0F;
            f4 = (renderer.aoLightValueScratchXYZNNP + renderer.aoLightValueScratchXZNP + renderer.aoLightValueScratchYZNP + f7) / 4.0F;
            renderer.brightnessTopLeft = renderer.getAoBrightness(renderer.aoBrightnessXZNP, renderer.aoBrightnessXYZNPP, renderer.aoBrightnessYZPP, i1);
            renderer.brightnessTopRight = renderer.getAoBrightness(renderer.aoBrightnessYZPP, renderer.aoBrightnessXZPP, renderer.aoBrightnessXYZPPP, i1);
            renderer.brightnessBottomRight = renderer.getAoBrightness(renderer.aoBrightnessYZNP, renderer.aoBrightnessXYZPNP, renderer.aoBrightnessXZPP, i1);
            renderer.brightnessBottomLeft = renderer.getAoBrightness(renderer.aoBrightnessXYZNNP, renderer.aoBrightnessXZNP, renderer.aoBrightnessYZNP, i1);

            if (flag1) {
                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = fRWes * 0.8F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = fGWes * 0.8F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = fBWes * 0.8F;
            } else {
                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = 0.8F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = 0.8F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = 0.8F;
            }

            renderer.colorRedTopLeft *= f3;
            renderer.colorGreenTopLeft *= f3;
            renderer.colorBlueTopLeft *= f3;
            renderer.colorRedBottomLeft *= f4;
            renderer.colorGreenBottomLeft *= f4;
            renderer.colorBlueBottomLeft *= f4;
            renderer.colorRedBottomRight *= f5;
            renderer.colorGreenBottomRight *= f5;
            renderer.colorBlueBottomRight *= f5;
            renderer.colorRedTopRight *= f6;
            renderer.colorGreenTopRight *= f6;
            renderer.colorBlueTopRight *= f6;
            if (TileEnt.West == -1) {
                setStandardTexture();
            } else {
                setGreyTexture();
            }
            renderer.renderFaceZPos(par1Block, (double) par2, (double) par3, (double) par4, westFace);

            if (RenderBlocks.fancyGrass && isGrass && colorWes == -1) {
                int col = renderer.blockAccess.getBiomeGenForCoords(par2, par4).getBiomeGrassColor();
                float f = (float) (col >> 16 & 255) / 255.0F;
                float f1 = (float) (col >> 8 & 255) / 255.0F;
                float f2 = (float) (col & 255) / 255.0F;

                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = f * 0.8F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = f1 * 0.8F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = f2 * 0.8F;

                renderer.colorRedTopLeft *= f3;
                renderer.colorGreenTopLeft *= f3;
                renderer.colorBlueTopLeft *= f3;
                renderer.colorRedBottomLeft *= f4;
                renderer.colorGreenBottomLeft *= f4;
                renderer.colorBlueBottomLeft *= f4;
                renderer.colorRedBottomRight *= f5;
                renderer.colorGreenBottomRight *= f5;
                renderer.colorBlueBottomRight *= f5;
                renderer.colorRedTopRight *= f6;
                renderer.colorGreenTopRight *= f6;
                renderer.colorBlueTopRight *= f6;

                renderer.renderFaceZPos(par1Block, (double) par2, (double) par3, (double) par4, BlockGrass.getIconSideOverlay());
            }

            flag = true;
        }

        if (renderer.renderAllFaces || par1Block.shouldSideBeRendered(renderer.blockAccess, par2 - 1, par3, par4, 4)) {
            if (renderer.renderMinX <= 0.0D) {
                --par2;
            }

            renderer.aoLightValueScratchXYNN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 - 1, par4);
            renderer.aoLightValueScratchXZNN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3, par4 - 1);
            renderer.aoLightValueScratchXZNP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3, par4 + 1);
            renderer.aoLightValueScratchXYNP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 + 1, par4);
            renderer.aoBrightnessXYNN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 - 1, par4);
            renderer.aoBrightnessXZNN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4 - 1);
            renderer.aoBrightnessXZNP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4 + 1);
            renderer.aoBrightnessXYNP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 + 1, par4);
            flag3 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 - 1, par3 + 1, par4)];
            flag2 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 - 1, par3 - 1, par4)];
            flag5 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 - 1, par3, par4 - 1)];
            flag4 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 - 1, par3, par4 + 1)];

            if (!flag5 && !flag2) {
                renderer.aoLightValueScratchXYZNNN = renderer.aoLightValueScratchXZNN;
                renderer.aoBrightnessXYZNNN = renderer.aoBrightnessXZNN;
            } else {
                renderer.aoLightValueScratchXYZNNN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 - 1, par4 - 1);
                renderer.aoBrightnessXYZNNN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 - 1, par4 - 1);
            }

            if (!flag4 && !flag2) {
                renderer.aoLightValueScratchXYZNNP = renderer.aoLightValueScratchXZNP;
                renderer.aoBrightnessXYZNNP = renderer.aoBrightnessXZNP;
            } else {
                renderer.aoLightValueScratchXYZNNP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 - 1, par4 + 1);
                renderer.aoBrightnessXYZNNP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 - 1, par4 + 1);
            }

            if (!flag5 && !flag3) {
                renderer.aoLightValueScratchXYZNPN = renderer.aoLightValueScratchXZNN;
                renderer.aoBrightnessXYZNPN = renderer.aoBrightnessXZNN;
            } else {
                renderer.aoLightValueScratchXYZNPN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 + 1, par4 - 1);
                renderer.aoBrightnessXYZNPN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 + 1, par4 - 1);
            }

            if (!flag4 && !flag3) {
                renderer.aoLightValueScratchXYZNPP = renderer.aoLightValueScratchXZNP;
                renderer.aoBrightnessXYZNPP = renderer.aoBrightnessXZNP;
            } else {
                renderer.aoLightValueScratchXYZNPP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 + 1, par4 + 1);
                renderer.aoBrightnessXYZNPP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 + 1, par4 + 1);
            }

            if (renderer.renderMinX <= 0.0D) {
                ++par2;
            }

            i1 = l;

            if (renderer.renderMinX <= 0.0D || !renderer.blockAccess.isBlockOpaqueCube(par2 - 1, par3, par4)) {
                i1 = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3, par4);
            }

            f7 = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 - 1, par3, par4);
            f6 = (renderer.aoLightValueScratchXYNN + renderer.aoLightValueScratchXYZNNP + f7 + renderer.aoLightValueScratchXZNP) / 4.0F;
            f3 = (f7 + renderer.aoLightValueScratchXZNP + renderer.aoLightValueScratchXYNP + renderer.aoLightValueScratchXYZNPP) / 4.0F;
            f4 = (renderer.aoLightValueScratchXZNN + f7 + renderer.aoLightValueScratchXYZNPN + renderer.aoLightValueScratchXYNP) / 4.0F;
            f5 = (renderer.aoLightValueScratchXYZNNN + renderer.aoLightValueScratchXYNN + renderer.aoLightValueScratchXZNN + f7) / 4.0F;
            renderer.brightnessTopRight = renderer.getAoBrightness(renderer.aoBrightnessXYNN, renderer.aoBrightnessXYZNNP, renderer.aoBrightnessXZNP, i1);
            renderer.brightnessTopLeft = renderer.getAoBrightness(renderer.aoBrightnessXZNP, renderer.aoBrightnessXYNP, renderer.aoBrightnessXYZNPP, i1);
            renderer.brightnessBottomLeft = renderer.getAoBrightness(renderer.aoBrightnessXZNN, renderer.aoBrightnessXYZNPN, renderer.aoBrightnessXYNP, i1);
            renderer.brightnessBottomRight = renderer.getAoBrightness(renderer.aoBrightnessXYZNNN, renderer.aoBrightnessXYNN, renderer.aoBrightnessXZNN, i1);

            if (flag1) {
                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = fRNor * 0.6F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = fGNor * 0.6F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = fBNor * 0.6F;
            } else {
                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = 0.6F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = 0.6F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = 0.6F;
            }

            renderer.colorRedTopLeft *= f3;
            renderer.colorGreenTopLeft *= f3;
            renderer.colorBlueTopLeft *= f3;
            renderer.colorRedBottomLeft *= f4;
            renderer.colorGreenBottomLeft *= f4;
            renderer.colorBlueBottomLeft *= f4;
            renderer.colorRedBottomRight *= f5;
            renderer.colorGreenBottomRight *= f5;
            renderer.colorBlueBottomRight *= f5;
            renderer.colorRedTopRight *= f6;
            renderer.colorGreenTopRight *= f6;
            renderer.colorBlueTopRight *= f6;
            if (TileEnt.North == -1) {
                setStandardTexture();
            } else {
                setGreyTexture();
            }
            renderer.renderFaceXNeg(par1Block, (double) par2, (double) par3, (double) par4, northFace);

            if (RenderBlocks.fancyGrass && isGrass && colorNor == -1) {
                int col = renderer.blockAccess.getBiomeGenForCoords(par2, par4).getBiomeGrassColor();
                float f = (float) (col >> 16 & 255) / 255.0F;
                float f1 = (float) (col >> 8 & 255) / 255.0F;
                float f2 = (float) (col & 255) / 255.0F;

                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = f * 0.6F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = f1 * 0.6F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = f2 * 0.6F;

                renderer.colorRedTopLeft *= f3;
                renderer.colorGreenTopLeft *= f3;
                renderer.colorBlueTopLeft *= f3;
                renderer.colorRedBottomLeft *= f4;
                renderer.colorGreenBottomLeft *= f4;
                renderer.colorBlueBottomLeft *= f4;
                renderer.colorRedBottomRight *= f5;
                renderer.colorGreenBottomRight *= f5;
                renderer.colorBlueBottomRight *= f5;
                renderer.colorRedTopRight *= f6;
                renderer.colorGreenTopRight *= f6;
                renderer.colorBlueTopRight *= f6;

                renderer.renderFaceXNeg(par1Block, (double) par2, (double) par3, (double) par4, BlockGrass.getIconSideOverlay());
            }

            flag = true;
        }

        if (renderer.renderAllFaces || par1Block.shouldSideBeRendered(renderer.blockAccess, par2 + 1, par3, par4, 5)) {
            if (renderer.renderMaxX >= 1.0D) {
                ++par2;
            }

            renderer.aoLightValueScratchXYPN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 - 1, par4);
            renderer.aoLightValueScratchXZPN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3, par4 - 1);
            renderer.aoLightValueScratchXZPP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3, par4 + 1);
            renderer.aoLightValueScratchXYPP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 + 1, par4);
            renderer.aoBrightnessXYPN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 - 1, par4);
            renderer.aoBrightnessXZPN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4 - 1);
            renderer.aoBrightnessXZPP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4 + 1);
            renderer.aoBrightnessXYPP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 + 1, par4);
            flag3 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 + 1, par3 + 1, par4)];
            flag2 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 + 1, par3 - 1, par4)];
            flag5 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 + 1, par3, par4 + 1)];
            flag4 = Block.canBlockGrass[renderer.blockAccess.getBlockId(par2 + 1, par3, par4 - 1)];

            if (!flag2 && !flag4) {
                renderer.aoLightValueScratchXYZPNN = renderer.aoLightValueScratchXZPN;
                renderer.aoBrightnessXYZPNN = renderer.aoBrightnessXZPN;
            } else {
                renderer.aoLightValueScratchXYZPNN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 - 1, par4 - 1);
                renderer.aoBrightnessXYZPNN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 - 1, par4 - 1);
            }

            if (!flag2 && !flag5) {
                renderer.aoLightValueScratchXYZPNP = renderer.aoLightValueScratchXZPP;
                renderer.aoBrightnessXYZPNP = renderer.aoBrightnessXZPP;
            } else {
                renderer.aoLightValueScratchXYZPNP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 - 1, par4 + 1);
                renderer.aoBrightnessXYZPNP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 - 1, par4 + 1);
            }

            if (!flag3 && !flag4) {
                renderer.aoLightValueScratchXYZPPN = renderer.aoLightValueScratchXZPN;
                renderer.aoBrightnessXYZPPN = renderer.aoBrightnessXZPN;
            } else {
                renderer.aoLightValueScratchXYZPPN = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 + 1, par4 - 1);
                renderer.aoBrightnessXYZPPN = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 + 1, par4 - 1);
            }

            if (!flag3 && !flag5) {
                renderer.aoLightValueScratchXYZPPP = renderer.aoLightValueScratchXZPP;
                renderer.aoBrightnessXYZPPP = renderer.aoBrightnessXZPP;
            } else {
                renderer.aoLightValueScratchXYZPPP = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2, par3 + 1, par4 + 1);
                renderer.aoBrightnessXYZPPP = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 + 1, par4 + 1);
            }

            if (renderer.renderMaxX >= 1.0D) {
                --par2;
            }

            i1 = l;

            if (renderer.renderMaxX >= 1.0D || !renderer.blockAccess.isBlockOpaqueCube(par2 + 1, par3, par4)) {
                i1 = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3, par4);
            }

            f7 = par1Block.getAmbientOcclusionLightValue(renderer.blockAccess, par2 + 1, par3, par4);
            f3 = (renderer.aoLightValueScratchXYPN + renderer.aoLightValueScratchXYZPNP + f7 + renderer.aoLightValueScratchXZPP) / 4.0F;
            f4 = (renderer.aoLightValueScratchXYZPNN + renderer.aoLightValueScratchXYPN + renderer.aoLightValueScratchXZPN + f7) / 4.0F;
            f5 = (renderer.aoLightValueScratchXZPN + f7 + renderer.aoLightValueScratchXYZPPN + renderer.aoLightValueScratchXYPP) / 4.0F;
            f6 = (f7 + renderer.aoLightValueScratchXZPP + renderer.aoLightValueScratchXYPP + renderer.aoLightValueScratchXYZPPP) / 4.0F;
            renderer.brightnessTopLeft = renderer.getAoBrightness(renderer.aoBrightnessXYPN, renderer.aoBrightnessXYZPNP, renderer.aoBrightnessXZPP, i1);
            renderer.brightnessTopRight = renderer.getAoBrightness(renderer.aoBrightnessXZPP, renderer.aoBrightnessXYPP, renderer.aoBrightnessXYZPPP, i1);
            renderer.brightnessBottomRight = renderer.getAoBrightness(renderer.aoBrightnessXZPN, renderer.aoBrightnessXYZPPN, renderer.aoBrightnessXYPP, i1);
            renderer.brightnessBottomLeft = renderer.getAoBrightness(renderer.aoBrightnessXYZPNN, renderer.aoBrightnessXYPN, renderer.aoBrightnessXZPN, i1);

            if (flag1) {
                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = fRSou * 0.6F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = fGSou * 0.6F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = fBSou * 0.6F;
            } else {
                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = 0.6F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = 0.6F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = 0.6F;
            }

            renderer.colorRedTopLeft *= f3;
            renderer.colorGreenTopLeft *= f3;
            renderer.colorBlueTopLeft *= f3;
            renderer.colorRedBottomLeft *= f4;
            renderer.colorGreenBottomLeft *= f4;
            renderer.colorBlueBottomLeft *= f4;
            renderer.colorRedBottomRight *= f5;
            renderer.colorGreenBottomRight *= f5;
            renderer.colorBlueBottomRight *= f5;
            renderer.colorRedTopRight *= f6;
            renderer.colorGreenTopRight *= f6;
            renderer.colorBlueTopRight *= f6;
            if (TileEnt.South == -1) {
                setStandardTexture();
            } else {
                setGreyTexture();
            }
            renderer.renderFaceXPos(par1Block, (double) par2, (double) par3, (double) par4, southFace);

            if (RenderBlocks.fancyGrass && isGrass && colorSou == -1) {
                int col = renderer.blockAccess.getBiomeGenForCoords(par2, par4).getBiomeGrassColor();
                float f = (float) (col >> 16 & 255) / 255.0F;
                float f1 = (float) (col >> 8 & 255) / 255.0F;
                float f2 = (float) (col & 255) / 255.0F;

                renderer.colorRedTopLeft = renderer.colorRedBottomLeft = renderer.colorRedBottomRight = renderer.colorRedTopRight = f * 0.6F;
                renderer.colorGreenTopLeft = renderer.colorGreenBottomLeft = renderer.colorGreenBottomRight = renderer.colorGreenTopRight = f1 * 0.6F;
                renderer.colorBlueTopLeft = renderer.colorBlueBottomLeft = renderer.colorBlueBottomRight = renderer.colorBlueTopRight = f2 * 0.6F;

                renderer.colorRedTopLeft *= f3;
                renderer.colorGreenTopLeft *= f3;
                renderer.colorBlueTopLeft *= f3;
                renderer.colorRedBottomLeft *= f4;
                renderer.colorGreenBottomLeft *= f4;
                renderer.colorBlueBottomLeft *= f4;
                renderer.colorRedBottomRight *= f5;
                renderer.colorGreenBottomRight *= f5;
                renderer.colorBlueBottomRight *= f5;
                renderer.colorRedTopRight *= f6;
                renderer.colorGreenTopRight *= f6;
                renderer.colorBlueTopRight *= f6;

                renderer.renderFaceXPos(par1Block, (double) par2, (double) par3, (double) par4, BlockGrass.getIconSideOverlay());
            }

            flag = true;
        }
        setStandardTexture();
        renderer.enableAO = false;
        return flag;
    }

    public boolean renderStandardBlockWithColorMultiplier(Block par1Block, int par2, int par3, int par4, RenderBlocks renderer, TileEntityRGB TileEnt) {
        renderer.enableAO = false;
        Tessellator var8 = Tessellator.instance;
        boolean var9 = false;
        boolean isGrass = false;
        float var11 = 1.0F;
        float bottomMulti = 0.5F;
        float eastWestMulti = 0.8F;
        float northSouthMulti = 0.6F;

        int white = par1Block.colorMultiplier(renderer.blockAccess, par2, par3, par4);
        int colorTop = TileEnt.Top;
        int colorBot = TileEnt.Bottom;
        int colorNor = TileEnt.North;
        int colorSou = TileEnt.South;
        int colorEas = TileEnt.East;
        int colorWes = TileEnt.West;
        Block blockModel = Block.blocksList[TileEnt.blockId];
        Icon topFace = blockModel.getIcon(1, TileEnt.blockMetadata);
        Icon bottomFace = blockModel.getIcon(0, TileEnt.blockMetadata);
        Icon northFace = blockModel.getIcon(4, TileEnt.blockMetadata);
        Icon southFace = blockModel.getIcon(5, TileEnt.blockMetadata);
        Icon eastFace = blockModel.getIcon(2, TileEnt.blockMetadata);
        Icon westFace = blockModel.getIcon(3, TileEnt.blockMetadata);

        if (TileEnt.blockId == Block.grass.blockID)
            isGrass = true;

        if (TileEnt.Top == -1) {
            colorTop = white;
        }

        if (TileEnt.Bottom == -1) {
            colorBot = white;
        }

        if (TileEnt.North == -1) {
            colorNor = white;
        }

        if (TileEnt.South == -1) {
            colorSou = white;
        }

        if (TileEnt.East == -1) {
            colorEas = white;
        }

        if (TileEnt.West == -1) {
            colorWes = white;
        }

        int var26 = par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4);

        if (TileEnt.Bottom == -1) {
            setStandardTexture();
        } else {
            setGreyTexture();
        }
        var8.setBrightness(renderer.renderMinY > 0.0D ? var26 : par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 - 1, par4));
        var8.setColorOpaque_I(colorBot);
        renderBottomFace(par1Block, (double) par2, (double) par3, (double) par4, bottomFace, renderer);
        var9 = true;

        if (TileEnt.Top == -1) {
            setStandardTexture();
        } else {
            setGreyTexture();
        }
        var8.setBrightness(renderer.renderMaxY < 1.0D ? var26 : par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3 + 1, par4));
        var8.setColorOpaque_I(colorTop);
        renderTopFace(par1Block, (double) par2, (double) par3, (double) par4, topFace, renderer);
        var9 = true;

        if (isGrass && TileEnt.Top == -1) {

            var8.setColorOpaque_I(renderer.blockAccess.getBiomeGenForCoords(par2, par4).getBiomeGrassColor());
            renderTopFace(par1Block, (double) par2, (double) par3, (double) par4, topFace, renderer);
        }

        if (TileEnt.East == -1) {
            setStandardTexture();
        } else {
            setGreyTexture();
        }
        var8.setBrightness(renderer.renderMinZ > 0.0D ? var26 : par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4 - 1));
        var8.setColorOpaque_I(colorEas);
        renderEastFace(par1Block, (double) par2, (double) par3, (double) par4, eastFace, renderer);

        if (isGrass && TileEnt.East == -1 && RenderBlocks.fancyGrass) {
            var8.setColorOpaque_I(renderer.blockAccess.getBiomeGenForCoords(par2, par4).getBiomeGrassColor());
            renderEastFace(par1Block, (double) par2, (double) par3, (double) par4, BlockGrass.getIconSideOverlay(), renderer);
        }

        if (TileEnt.West == -1) {
            setStandardTexture();
        } else {
            setGreyTexture();
        }

        var8.setBrightness(renderer.renderMaxZ < 1.0D ? var26 : par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2, par3, par4 + 1));
        var8.setColorOpaque_I(colorWes);
        renderWestFace(par1Block, (double) par2, (double) par3, (double) par4, westFace, renderer);

        if (isGrass && TileEnt.West == -1 && RenderBlocks.fancyGrass) {
            var8.setColorOpaque_I(renderer.blockAccess.getBiomeGenForCoords(par2, par4).getBiomeGrassColor());
            renderWestFace(par1Block, (double) par2, (double) par3, (double) par4, BlockGrass.getIconSideOverlay(), renderer);
        }

        if (TileEnt.North == -1) {
            setStandardTexture();
        } else {
            setGreyTexture();
        }

        var8.setBrightness(renderer.renderMinX > 0.0D ? var26 : par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 - 1, par3, par4));
        var8.setColorOpaque_I(colorNor);
        renderNorthFace(par1Block, (double) par2, (double) par3, (double) par4, northFace, renderer);

        if (isGrass && TileEnt.North == -1 && RenderBlocks.fancyGrass) {
            var8.setColorOpaque_I(renderer.blockAccess.getBiomeGenForCoords(par2, par4).getBiomeGrassColor());
            renderNorthFace(par1Block, (double) par2, (double) par3, (double) par4, BlockGrass.getIconSideOverlay(), renderer);
        }

        if (TileEnt.South == -1) {
            setStandardTexture();
        } else {
            setGreyTexture();
        }

        var8.setBrightness(renderer.renderMaxX < 1.0D ? var26 : par1Block.getMixedBrightnessForBlock(renderer.blockAccess, par2 + 1, par3, par4));
        var8.setColorOpaque_I(colorSou);
        	renderSouthFace(par1Block, (double) par2, (double) par3, (double) par4, southFace, renderer);

        if (isGrass && TileEnt.South == -1 && RenderBlocks.fancyGrass) {
            var8.setColorOpaque_I(renderer.blockAccess.getBiomeGenForCoords(par2, par4).getBiomeGrassColor());
            renderSouthFace(par1Block, (double) par2, (double) par3, (double) par4, BlockGrass.getIconSideOverlay(), renderer);
        }
        setStandardTexture();

        return true;
    }

    @Override
    public boolean shouldRender3DInInventory() {

        return false;
    }

    @Override
    public int getRenderId() {

        return ClientProxy.woolRenderType;
    }

    /**
     * Renders the given texture to the bottom face of the block. Args: block, x, y, z, texture
     */
    public void renderBottomFace(Block par1Block, double par2, double par4, double par6, Icon par8Icon, RenderBlocks renderer) {
        Tessellator tessellator = Tessellator.instance;

        if (renderer.hasOverrideBlockTexture()) {
            par8Icon = renderer.overrideBlockTexture;
        }

        double d3 = (double) par8Icon.getInterpolatedU(renderer.renderMinX * 16.0D);
        double d4 = (double) par8Icon.getInterpolatedU(renderer.renderMaxX * 16.0D);
        double d5 = (double) par8Icon.getInterpolatedV(renderer.renderMinZ * 16.0D);
        double d6 = (double) par8Icon.getInterpolatedV(renderer.renderMaxZ * 16.0D);

        if (renderer.renderMinX < 0.0D || renderer.renderMaxX > 1.0D) {
            d3 = (double) par8Icon.getMinU();
            d4 = (double) par8Icon.getMaxU();
        }

        if (renderer.renderMinZ < 0.0D || renderer.renderMaxZ > 1.0D) {
            d5 = (double) par8Icon.getMinV();
            d6 = (double) par8Icon.getMaxV();
        }

        double d7 = d4;
        double d8 = d3;
        double d9 = d5;
        double d10 = d6;

        if (renderer.uvRotateBottom == 2) {
            d3 = (double) par8Icon.getInterpolatedU(renderer.renderMinZ * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMaxX * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(renderer.renderMaxZ * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMinX * 16.0D);
            d9 = d5;
            d10 = d6;
            d7 = d3;
            d8 = d4;
            d5 = d6;
            d6 = d9;
        } else if (renderer.uvRotateBottom == 1) {
            d3 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMaxZ * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(renderer.renderMinX * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMinZ * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(renderer.renderMaxX * 16.0D);
            d7 = d4;
            d8 = d3;
            d3 = d4;
            d4 = d8;
            d9 = d6;
            d10 = d5;
        } else if (renderer.uvRotateBottom == 3) {
            d3 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMinX * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMaxX * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMinZ * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMaxZ * 16.0D);
            d7 = d4;
            d8 = d3;
            d9 = d5;
            d10 = d6;
        }

        double d11 = par2 + renderer.renderMinX;
        double d12 = par2 + renderer.renderMaxX;
        double d13 = par4 + renderer.renderMinY;
        double d14 = par6 + renderer.renderMinZ;
        double d15 = par6 + renderer.renderMaxZ;

        if (renderer.enableAO) {
            tessellator.setColorOpaque_F(renderer.colorRedTopLeft, renderer.colorGreenTopLeft, renderer.colorBlueTopLeft);
            tessellator.setBrightness(renderer.brightnessTopLeft);
            tessellator.addVertexWithUV(d11, d13, d15, d8, d10);
            tessellator.setColorOpaque_F(renderer.colorRedBottomLeft, renderer.colorGreenBottomLeft, renderer.colorBlueBottomLeft);
            tessellator.setBrightness(renderer.brightnessBottomLeft);
            tessellator.addVertexWithUV(d11, d13, d14, d3, d5);
            tessellator.setColorOpaque_F(renderer.colorRedBottomRight, renderer.colorGreenBottomRight, renderer.colorBlueBottomRight);
            tessellator.setBrightness(renderer.brightnessBottomRight);
            tessellator.addVertexWithUV(d12, d13, d14, d7, d9);
            tessellator.setColorOpaque_F(renderer.colorRedTopRight, renderer.colorGreenTopRight, renderer.colorBlueTopRight);
            tessellator.setBrightness(renderer.brightnessTopRight);
            tessellator.addVertexWithUV(d12, d13, d15, d4, d6);
        } else {
            tessellator.addVertexWithUV(d11, d13, d15, d8, d10);
            tessellator.addVertexWithUV(d11, d13, d14, d3, d5);
            tessellator.addVertexWithUV(d12, d13, d14, d7, d9);
            tessellator.addVertexWithUV(d12, d13, d15, d4, d6);
        }

    }

    /**
     * Renders the given texture to the top face of the block. Args: block, x, y, z, texture
     */
    public void renderTopFace(Block par1Block, double par2, double par4, double par6, Icon par8Icon, RenderBlocks renderer) {
        Tessellator tessellator = Tessellator.instance;

        if (renderer.hasOverrideBlockTexture()) {
            par8Icon = renderer.overrideBlockTexture;
        }

        double d3 = (double) par8Icon.getInterpolatedU(renderer.renderMinX * 16.0D);
        double d4 = (double) par8Icon.getInterpolatedU(renderer.renderMaxX * 16.0D);
        double d5 = (double) par8Icon.getInterpolatedV(renderer.renderMinZ * 16.0D);
        double d6 = (double) par8Icon.getInterpolatedV(renderer.renderMaxZ * 16.0D);

        if (renderer.renderMinX < 0.0D || renderer.renderMaxX > 1.0D) {
            d3 = (double) par8Icon.getMinU();
            d4 = (double) par8Icon.getMaxU();
        }

        if (renderer.renderMinZ < 0.0D || renderer.renderMaxZ > 1.0D) {
            d5 = (double) par8Icon.getMinV();
            d6 = (double) par8Icon.getMaxV();
        }

        double d7 = d4;
        double d8 = d3;
        double d9 = d5;
        double d10 = d6;

        if (renderer.uvRotateTop == 1) {
            d3 = (double) par8Icon.getInterpolatedU(renderer.renderMinZ * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMaxX * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(renderer.renderMaxZ * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMinX * 16.0D);
            d9 = d5;
            d10 = d6;
            d7 = d3;
            d8 = d4;
            d5 = d6;
            d6 = d9;
        } else if (renderer.uvRotateTop == 2) {
            d3 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMaxZ * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(renderer.renderMinX * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMinZ * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(renderer.renderMaxX * 16.0D);
            d7 = d4;
            d8 = d3;
            d3 = d4;
            d4 = d8;
            d9 = d6;
            d10 = d5;
        } else if (renderer.uvRotateTop == 3) {
            d3 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMinX * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMaxX * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMinZ * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMaxZ * 16.0D);
            d7 = d4;
            d8 = d3;
            d9 = d5;
            d10 = d6;
        }

        double d11 = par2 + renderer.renderMinX;
        double d12 = par2 + renderer.renderMaxX;
        double d13 = par4 + renderer.renderMaxY;
        double d14 = par6 + renderer.renderMinZ;
        double d15 = par6 + renderer.renderMaxZ;

        if (renderer.enableAO) {
            tessellator.setColorOpaque_F(renderer.colorRedTopLeft, renderer.colorGreenTopLeft, renderer.colorBlueTopLeft);
            tessellator.setBrightness(renderer.brightnessTopLeft);
            tessellator.addVertexWithUV(d12, d13, d15, d4, d6);
            tessellator.setColorOpaque_F(renderer.colorRedBottomLeft, renderer.colorGreenBottomLeft, renderer.colorBlueBottomLeft);
            tessellator.setBrightness(renderer.brightnessBottomLeft);
            tessellator.addVertexWithUV(d12, d13, d14, d7, d9);
            tessellator.setColorOpaque_F(renderer.colorRedBottomRight, renderer.colorGreenBottomRight, renderer.colorBlueBottomRight);
            tessellator.setBrightness(renderer.brightnessBottomRight);
            tessellator.addVertexWithUV(d11, d13, d14, d3, d5);
            tessellator.setColorOpaque_F(renderer.colorRedTopRight, renderer.colorGreenTopRight, renderer.colorBlueTopRight);
            tessellator.setBrightness(renderer.brightnessTopRight);
            tessellator.addVertexWithUV(d11, d13, d15, d8, d10);
        } else {
            tessellator.addVertexWithUV(d12, d13, d15, d4, d6);
            tessellator.addVertexWithUV(d12, d13, d14, d7, d9);
            tessellator.addVertexWithUV(d11, d13, d14, d3, d5);
            tessellator.addVertexWithUV(d11, d13, d15, d8, d10);
        }
    }

    /**
     * Renders the given texture to the east (z-negative) face of the block. Args: block, x, y, z, texture
     */
    public void renderEastFace(Block par1Block, double par2, double par4, double par6, Icon par8Icon, RenderBlocks renderer) {
        Tessellator tessellator = Tessellator.instance;

        if (renderer.hasOverrideBlockTexture()) {
            par8Icon = renderer.overrideBlockTexture;
        }

        double d3 = (double) par8Icon.getInterpolatedU(renderer.renderMinX * 16.0D);
        double d4 = (double) par8Icon.getInterpolatedU(renderer.renderMaxX * 16.0D);
        double d5 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMaxY * 16.0D);
        double d6 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMinY * 16.0D);
        double d7;

        if (renderer.flipTexture) {
            d7 = d3;
            d3 = d4;
            d4 = d7;
        }

        if (renderer.renderMinX < 0.0D || renderer.renderMaxX > 1.0D) {
            d3 = (double) par8Icon.getMinU();
            d4 = (double) par8Icon.getMaxU();
        }

        if (renderer.renderMinY < 0.0D || renderer.renderMaxY > 1.0D) {
            d5 = (double) par8Icon.getMinV();
            d6 = (double) par8Icon.getMaxV();
        }

        d7 = d4;
        double d8 = d3;
        double d9 = d5;
        double d10 = d6;

        if (renderer.uvRotateEast == 2) {
            d3 = (double) par8Icon.getInterpolatedU(renderer.renderMinY * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMinX * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(renderer.renderMaxY * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMaxX * 16.0D);
            d9 = d5;
            d10 = d6;
            d7 = d3;
            d8 = d4;
            d5 = d6;
            d6 = d9;
        } else if (renderer.uvRotateEast == 1) {
            d3 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMaxY * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(renderer.renderMaxX * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMinY * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(renderer.renderMinX * 16.0D);
            d7 = d4;
            d8 = d3;
            d3 = d4;
            d4 = d8;
            d9 = d6;
            d10 = d5;
        } else if (renderer.uvRotateEast == 3) {
            d3 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMinX * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMaxX * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(renderer.renderMaxY * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(renderer.renderMinY * 16.0D);
            d7 = d4;
            d8 = d3;
            d9 = d5;
            d10 = d6;
        }

        double d11 = par2 + renderer.renderMinX;
        double d12 = par2 + renderer.renderMaxX;
        double d13 = par4 + renderer.renderMinY;
        double d14 = par4 + renderer.renderMaxY;
        double d15 = par6 + renderer.renderMinZ;

        if (renderer.enableAO) {
            tessellator.setColorOpaque_F(renderer.colorRedTopLeft, renderer.colorGreenTopLeft, renderer.colorBlueTopLeft);
            tessellator.setBrightness(renderer.brightnessTopLeft);
            tessellator.addVertexWithUV(d11, d14, d15, d7, d9);
            tessellator.setColorOpaque_F(renderer.colorRedBottomLeft, renderer.colorGreenBottomLeft, renderer.colorBlueBottomLeft);
            tessellator.setBrightness(renderer.brightnessBottomLeft);
            tessellator.addVertexWithUV(d12, d14, d15, d3, d5);
            tessellator.setColorOpaque_F(renderer.colorRedBottomRight, renderer.colorGreenBottomRight, renderer.colorBlueBottomRight);
            tessellator.setBrightness(renderer.brightnessBottomRight);
            tessellator.addVertexWithUV(d12, d13, d15, d8, d10);
            tessellator.setColorOpaque_F(renderer.colorRedTopRight, renderer.colorGreenTopRight, renderer.colorBlueTopRight);
            tessellator.setBrightness(renderer.brightnessTopRight);
            tessellator.addVertexWithUV(d11, d13, d15, d4, d6);
        } else {
            tessellator.addVertexWithUV(d11, d14, d15, d7, d9);
            tessellator.addVertexWithUV(d12, d14, d15, d3, d5);
            tessellator.addVertexWithUV(d12, d13, d15, d8, d10);
            tessellator.addVertexWithUV(d11, d13, d15, d4, d6);
        }
    }

    /**
     * Renders the given texture to the west (z-positive) face of the block. Args: block, x, y, z, texture
     */
    public void renderWestFace(Block par1Block, double par2, double par4, double par6, Icon par8Icon, RenderBlocks renderer) {
        Tessellator tessellator = Tessellator.instance;

        if (renderer.hasOverrideBlockTexture()) {
            par8Icon = renderer.overrideBlockTexture;
        }

        double d3 = (double) par8Icon.getInterpolatedU(renderer.renderMinX * 16.0D);
        double d4 = (double) par8Icon.getInterpolatedU(renderer.renderMaxX * 16.0D);
        double d5 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMaxY * 16.0D);
        double d6 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMinY * 16.0D);
        double d7;

        if (renderer.flipTexture) {
            d7 = d3;
            d3 = d4;
            d4 = d7;
        }

        if (renderer.renderMinX < 0.0D || renderer.renderMaxX > 1.0D) {
            d3 = (double) par8Icon.getMinU();
            d4 = (double) par8Icon.getMaxU();
        }

        if (renderer.renderMinY < 0.0D || renderer.renderMaxY > 1.0D) {
            d5 = (double) par8Icon.getMinV();
            d6 = (double) par8Icon.getMaxV();
        }

        d7 = d4;
        double d8 = d3;
        double d9 = d5;
        double d10 = d6;

        if (renderer.uvRotateWest == 1) {
            d3 = (double) par8Icon.getInterpolatedU(renderer.renderMinY * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMinX * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(renderer.renderMaxY * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMaxX * 16.0D);
            d9 = d5;
            d10 = d6;
            d7 = d3;
            d8 = d4;
            d5 = d6;
            d6 = d9;
        } else if (renderer.uvRotateWest == 2) {
            d3 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMaxY * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(renderer.renderMinX * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMinY * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(renderer.renderMaxX * 16.0D);
            d7 = d4;
            d8 = d3;
            d3 = d4;
            d4 = d8;
            d9 = d6;
            d10 = d5;
        } else if (renderer.uvRotateWest == 3) {
            d3 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMinX * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMaxX * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(renderer.renderMaxY * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(renderer.renderMinY * 16.0D);
            d7 = d4;
            d8 = d3;
            d9 = d5;
            d10 = d6;
        }

        double d11 = par2 + renderer.renderMinX;
        double d12 = par2 + renderer.renderMaxX;
        double d13 = par4 + renderer.renderMinY;
        double d14 = par4 + renderer.renderMaxY;
        double d15 = par6 + renderer.renderMaxZ;

        if (renderer.enableAO) {
            tessellator.setColorOpaque_F(renderer.colorRedTopLeft, renderer.colorGreenTopLeft, renderer.colorBlueTopLeft);
            tessellator.setBrightness(renderer.brightnessTopLeft);
            tessellator.addVertexWithUV(d11, d14, d15, d3, d5);
            tessellator.setColorOpaque_F(renderer.colorRedBottomLeft, renderer.colorGreenBottomLeft, renderer.colorBlueBottomLeft);
            tessellator.setBrightness(renderer.brightnessBottomLeft);
            tessellator.addVertexWithUV(d11, d13, d15, d8, d10);
            tessellator.setColorOpaque_F(renderer.colorRedBottomRight, renderer.colorGreenBottomRight, renderer.colorBlueBottomRight);
            tessellator.setBrightness(renderer.brightnessBottomRight);
            tessellator.addVertexWithUV(d12, d13, d15, d4, d6);
            tessellator.setColorOpaque_F(renderer.colorRedTopRight, renderer.colorGreenTopRight, renderer.colorBlueTopRight);
            tessellator.setBrightness(renderer.brightnessTopRight);
            tessellator.addVertexWithUV(d12, d14, d15, d7, d9);
        } else {
            tessellator.addVertexWithUV(d11, d14, d15, d3, d5);
            tessellator.addVertexWithUV(d11, d13, d15, d8, d10);
            tessellator.addVertexWithUV(d12, d13, d15, d4, d6);
            tessellator.addVertexWithUV(d12, d14, d15, d7, d9);
        }
    }

    /**
     * Renders the given texture to the north (x-negative) face of the block. Args: block, x, y, z, texture
     */
    public void renderNorthFace(Block par1Block, double par2, double par4, double par6, Icon par8Icon, RenderBlocks renderer) {
        Tessellator tessellator = Tessellator.instance;

        if (renderer.hasOverrideBlockTexture()) {
            par8Icon = renderer.overrideBlockTexture;
        }

        double d3 = (double) par8Icon.getInterpolatedU(renderer.renderMinZ * 16.0D);
        double d4 = (double) par8Icon.getInterpolatedU(renderer.renderMaxZ * 16.0D);
        double d5 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMaxY * 16.0D);
        double d6 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMinY * 16.0D);
        double d7;

        if (renderer.flipTexture) {
            d7 = d3;
            d3 = d4;
            d4 = d7;
        }

        if (renderer.renderMinZ < 0.0D || renderer.renderMaxZ > 1.0D) {
            d3 = (double) par8Icon.getMinU();
            d4 = (double) par8Icon.getMaxU();
        }

        if (renderer.renderMinY < 0.0D || renderer.renderMaxY > 1.0D) {
            d5 = (double) par8Icon.getMinV();
            d6 = (double) par8Icon.getMaxV();
        }

        d7 = d4;
        double d8 = d3;
        double d9 = d5;
        double d10 = d6;

        if (renderer.uvRotateNorth == 1) {
            d3 = (double) par8Icon.getInterpolatedU(renderer.renderMinY * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMaxZ * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(renderer.renderMaxY * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMinZ * 16.0D);
            d9 = d5;
            d10 = d6;
            d7 = d3;
            d8 = d4;
            d5 = d6;
            d6 = d9;
        } else if (renderer.uvRotateNorth == 2) {
            d3 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMaxY * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(renderer.renderMinZ * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMinY * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(renderer.renderMaxZ * 16.0D);
            d7 = d4;
            d8 = d3;
            d3 = d4;
            d4 = d8;
            d9 = d6;
            d10 = d5;
        } else if (renderer.uvRotateNorth == 3) {
            d3 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMinZ * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMaxZ * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(renderer.renderMaxY * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(renderer.renderMinY * 16.0D);
            d7 = d4;
            d8 = d3;
            d9 = d5;
            d10 = d6;
        }

        double d11 = par2 + renderer.renderMinX;
        double d12 = par4 + renderer.renderMinY;
        double d13 = par4 + renderer.renderMaxY;
        double d14 = par6 + renderer.renderMinZ;
        double d15 = par6 + renderer.renderMaxZ;

        if (renderer.enableAO) {
            tessellator.setColorOpaque_F(renderer.colorRedTopLeft, renderer.colorGreenTopLeft, renderer.colorBlueTopLeft);
            tessellator.setBrightness(renderer.brightnessTopLeft);
            tessellator.addVertexWithUV(d11, d13, d15, d7, d9);
            tessellator.setColorOpaque_F(renderer.colorRedBottomLeft, renderer.colorGreenBottomLeft, renderer.colorBlueBottomLeft);
            tessellator.setBrightness(renderer.brightnessBottomLeft);
            tessellator.addVertexWithUV(d11, d13, d14, d3, d5);
            tessellator.setColorOpaque_F(renderer.colorRedBottomRight, renderer.colorGreenBottomRight, renderer.colorBlueBottomRight);
            tessellator.setBrightness(renderer.brightnessBottomRight);
            tessellator.addVertexWithUV(d11, d12, d14, d8, d10);
            tessellator.setColorOpaque_F(renderer.colorRedTopRight, renderer.colorGreenTopRight, renderer.colorBlueTopRight);
            tessellator.setBrightness(renderer.brightnessTopRight);
            tessellator.addVertexWithUV(d11, d12, d15, d4, d6);
        } else {
            tessellator.addVertexWithUV(d11, d13, d15, d7, d9);
            tessellator.addVertexWithUV(d11, d13, d14, d3, d5);
            tessellator.addVertexWithUV(d11, d12, d14, d8, d10);
            tessellator.addVertexWithUV(d11, d12, d15, d4, d6);
        }
    }

    /**
     * Renders the given texture to the south (x-positive) face of the block. Args: block, x, y, z, texture
     */
    public void renderSouthFace(Block par1Block, double par2, double par4, double par6, Icon par8Icon, RenderBlocks renderer) {
        Tessellator tessellator = Tessellator.instance;

        if (renderer.hasOverrideBlockTexture()) {
            par8Icon = renderer.overrideBlockTexture;
        }

        double d3 = (double) par8Icon.getInterpolatedU(renderer.renderMinZ * 16.0D);
        double d4 = (double) par8Icon.getInterpolatedU(renderer.renderMaxZ * 16.0D);
        double d5 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMaxY * 16.0D);
        double d6 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMinY * 16.0D);
        double d7;

        if (renderer.flipTexture) {
            d7 = d3;
            d3 = d4;
            d4 = d7;
        }

        if (renderer.renderMinZ < 0.0D || renderer.renderMaxZ > 1.0D) {
            d3 = (double) par8Icon.getMinU();
            d4 = (double) par8Icon.getMaxU();
        }

        if (renderer.renderMinY < 0.0D || renderer.renderMaxY > 1.0D) {
            d5 = (double) par8Icon.getMinV();
            d6 = (double) par8Icon.getMaxV();
        }

        d7 = d4;
        double d8 = d3;
        double d9 = d5;
        double d10 = d6;

        if (renderer.uvRotateSouth == 2) {
            d3 = (double) par8Icon.getInterpolatedU(renderer.renderMinY * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMinZ * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(renderer.renderMaxY * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(16.0D - renderer.renderMaxZ * 16.0D);
            d9 = d5;
            d10 = d6;
            d7 = d3;
            d8 = d4;
            d5 = d6;
            d6 = d9;
        } else if (renderer.uvRotateSouth == 1) {
            d3 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMaxY * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(renderer.renderMaxZ * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMinY * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(renderer.renderMinZ * 16.0D);
            d7 = d4;
            d8 = d3;
            d3 = d4;
            d4 = d8;
            d9 = d6;
            d10 = d5;
        } else if (renderer.uvRotateSouth == 3) {
            d3 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMinZ * 16.0D);
            d4 = (double) par8Icon.getInterpolatedU(16.0D - renderer.renderMaxZ * 16.0D);
            d5 = (double) par8Icon.getInterpolatedV(renderer.renderMaxY * 16.0D);
            d6 = (double) par8Icon.getInterpolatedV(renderer.renderMinY * 16.0D);
            d7 = d4;
            d8 = d3;
            d9 = d5;
            d10 = d6;
        }

        double d11 = par2 + renderer.renderMaxX;
        double d12 = par4 + renderer.renderMinY;
        double d13 = par4 + renderer.renderMaxY;
        double d14 = par6 + renderer.renderMinZ;
        double d15 = par6 + renderer.renderMaxZ;

        if (renderer.enableAO) {
            tessellator.setColorOpaque_F(renderer.colorRedTopLeft, renderer.colorGreenTopLeft, renderer.colorBlueTopLeft);
            tessellator.setBrightness(renderer.brightnessTopLeft);
            tessellator.addVertexWithUV(d11, d12, d15, d8, d10);
            tessellator.setColorOpaque_F(renderer.colorRedBottomLeft, renderer.colorGreenBottomLeft, renderer.colorBlueBottomLeft);
            tessellator.setBrightness(renderer.brightnessBottomLeft);
            tessellator.addVertexWithUV(d11, d12, d14, d4, d6);
            tessellator.setColorOpaque_F(renderer.colorRedBottomRight, renderer.colorGreenBottomRight, renderer.colorBlueBottomRight);
            tessellator.setBrightness(renderer.brightnessBottomRight);
            tessellator.addVertexWithUV(d11, d13, d14, d7, d9);
            tessellator.setColorOpaque_F(renderer.colorRedTopRight, renderer.colorGreenTopRight, renderer.colorBlueTopRight);
            tessellator.setBrightness(renderer.brightnessTopRight);
            tessellator.addVertexWithUV(d11, d13, d15, d3, d5);
        } else {
            tessellator.addVertexWithUV(d11, d12, d15, d8, d10);
            tessellator.addVertexWithUV(d11, d12, d14, d4, d6);
            tessellator.addVertexWithUV(d11, d13, d14, d7, d9);
            tessellator.addVertexWithUV(d11, d13, d15, d3, d5);
        }
    }

}
