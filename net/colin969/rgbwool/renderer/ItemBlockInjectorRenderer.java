package net.colin969.rgbwool.renderer;

import org.lwjgl.opengl.GL11;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.IItemRenderer;

public class ItemBlockInjectorRenderer implements IItemRenderer{

	public static RenderItem renderItem = new RenderItem();
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return type == ItemRenderType.INVENTORY;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
		return false;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
        FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
        NBTTagCompound nbt = item.getTagCompound();
        
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDepthMask(false);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawing(GL11.GL_QUADS);
        if(nbt != null)
        	tessellator.setColorOpaque_I(nbt.getInteger("color"));
        else
        	tessellator.setColorOpaque_I(16777215);
        tessellator.addVertex(0, 0, 0);
        tessellator.addVertex(0, 16, 0);
        tessellator.addVertex(16, 16, 0);
        tessellator.addVertex(16, 0, 0);
        tessellator.draw();
        
        GL11.glDepthMask(true);
        GL11.glDisable(GL11.GL_BLEND);
        
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        renderItem.renderIcon(0, 0, item.getIconIndex(), 16, 16);
        if(nbt.getInteger("Mode") == 0){
        	fontRenderer.drawString("B", 10, 0, 0xFFFFFF, true);
        } else {
        	fontRenderer.drawString("S", 10, 0, 0xFFFFFF, true);
        }
        
	}

}
