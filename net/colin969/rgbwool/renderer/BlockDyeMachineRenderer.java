package net.colin969.rgbwool.renderer;

import org.lwjgl.opengl.GL11;

import net.colin969.rgbwool.block.BlockDyeMachine;
import net.colin969.rgbwool.entity.TileEntityDyeMachine;
import net.colin969.rgbwool.entity.TileEntitySpecialFactory;
import net.colin969.rgbwool.model.ModelFactoryBlock;
import net.colin969.rgbwool.proxy.ClientProxy;
import net.colin969.rgbwool.proxy.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.Entity;
import net.minecraft.world.IBlockAccess;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

public class BlockDyeMachineRenderer implements ISimpleBlockRenderingHandler{

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID,
			RenderBlocks renderer) {
		
		
			Tessellator tessellator = Tessellator.instance;
			
			if(metadata != 8){
					
				Block handlerBlock = block;
				block.setBlockBoundsForItemRender();
				GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
				tessellator.startDrawingQuads();
				tessellator.setNormal(0F, +1.0F, 0F);
				renderer.setRenderBounds(0, 0, 0, 1, 1, 1);
				renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, handlerBlock.getIcon(1, metadata));
				tessellator.draw();
				tessellator.startDrawingQuads();
				tessellator.setNormal(0F, -1.0F, 0F);
				renderer.setRenderBounds(0, 0, 0, 1, 1, 1);
				renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, handlerBlock.getIcon(0, metadata));
				tessellator.draw();
				tessellator.startDrawingQuads();
				tessellator.setNormal(-1.0F, 0F, 0F);
				renderer.setRenderBounds(0, 0, 0, 1, 1, 1);
				renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, handlerBlock.getIcon(2, metadata));
				tessellator.draw();
				tessellator.startDrawingQuads();
				tessellator.setNormal(0F, 0F, -1.0F);
				renderer.setRenderBounds(0, 0, 0, 1, 1, 1);
				renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, handlerBlock.getIcon(3, metadata));
				tessellator.draw();
				tessellator.startDrawingQuads();
				tessellator.setNormal(+1.0F, 0F, 0F);
				renderer.setRenderBounds(0, 0, 0, 1, 1, 1);
				renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, handlerBlock.getIcon(4, metadata));
				tessellator.draw();
				tessellator.startDrawingQuads();
				tessellator.setNormal(0F, 0F, +1.0F);
				renderer.setRenderBounds(0, 0, 0, 1, 1, 1);
				renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, handlerBlock.getIcon(5, metadata));
				tessellator.draw();
				GL11.glTranslatef(0.5F, 0.5F, 0.5F);
				
			} else {
			
				ModelFactoryBlock model = new ModelFactoryBlock();
				
				FMLClientHandler.instance().getClient().renderEngine.bindTexture("/mods/RGBBlocks/textures/blocks/ModelFactoryBlock.png");
				
		        GL11.glPushMatrix(); //start
		        GL11.glTranslatef(0, 0.075F, 0); //size
		        GL11.glRotatef(180, 1, 0, 0);
		        GL11.glRotatef(-90, 0, 1, 0);
		        boolean[] sides = {true,true,true,true,true,true};
		        model.render(sides);
		        GL11.glPopMatrix(); //end
			
		        FMLClientHandler.instance().getClient().renderEngine.bindTexture("/terrain.png");
			}
		
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z,
			Block block, int modelId, RenderBlocks renderer) {
		
		if(world.getBlockMetadata(x, y, z) != 8){
			renderer.renderStandardBlock(block, x, y, z);
			TileEntityDyeMachine te = (TileEntityDyeMachine) world.getBlockTileEntity(x, y, z);
			int color;
			double renderPercentage;
			if(te != null){
				renderPercentage =  (te.getDye() / te.getMaxDye());
				color = te.color;
			} else {
				renderPercentage = 0;
				color = 16777215;
			}
	        Block handlerBlock = block;
			ClientProxy.DyeBlockRenderStage = 0;
			renderer.setRenderBounds(0.001, 0.001, 0.001, 0.999, 0.999, 0.999);
			renderer.setOverrideBlockTexture(handlerBlock.getIcon(0, 0));
			renderer.renderStandardBlock(block, x, y, z);
			ClientProxy.DyeBlockRenderStage = 1;
			renderer.setRenderBounds(0, 0, 0, 1, 1, 1);
			renderer.clearOverrideBlockTexture();
			GL11.glPushMatrix();
			switch(world.getBlockMetadata(x, y, z)){
			case 2:
				renderer.setRenderBounds(0.125, 0.5625, -0.001, 0.375, 0.5625 + (0.3125 * renderPercentage), 1.001);
				renderer.renderFaceZNeg(block, x, y, z, ((BlockDyeMachine) block).getTankIcon());
				break;
			case 5:
				renderer.setRenderBounds(-0.001, 0.5625, 0.125, 1.001, 0.5625 + (0.3125 * renderPercentage), 0.375);
				renderer.renderFaceXPos(block, x, y, z, ((BlockDyeMachine) block).getTankIcon());
				break;
			case 3:
				renderer.setRenderBounds(0.625, 0.5625, -0.001, 0.875, 0.5625 + (0.3125 * renderPercentage), 1.001);
				renderer.renderFaceZPos(block, x, y, z, ((BlockDyeMachine) block).getTankIcon());
				break;
			case 4:
				renderer.setRenderBounds(-0.001, 0.5625, 0.625, 1.001, 0.5625 + (0.3125 * renderPercentage), 0.875);
				renderer.renderFaceXNeg(block, x, y, z, ((BlockDyeMachine) block).getTankIcon());
				break;
				
			}
			Tessellator.instance.setColorOpaque_I(color);
			renderer.setRenderBounds(0, 0, 0, 1, 1, 1);
			renderer.renderFaceYPos(block, x, y, z, ((BlockDyeMachine) block).getBlockOverlay());
			GL11.glPopMatrix();
			ClientProxy.DyeBlockRenderStage = 0;
			renderer.clearOverrideBlockTexture();
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean shouldRender3DInInventory() {
		return true;
	}

	@Override
	public int getRenderId() {
		return CommonProxy.dyeRenderType;
	}

}
