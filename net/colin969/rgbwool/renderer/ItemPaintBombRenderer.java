package net.colin969.rgbwool.renderer;

import net.colin969.rgbwool.item.ItemPaintBomb;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraftforge.client.IItemRenderer;

public class ItemPaintBombRenderer implements IItemRenderer{

	public static RenderItem renderItem = new RenderItem();
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return type == type.INVENTORY ? true : false;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
			ItemRendererHelper helper) {
		return false;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		if(type == type.INVENTORY){
			renderItem.renderIcon(0, 0, item.getIconIndex(), 16, 16);
			int color = item.getTagCompound().getInteger("color");
			Tessellator.instance.setColorOpaque_I(color);
			
	        Tessellator tessellator = Tessellator.instance;
	        tessellator.startDrawingQuads();
	        tessellator.setColorOpaque_I(color);
	        
	        Icon icon = ((ItemPaintBomb) item.getItem()).getIconOverlay();
	        
	        tessellator.addVertexWithUV((double)(0 + 0), (double)(0 + 16), renderItem.zLevel, (double)icon.getMinU(), (double)icon.getMaxV());
	        tessellator.addVertexWithUV((double)(0 + 16), (double)(0 + 16), renderItem.zLevel, (double)icon.getMaxU(), (double)icon.getMaxV());
	        tessellator.addVertexWithUV((double)(0 + 16), (double)(0 + 0), renderItem.zLevel, (double)icon.getMaxU(), (double)icon.getMinV());
	        tessellator.addVertexWithUV((double)(0 + 0), (double)(0 + 0), renderItem.zLevel, (double)icon.getMinU(), (double)icon.getMinV());
	        tessellator.draw();
			
			Tessellator.instance.setColorOpaque_I(16777215);
		}
	}
	
}
