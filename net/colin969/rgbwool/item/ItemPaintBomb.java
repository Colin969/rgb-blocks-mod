package net.colin969.rgbwool.item;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.colin969.rgbwool.entity.EntityPaintBomb;
import net.colin969.rgbwool.lang.LocalizationHandler;
import net.colin969.rgbwool.lang.LocalizationHelper;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionHelper;
import net.minecraft.util.Icon;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class ItemPaintBomb extends ItemPotion{

	private Icon iconOverlay;
	
	public ItemPaintBomb(int par1) {
		super(par1);
		this.setMaxStackSize(16);
	}
	
	@Override
	public void registerIcons(IconRegister ir){
		this.itemIcon = ir.registerIcon("potion_splash");
		this.iconOverlay = ir.registerIcon("potion_contents");
	}
	
	@Override
	public Icon getIcon(ItemStack par3ItemStack, int j1){
		return this.itemIcon;
	}
	
	@Override
    public Icon getIconFromDamage(int par1)
    {
        return this.itemIcon;
    }
	
    public Icon getIconFromDamageForRenderPass(int par1, int par2)
    {
        return this.itemIcon;
    }
	
	@Override
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
           if (!par3EntityPlayer.capabilities.isCreativeMode)
           {
               --par1ItemStack.stackSize;
           }

           par2World.playSoundAtEntity(par3EntityPlayer, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

           if (!par2World.isRemote)
           {
               par2World.spawnEntityInWorld(new EntityPaintBomb(par2World, par3EntityPlayer, par1ItemStack));
           }

           return par1ItemStack;
    }
	
	@Override
    public String getItemDisplayName(ItemStack par1ItemStack)
    {
		return LocalizationHelper.getLocalizedString("item.paintbomb");
    }
	
    @Override
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
        NBTTagCompound nbt = par1ItemStack.getTagCompound();
        if (nbt != null) {
            par3List.add("\247c" + LocalizationHelper.getLocalizedString("color.red") + ": " + String.valueOf((nbt.getInteger("color") >> 16) & 0xFF));
            par3List.add("\247a" + LocalizationHelper.getLocalizedString("color.green") + ": " + String.valueOf((nbt.getInteger("color") >> 8) & 0xFF));
            par3List.add("\2479" + LocalizationHelper.getLocalizedString("color.blue") + ": " + String.valueOf((nbt.getInteger("color")) & 0xFF));
        } else {
            par3List.add("Red: " + "255");
            par3List.add("Green: " + "255");
            par3List.add("Blue: " + "255");
        }
    }
	
    @SideOnly(Side.CLIENT)
    @Override
    public int getColorFromDamage(int par1)
    {
        return 16777215;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public int getColorFromItemStack(ItemStack par1ItemStack, int par2)
    {
        NBTTagCompound nbt = par1ItemStack.getTagCompound();
        if(nbt != null){

        } else {
            nbt = new NBTTagCompound();
            nbt.setInteger("color", 16777215);
            par1ItemStack.setTagCompound(nbt);
        }
        return 16777215;
        
    }
    
    @Override
    public void getSubItems(int par1, CreativeTabs ct, List list){
    	ItemStack item = new ItemStack(this);
    	NBTTagCompound nbt = item.getTagCompound();
    	if(nbt != null){
    		nbt.setInteger("color", 16777215);
    	} else {
    		nbt = new NBTTagCompound();
    		nbt.setInteger("color", 16777215);
    	}
    	item.setTagCompound(nbt);
    	list.add(item);
    }

	public Icon getIconOverlay() {
		return iconOverlay;
	}

}
