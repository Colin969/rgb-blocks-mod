package net.colin969.rgbwool.item;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.colin969.rgbwool.ModProperties;
import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.entity.TileEntityRGB;
import net.colin969.rgbwool.lang.LocalizationHelper;
import net.minecraft.block.Block;
import net.minecraft.block.BlockHalfSlab;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class ItemBlockInjector extends Item {

    // Sorted in groups, 1 group = 1 Metadata. E.G {R,G,B, R,G,B}
    private int[] woolCode = { 221, 221, 211, 219, 125, 62, 179, 80, 188, 107, 138, 201, 177, 166, 39, 65, 174, 56, 208, 174, 56, 64, 64, 64, 154, 161, 161, 46, 110, 137, 126, 61, 181, 46, 56, 141, 79, 50, 31, 53, 70, 27, 150, 52, 48, 25, 22, 22 };
    
    public ItemBlockInjector(int par1, int i) {
        super(par1);
        this.setNoRepair();
        this.setMaxDamage(i);
    }

    @Override
    public void registerIcons(IconRegister iconRegister) {
        itemIcon = iconRegister.registerIcon(RGBBlock.MODID + ":Injector");
    }

    @Override
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
        NBTTagCompound nbt = par1ItemStack.getTagCompound();
        if (nbt != null) {
            par3List.add("\247c" + LocalizationHelper.getLocalizedString("color.red") + ": " + String.valueOf((nbt.getInteger("color") >> 16) & 0xFF));
            par3List.add("\247a" + LocalizationHelper.getLocalizedString("color.green") + ": " + String.valueOf((nbt.getInteger("color") >> 8) & 0xFF));
            par3List.add("\2479" + LocalizationHelper.getLocalizedString("color.blue") + ": " + String.valueOf((nbt.getInteger("color")) & 0xFF));
        } else {
            par3List.add("Red: " + "255");
            par3List.add("Green: " + "255");
            par3List.add("Blue: " + "255");
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public int getColorFromItemStack(ItemStack par1ItemStack, int par2) {
        NBTTagCompound nbt = par1ItemStack.getTagCompound();
        if (nbt == null) {
            nbt = new NBTTagCompound();
            nbt.setInteger("color", 16777215);
            nbt.setInteger("Mode", 0);
            par1ItemStack.setTagCompound(nbt);
        }

        return 0xFFFFFF;
    }

    public void setColor(ItemStack stack, int color) {
        if (!stack.hasTagCompound()) {
            stack.setTagCompound(new NBTTagCompound());
        }
        NBTTagCompound cmpound = stack.getTagCompound();
        cmpound.setInteger("color", color);
    }

    public String getTwoCharHex(int num) {
        if (num < 16) {
            return "0" + Integer.toHexString(num);
        } else {
            return Integer.toHexString(num);
        }
    }

    @Override
    public String getItemDisplayName(ItemStack par1ItemStack) {
    	NBTTagCompound nbt = par1ItemStack.getTagCompound();
    	if(nbt == null){
	    	if(par1ItemStack.getMaxDamage() == 2000)
	    		return LocalizationHelper.getLocalizedString("item.block_injector");
	    	else
	    		return LocalizationHelper.getLocalizedString("item.block_injector") + " V" + String.valueOf(par1ItemStack.getMaxDamage() / 2000);
    	} else if(!nbt.getString("label").equals("")){
    		return nbt.getString("label");
    	} else {
	    	if(par1ItemStack.getMaxDamage() == 2000)
	    		return LocalizationHelper.getLocalizedString("item.block_injector");
	    	else
	    		return LocalizationHelper.getLocalizedString("item.block_injector") + " V" + String.valueOf(par1ItemStack.getMaxDamage() / 2000);
    	}
    }
    
    @Override
    public ItemStack onItemRightClick(ItemStack item, World world, EntityPlayer player){
        player.openGui(RGBBlock.instance, 0, world, (int) Math.floor(player.posX), (int) Math.floor(player.posY), (int) Math.floor(player.posZ));
    	return item;
    }

    @Override
    public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float offsetX, float offsetY, float offsetZ) {
        int blockId = world.getBlockId(x, y, z);

        if (blockId > 0 && Block.blocksList[blockId] != null) {
            Block block = Block.blocksList[blockId];
            int blockMetadata = world.getBlockMetadata(x, y, z);
            
            if (blockId != RGBBlock.RGBBlockID && blockId != Block.glass.blockID) {
                if (!isSlab(blockId) && (block.renderAsNormalBlock() == false || block.hasTileEntity(0))) {
                    player.openGui(RGBBlock.instance, 0, world, x, y, z);
                    return false;
                }
            }
            NBTTagCompound nbt = stack.getTagCompound();
            if (nbt != null) {
                if (nbt.getInteger("Mode") == 0 && !player.isSneaking()) {
                    if (blockId != RGBBlock.RGBBlockID) {
                    		int cost = 12;
                            int Color = nbt.getInteger("color");
                        	if(!world.isRemote){
                            	if(!applyCost(cost, player))
                            		return true;
                        		world.setBlock(x, y, z, RGBBlock.RGBBlockID, 0, 0);
                        		TileEntityRGB blockTile = RGBBlock.BlockRGB.getTileAt(world, x, y, z, false);
                        		for (int allSides = 0; allSides <= 5; allSides++) {
                        			blockTile.setSideColor(allSides, Color);
                        		}
                           	 	blockTile.blockId = blockId;
                           	 	blockTile.blockMetadata = blockMetadata;
                           	 	world.markBlockForUpdate(x, y, z);
                        	}
                    } else if (blockId == RGBBlock.RGBBlockID) {
                    	int cost = 12;
                        TileEntityRGB blockTile = RGBBlock.BlockRGB.getTileAt(world, x, y, z, false);
                        if (blockTile != null) {
                        	if(!world.isRemote){
                            int Color = nbt.getInteger("color");
                            for(int i = 0; i <= 5; i++){
                            	if(blockTile.getSideColor(i) == Color)
                            		cost--;
                            }
                        	if(!applyCost(cost, player))
                        		return true;
                            for (int allSides = 0; allSides <= 5; allSides++) {
                                blockTile.setSideColor(allSides, Color);
                            }
                           	world.markBlockForUpdate(x, y, z);
                        	}
                        }
                    }
                } else if (nbt.getInteger("Mode") == 1 && !player.isSneaking() && !isPlacedItem(blockId)) {
                    if (blockId == RGBBlock.RGBBlockID) {
                        TileEntityRGB blockTile = RGBBlock.BlockRGB.getTileAt(world, x, y, z, false);
                        if (blockTile != null) {
                            int Color = nbt.getInteger("color");
                            if(Color != blockTile.getSideColor(side)){
                            	if(!applyCost(2, player))
                            		return true;
                            }
                            if (!world.isRemote){
                            	blockTile.setSideColor(side, Color);
                            	world.markBlockForUpdate(x, y, z);
                            } else {
                            	world.markBlockForRenderUpdate(x, y, z);
                            }
                        }
                    	
                        return true;
                    } else {
                    	if (!world.isRemote){
                        int Color = nbt.getInteger("color");
                        world.setBlock(x, y, z, RGBBlock.RGBBlockID, 0, 0);
                        TileEntityRGB blockTile = RGBBlock.BlockRGB.getTileAt(world, x, y, z, false);
                         if(!applyCost(2, player))
                            return true;
                        blockTile.setSideColor(side, Color);
                        blockTile.blockId = blockId;
                        blockTile.blockMetadata = blockMetadata;
                      	world.markBlockForUpdate(x, y, z);
                    	}
                        return true;
                    }
                } else if (player.isSneaking()) {
                    if (blockId == 35) {
                        int metadata = world.getBlockMetadata(x, y, z);
                        if (metadata == 0) {
                            if (!world.isRemote)
                                ((EntityPlayerMP) player).setCurrentItemOrArmor(0, new ItemStack(RGBBlock.BlockInjector));
                        }
                        if (metadata * 3 + 2 < woolCode.length) {
                            ItemStack item = player.getCurrentEquippedItem();
                            int red = woolCode[metadata * 3];
                            int green = woolCode[metadata * 3 + 1];
                            int blue = woolCode[metadata * 3 + 2];

                            String hex = getTwoCharHex(red) + getTwoCharHex(green) + getTwoCharHex(blue);
                            int color = Integer.parseInt(hex, 16);

                            nbt.setInteger("color", color);

                            if (!world.isRemote) {
                                ((EntityPlayerMP) player).setCurrentItemOrArmor(0, item);
                            }
                        }
                        return true;
                    } else if (blockId == RGBBlock.RGBBlockID) {
                        TileEntityRGB blockTile = RGBBlock.BlockRGB.getTileAt(world, x, y, z, false);
                        if (blockTile != null) {
                            ItemStack item = player.getCurrentEquippedItem();
                            int Color = blockTile.getSideColor(side);
                            if (Color >= 0) {
                                nbt.setInteger("color", Color);
                                if (!world.isRemote) {
                                    ((EntityPlayerMP) player).setCurrentItemOrArmor(0, item);
                                }
                            }
                            return true;
                        }
                    } else if((blockId == Block.grass.blockID && side == 1) || (blockId == Block.leaves.blockID) && RGBBlock.selectFoliage) {          
                        int multi = block.colorMultiplier(world, x, y, z);
                        
                        nbt.setInteger("color", multi);
                        return true;
                    } else {
                        player.openGui(RGBBlock.instance, 0, world, x, y, z);
                        return false;
                    }
                }
                return true; // return false == send packet
            }
        }
        return true; // == dont send packet
    }

    private boolean applyCost(int i, EntityPlayer player) {
    	if((!RGBBlock.isSurvival || player.capabilities.isCreativeMode) && !player.worldObj.isRemote)
    		return true;
    	
    	if(!player.worldObj.isRemote){
    		ItemStack item = player.getCurrentEquippedItem();
    		int itemDamage = item.getItemDamage();
    		if(itemDamage <= this.getMaxDamage() - i){
    			item.setItemDamage(itemDamage + i);
				player.setCurrentItemOrArmor(0, item);
				return true;
			}
			player.sendChatToPlayer("Not Enough Dye!");
			return false;
		}
		return false;
	}

	private boolean isPlacedItem(int blockId) {
        if (blockId == 6 || blockId == 32 || blockId == 37 || blockId == 38 || blockId == 39 || blockId == 40)
            return true;
        return false;
    }

    private boolean isSlab(int blockId) {
        if (Block.blocksList[blockId] instanceof BlockHalfSlab) {
        	return true; 
        }
        return false;
    }
}
