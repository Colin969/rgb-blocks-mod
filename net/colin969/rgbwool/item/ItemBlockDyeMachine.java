package net.colin969.rgbwool.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.lang.LocalizationHelper;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemBlockWithMetadata;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.Icon;

public class ItemBlockDyeMachine extends ItemBlock{

	public ItemBlockDyeMachine(int par1) {
		super(par1);
        this.setMaxDamage(0);
        this.setHasSubtypes(true);
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public Icon getIconFromDamage(int par1)
	{
	        return RGBBlock.DyeMachine.getIcon(0, par1);
	}
	
	@Override
    public int getSpriteNumber()
    {
        return 0;
    }
	
	@Override
	public int getMetadata(int par1)
	{
	         return par1;
	}
	
	@Override
    public String getUnlocalizedName(ItemStack par1ItemStack)
    {
		if(par1ItemStack.getItemDamage() == 8)
			return "block.dye_factory";
		else
			return "block.dye_machine";
    	
    }
	
	@Override
	public String getLocalizedName(ItemStack item){
		return LocalizationHelper.getLocalizedString(this.getUnlocalizedName(item));
	}
	
	@Override
	public String getItemDisplayName(ItemStack item){
		return getLocalizedName(item);
	}

}
