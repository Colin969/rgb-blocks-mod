package net.colin969.rgbwool.item;

import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.entity.TileEntityRGB;
import net.colin969.rgbwool.lang.LocalizationHelper;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemBlockCleanser extends Item{

	public ItemBlockCleanser(int par1) {
		super(par1);
		this.setNoRepair();
	}
	
	@Override
	public void registerIcons(IconRegister ir){
		itemIcon = ir.registerIcon("RGBBlocks:BlockCleanser");
	}
	
    @Override
    public String getItemDisplayName(ItemStack par1ItemStack) {
        return LocalizationHelper.getLocalizedString("item.block_cleanser");
    }
	
	@Override
    public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float offsetX, float offsetY, float offsetZ) {
		int blockId = world.getBlockId(x, y, z);
		
		if(blockId == RGBBlock.RGBBlockID && !player.isSneaking()){
			TileEntityRGB te = (TileEntityRGB) world.getBlockTileEntity(x, y, z);
			world.removeBlockTileEntity(x, y, z);
			world.setBlock(x, y, z, te.blockId, te.blockMetadata, 0);
            if (!world.isRemote)
                world.markBlockForUpdate(x, y, z);
            else
                world.markBlockForRenderUpdate(x, y, z);
			return true;
		} else if (blockId == RGBBlock.RGBBlockID && player.isSneaking()){
			TileEntityRGB te = (TileEntityRGB) world.getBlockTileEntity(x, y, z);
			te.setSideColor(side, -1);
            if (!world.isRemote)
                world.markBlockForUpdate(x, y, z);
            else
                world.markBlockForRenderUpdate(x, y, z);
			return true;
		}
		return false;
	}

}
