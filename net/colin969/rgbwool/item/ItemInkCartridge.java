package net.colin969.rgbwool.item;

import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.lang.LocalizationHelper;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemInkCartridge extends Item{

	public ItemInkCartridge(int par1) {
		super(par1);
	}
	
	@Override
	public void registerIcons(IconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon(RGBBlock.MODID+":Cartridge");
	}
	
	@Override
	public String getItemDisplayName(ItemStack par1ItemStack){
		return LocalizationHelper.getLocalizedString("item.ink_cartridge");
	}

}
