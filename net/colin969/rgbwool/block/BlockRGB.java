package net.colin969.rgbwool.block;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.entity.TileEntityRGB;
import net.colin969.rgbwool.proxy.ClientProxy;
import net.colin969.rgbwool.proxy.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.BlockHalfSlab;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.common.IPlantable;

public class BlockRGB extends BlockContainer {
	
    public BlockRGB(int id, Material material) {
        super(id, material);

    }

    @Override
    public void registerIcons(IconRegister par1IconRegister) {
    	this.blockIcon = par1IconRegister.registerIcon("RGBBlocks:ParticleImage");
    }
    
    public TileEntityRGB getTileAt(IBlockAccess world, int x, int y, int z) {
        return getTileAt(world, x, y, z, true);
    }
    
    public TileEntityRGB getTileAt(IBlockAccess world, int x, int y, int z, boolean validate) {
        if (world != null) {
            TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
            if (tileEntity instanceof TileEntityRGB) {
                TileEntityRGB rgb = (TileEntityRGB) tileEntity;
                if (!validate || rgb.isBlockValid()) {
                    return rgb;
                }
            }
        }
        return null;
    }
    
    public Block getModelBlock(IBlockAccess world, int x, int y, int z) {
        Block b = null;
        if (world != null) {
            TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
            if (tileEntity instanceof TileEntityRGB) {
                TileEntityRGB rgb = (TileEntityRGB) tileEntity;
                if (rgb.isBlockValid()) {
                    b = blocksList[rgb.blockId];
                }
            }
        }
        return b == null || b == this ? Block.stone : b;
    }

    @Override
    public Icon getBlockTexture(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5) {
        TileEntityRGB rgb = getTileAt(par1IBlockAccess, par2, par3, par4);
        if (rgb != null) {
            return blocksList[rgb.blockId].getBlockTexture(par1IBlockAccess, par2, par3, par4, par5); 
        }
        return null;
    }
    

    public void setBlockBoundsBasedOnState(IBlockAccess par1IBlockAccess, int par2, int par3, int par4) {
        TileEntityRGB TileEnt = (TileEntityRGB) par1IBlockAccess.getBlockTileEntity(par2, par3, par4);
        if (isSlab(TileEnt.blockId)) {
        		boolean flag = (TileEnt.blockMetadata & 8) != 0;
            	if (flag) {
            		this.setBlockBounds(0.0F, 0.5F, 0.0F, 1.0F, 1.0F, 1.0F);
            	} else {
                	this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.5F, 1.0F);
            	}

        } else if (isPlacedItem(TileEnt.blockId)) {
            float f = 0.2F;
            this.setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f * 2.0F, 0.5F + f);
        } else {
            this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
        }
    }

	private boolean isPlacedItem(int blockId) {
        if (blockId == 6 || blockId == 32 || blockId == 37 || blockId == 38 || blockId == 39 || blockId == 40)
            return true;
        return false;
    }

    @Override
    public void addCollisionBoxesToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, List par6List, Entity par7Entity) {

        this.setBlockBoundsBasedOnState(par1World, par2, par3, par4);
        super.addCollisionBoxesToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
    }

    public boolean isSlab(int blockId) {
        if (Block.blocksList[blockId] instanceof BlockHalfSlab){
        	if(!Block.blocksList[blockId].renderAsNormalBlock())
        		return true;
        	else return false;
        }
        return false;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    @Override
    public boolean canPlaceTorchOnTop(World world, int x, int y, int z) {
        return getModelBlock(world, x, y, z).canPlaceBlockAt(world, x, y, z);
    }

    @Override
    public boolean isBlockSolidOnSide(World world, int x, int y, int z, ForgeDirection side) {
        return getModelBlock(world, x, y, z).isBlockSolidOnSide(world, x, y, z, side);
    }

    @Override
    public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLiving par5EntityLiving, ItemStack par6ItemStack){
    	NBTTagCompound NBTcompound = par6ItemStack.getTagCompound();
    	TileEntityRGB tileEnt = this.getTileAt(par1World, par2, par3, par4);
    	if(tileEnt != null){
    		tileEnt.Top = NBTcompound.getInteger("Color");
    		tileEnt.Bottom = NBTcompound.getInteger("Color");
    		tileEnt.North = NBTcompound.getInteger("Color");
    		tileEnt.South = NBTcompound.getInteger("Color");
    		tileEnt.East = NBTcompound.getInteger("Color");
        	tileEnt.West = NBTcompound.getInteger("Color");
        	tileEnt.blockId = NBTcompound.getInteger("blockId");
        	tileEnt.blockMetadata = NBTcompound.getInteger("blockMetadata");
    	}
    }
    
    @Override
    public void dropBlockAsItemWithChance(World par1World, int par2, int par3, int par4, int par5, float par6, int par7) {
        if (!par1World.isRemote) {
            ArrayList<ItemStack> items = getBlockDropped(par1World, par2, par3, par4, par5, par7);

            for (ItemStack item : items) {
                if (par1World.rand.nextFloat() <= par6) {
                    this.dropBlockAsItem_do(par1World, par2, par3, par4, item);
                }
            }
        }
    }

    @Override
    public ArrayList<ItemStack> getBlockDropped(World world, int x, int y, int z, int metadata, int fortune) {
        ArrayList<ItemStack> ret = new ArrayList<ItemStack>();

        TileEntityRGB te = (TileEntityRGB) world.getBlockTileEntity(x, y, z);
        ItemStack is;
        if (te == null) {
            return ret;
        } else {
            ArrayList<ItemStack> stacks = Block.blocksList[te.blockId].getBlockDropped(world, x, y, z, te.blockMetadata, 0);
            for (int index = 0; index < stacks.size(); index++)
                ret.add(stacks.get(index));
            world.removeBlockTileEntity(x, y, z);
        }
        
        return ret;
    }

    @Override
    public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6) {
    	this.dropBlockAsItem(par1World, par2, par3, par4, par5, par6);
    }
    
    @Override
    public void onBlockDestroyedByPlayer(World world, int x, int y, int z, int meta){
    	
    }

    @Override
    public int getRenderType() {
        return CommonProxy.woolRenderType;
    }

    @Override
    public boolean canRenderInPass(int pass) {
        return pass == 0;
    }

    @Override
    public boolean renderAsNormalBlock() {
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World var1) {
        return new TileEntityRGB();
    }

    @Override
    public int getRenderColor(int par1) {
        return 0xFFFFFF;
    }
    
    @Override
    public int colorMultiplier(IBlockAccess par1iBlockAccess, int par2, int par3, int par4) {
        return getModelBlock(par1iBlockAccess, par2, par3, par4).colorMultiplier(par1iBlockAccess, par2, par3, par4);
    }
    
    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z){
    	try{
    		TileEntityRGB te = (TileEntityRGB) world.getBlockTileEntity(x, y, z);
    		Integer r = lightValue[te.blockId];
    		return r;
    	} catch (Exception e){
    		return 0;
    	}
    }
    
    @Override
    public float getBlockHardness(World world, int x, int y, int z){
    	TileEntityRGB te = (TileEntityRGB) world.getBlockTileEntity(x, y, z);
    	if(te == null)
    		return 0.8F;
    	return blocksList[te.blockId].blockHardness;
    }
    
    @Override
    public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z){
    	TileEntityRGB te = (TileEntityRGB) world.getBlockTileEntity(x, y, z);
    	return new ItemStack(te.blockId, 1, te.blockMetadata);
    }
    
    @Override
    public boolean canSustainLeaves(World world, int x, int y, int z){
    	return isWood(world, x, y, z);
    }
    
    @Override
    public boolean isWood(World world, int x, int y, int z){
    	TileEntityRGB te = (TileEntityRGB) world.getBlockTileEntity(x, y, z);
    	if(te.blockId == Block.wood.blockID)
    		return true;
    	return false;
    }
    
    @Override
    public int getLightOpacity(World world, int x, int y, int z){
    	TileEntityRGB te = (TileEntityRGB) world.getBlockTileEntity(x, y, z);
    	return lightOpacity[te.blockId];
    }
    
    @Override
    public boolean canSustainPlant(World world, int x, int y, int z, ForgeDirection direction, IPlantable plant)
    {
    	TileEntityRGB te = (TileEntityRGB) world.getBlockTileEntity(x, y, z);
        int plantID = plant.getPlantID(world, x, y + 1, z);
        EnumPlantType plantType = plant.getPlantType(world, x, y + 1, z);

        if (plantID == cactus.blockID && te.blockId == cactus.blockID)
        {
            return true;
        }

        if (plantID == reed.blockID && te.blockId == reed.blockID)
        {
            return true;
        }

        if (plant instanceof BlockFlower && te.blockId == Block.grass.blockID || te.blockId == Block.dirt.blockID)
        {
            return true;
        }

        switch (plantType)
        {
            case Desert: return te.blockId == sand.blockID;
            case Nether: return te.blockId == slowSand.blockID;
            case Crop:   return te.blockId == tilledField.blockID;
            case Cave:   return isBlockSolidOnSide(world, x, y, z, ForgeDirection.UP);
            case Plains: return te.blockId == grass.blockID || blockID == dirt.blockID;
            case Water:  return world.getBlockMaterial(x, y, z) == Material.water && world.getBlockMetadata(x, y, z) == 0;
            case Beach:
                boolean isBeach = (te.blockId == Block.grass.blockID || te.blockId == Block.dirt.blockID || te.blockId == Block.sand.blockID);
                boolean hasWater = (world.getBlockMaterial(x - 1, y, z    ) == Material.water ||
                                    world.getBlockMaterial(x + 1, y, z    ) == Material.water ||
                                    world.getBlockMaterial(x,     y, z - 1) == Material.water ||
                                    world.getBlockMaterial(x,     y, z + 1) == Material.water);
                return isBeach && hasWater;
        }

        return false;
    }
    
    @Override
    public int getDamageValue(World world, int x, int y, int z){
    	TileEntityRGB te = (TileEntityRGB) world.getBlockTileEntity(x, y, z);
    	return te.blockMetadata;
    }
    @Override
    public float getExplosionResistance(Entity par1Entity, World world, int x, int y, int z, double explosionX, double explosionY, double explosionZ){
    	TileEntityRGB te = (TileEntityRGB) world.getBlockTileEntity(x, y, z);
    	return blocksList[te.blockId].blockResistance / 5.0F;
    }
    
}
