package net.colin969.rgbwool.block;

import java.util.List;
import java.util.Random;

import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.entity.TileEntityDyeMachine;
import net.colin969.rgbwool.lang.LocalizationHelper;
import net.colin969.rgbwool.proxy.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockDyeMachine extends BlockContainer {

		private Icon dyeSide;
		private Icon dyeTop;
		private Icon dyeFront;
		private Icon dyeBottom;
		private Icon dyeTopOverlay;
	
        public BlockDyeMachine (int id, Material material) {
                super(id, material);
        }
        
        @Override
        public void registerIcons(IconRegister par1IconRegister)
        {
            this.dyeSide = par1IconRegister.registerIcon(RGBBlock.MODID+":DyeMachineSide");
            this.dyeTop = par1IconRegister.registerIcon(RGBBlock.MODID+":DyeMachineTop");
            this.dyeBottom = par1IconRegister.registerIcon(RGBBlock.MODID+":DyeMachineBottom");
            this.dyeFront = par1IconRegister.registerIcon(RGBBlock.MODID+":DyeMachineFront");
            this.dyeTopOverlay = par1IconRegister.registerIcon(RGBBlock.MODID + ":DyeMachineTopOverlay");
        }
        
        @Override
        public int getRenderType(){
        	return CommonProxy.dyeRenderType;
        }
        
        @Override
        public String getLocalizedName(){
        	return LocalizationHelper.getLocalizedString(this.getUnlocalizedName());
        }
        
        @Override
        public int damageDropped(int i){
        	return i;
        }     
        
        @Override
        public Icon getIcon(int par1, int par2)
        {
        	if(par2 == 8)
        		return dyeTop;
        	else
        		return par1 == 1 ? this.dyeTop : (par1 == 0 ? this.dyeBottom : (par1 == 3 ? this.dyeFront : this.dyeSide));
        	
        }
        
        public Icon getBlockTexture(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5)
        {
        	int meta = par1IBlockAccess.getBlockMetadata(par2, par3, par4);
        	if(meta == 8){
        		return dyeTop;
            } else {
	            if (par5 == 1)
	            {
	                return this.dyeTop;
	            }
	            else if (par5 == 0)
	            {
	                return this.dyeBottom;
	            }
	            else
	            {
	                int var6 = par1IBlockAccess.getBlockMetadata(par2, par3, par4);
	                return par5 != var6 ? this.dyeSide : (this.dyeFront);
	            }
            }
        }
        
        @Override
        public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLiving par5EntityLiving,  ItemStack par6ItemStack)
        {
            if(par6ItemStack.getItemDamage() == 8){
            	par1World.setBlockMetadataWithNotify(par2, par3, par4, 8, 0);
            } else {
                int var6 = MathHelper.floor_double((double)(par5EntityLiving.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
                
	            if (var6 == 0)
	            {
	                par1World.setBlockMetadataWithNotify(par2, par3, par4, 2, 0);
	            }
	
	            if (var6 == 1)
	            {
	                par1World.setBlockMetadataWithNotify(par2, par3, par4, 5, 0);
	            }
	
	            if (var6 == 2)
	            {
	                par1World.setBlockMetadataWithNotify(par2, par3, par4, 3, 0);
	            }
	
	            if (var6 == 3)
	            {
	                par1World.setBlockMetadataWithNotify(par2, par3, par4, 4, 0);
	            }
            }
        }
        
        @Override
        public void onNeighborBlockChange(World par1World, int par2, int par3, int par4, int par5) {
        	TileEntity te = par1World.getBlockTileEntity(par2, par3, par4);
        	if(te != null){
        		((TileEntityDyeMachine) te).updateConnectedSides();
        	}
        	if(par1World.isRemote){
        		par1World.markBlockForRenderUpdate(par2, par3, par4);
        	} else {
        		par1World.markBlockForUpdate(par2, par3, par4);
        	}
        	
        	super.onNeighborBlockChange(par1World, par2, par3, par4, par5);
        	
        }

    	
    	@Override
        public void onBlockAdded(World par1World, int par2, int par3, int par4)
        {
            super.onBlockAdded(par1World, par2, par3, par4);
            this.setDefaultDirection(par1World, par2, par3, par4);
            TileEntity te = par1World.getBlockTileEntity(par2, par3, par4);
            if(te != null)
            	((TileEntityDyeMachine) te).updateConnectedSides();
        }
    	
        private void setDefaultDirection(World par1World, int par2, int par3, int par4)
        {
            if (!par1World.isRemote)
            {
                int var5 = par1World.getBlockId(par2, par3, par4 - 1);
                int var6 = par1World.getBlockId(par2, par3, par4 + 1);
                int var7 = par1World.getBlockId(par2 - 1, par3, par4);
                int var8 = par1World.getBlockId(par2 + 1, par3, par4);
                byte var9 = 3;

                if (Block.opaqueCubeLookup[var5] && !Block.opaqueCubeLookup[var6])
                {
                    var9 = 3;
                }

                if (Block.opaqueCubeLookup[var6] && !Block.opaqueCubeLookup[var5])
                {
                    var9 = 2;
                }

                if (Block.opaqueCubeLookup[var7] && !Block.opaqueCubeLookup[var8])
                {
                    var9 = 5;
                }

                if (Block.opaqueCubeLookup[var8] && !Block.opaqueCubeLookup[var7])
                {
                    var9 = 4;
                }

                par1World.setBlockMetadataWithNotify(par2, par3, par4, var9, 0);
            }
        }

    	
		@Override
        public boolean onBlockActivated(World world, int x, int y, int z,
                        EntityPlayer player, int idk, float what, float these, float are) {
                TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
                if (tileEntity == null || player.isSneaking()) {
                        return false;
                }
                
                if(world.getBlockMetadata(x, y, z) == 8){
                	if(!world.isRemote){
                		player.sendChatToPlayer("Contains " + String.valueOf(((TileEntityDyeMachine) world.getBlockTileEntity(x, y, z)).getDye()) + " Liquid Dye");
                	}
                	return true;
                }
                
                player.openGui(RGBBlock.instance, 0, world, x, y, z);
                return true;
        }

        @Override
        public void breakBlock(World world, int x, int y, int z, int par5, int par6) {
                dropItems(world, x, y, z);
                super.breakBlock(world, x, y, z, par5, par6);
        }

        public void dropItems(World world, int x, int y, int z){
                Random rand = new Random();

                TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
                if (!(tileEntity instanceof IInventory)) {
                        return;
                }
                IInventory inventory = (IInventory) tileEntity;

                for (int i = 0; i < inventory.getSizeInventory(); i++) {
                        ItemStack item = inventory.getStackInSlot(i);

                        if (item != null && item.stackSize > 0) {
                                float rx = rand.nextFloat() * 0.8F + 0.1F;
                                float ry = rand.nextFloat() * 0.8F + 0.1F;
                                float rz = rand.nextFloat() * 0.8F + 0.1F;

                                EntityItem entityItem = new EntityItem(world,
                                                x + rx, y + ry, z + rz,
                                                new ItemStack(item.itemID, item.stackSize, item.getItemDamage()));

                                if (item.hasTagCompound()) {
                                        entityItem.readFromNBT((NBTTagCompound) item.getTagCompound().copy());
                                }

                                float factor = 0.05F;
                                entityItem.motionX = rand.nextGaussian() * factor;
                                entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
                                entityItem.motionZ = rand.nextGaussian() * factor;
                                world.spawnEntityInWorld(entityItem);
                                item.stackSize = 0;
                        }
                }
        }

        @Override
        public TileEntity createNewTileEntity(World world) {
                return new TileEntityDyeMachine();
        }
        
        @Override
        public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs, List par3List){
        	par3List.add(new ItemStack(par1, 1, 0));
        	par3List.add(new ItemStack(par1, 1, 8));
        }

		public Icon getBlockOverlay() {
			return dyeTopOverlay;
		}

		public Icon getTankIcon() {
			return RGBBlock.LiquidDyeStill.getIcon(0, 0);
		}
		
		@Override
		public boolean isOpaqueCube(){
			return false;
		}

}