package net.colin969.rgbwool.container;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import cpw.mods.fml.common.network.PacketDispatcher;
import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.entity.TileEntityDyeMachine;
import net.colin969.rgbwool.item.ItemBlockInjector;
import net.colin969.rgbwool.item.ItemRGBBlock;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;

public class ContainerDyeMachine extends Container {

	public TileEntityDyeMachine tileEntity;

	private World worldObj;

	public ContainerDyeMachine(InventoryPlayer inventoryPlayer,
			TileEntityDyeMachine te, World world) {
		this.worldObj = world;
		tileEntity = te;
		// the Slot constructor takes the IInventory and the slot number in that
		// it binds to
		// and the x-y coordinates it resides on-screen
		// this.addSlotToContainer(new Slot(tileEntity, 0, 56, 17));
		this.addSlotToContainer(new Slot(te, 0, 54, 24));
		this.addSlotToContainer(new Slot(te, 1, 116, 34));
		this.addSlotToContainer(new Slot(te, 2, 54, 44));

		// commonly used vanilla code that adds the player's inventory
		bindPlayerInventory(inventoryPlayer);
	}

	@Override
	public boolean canInteractWith(EntityPlayer player) {
		return tileEntity.isUseableByPlayer(player);
	}
	
	@Override
	public void detectAndSendChanges(){
        for (int i = 0; i < this.inventorySlots.size(); ++i)
        {
            ItemStack itemstack = ((Slot)this.inventorySlots.get(i)).getStack();
            ItemStack itemstack1 = (ItemStack)this.inventoryItemStacks.get(i);

            if (!ItemStack.areItemStacksEqual(itemstack1, itemstack))
            {
                itemstack1 = itemstack == null ? null : itemstack.copy();
                this.inventoryItemStacks.set(i, itemstack1);

                for (int j = 0; j < this.crafters.size(); ++j)
                {
                    ((ICrafting)this.crafters.get(j)).sendSlotContents(this, i, itemstack1);
                }
            }
        }
	}

	protected void bindPlayerInventory(InventoryPlayer inventoryPlayer) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9,
						8 + j * 18, 84 + i * 18));
			}
		}

		for (int i = 0; i < 9; i++) {
			addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18, 142));
		}
	}

	   @Override
	   public ItemStack transferStackInSlot(EntityPlayer entityPlayer, int slot) {
	      Slot slotObject = (Slot) inventorySlots.get(slot);
	      if(slotObject != null && slotObject.getHasStack()) {
	         ItemStack stackInSlot = slotObject.getStack();
	         ItemStack stack = stackInSlot.copy();
	         if(slot == 0 || slot == 1 || slot == 2){
		           if(!mergeItemStack(stackInSlot, 2, inventorySlots.size(), true)){
		        	   return null;
		           } else {
		        	   mergeItemStack(stackInSlot, 2, inventorySlots.size(), true);
		        	   slotObject.decrStackSize(slotObject.getStack().stackSize);
		        	   return null;
		           }
	         } else {
	        	 int slotno = 0;
	        	 if(!mergeItemStack(stackInSlot, slotno, 1, true)){
	        		 return null;
	        	 } else {
	        		 mergeItemStack(stackInSlot, slotno, inventorySlots.size(), true);
	        		 slotObject.decrStackSize(slotObject.getStack().stackSize);
	        		 return null;
	        	 }
	         }
	      }
	      return null;
	}

	public void setCurrentColor(int color) {
		this.tileEntity.color = color;
	}

	private int getColor() {
		return this.tileEntity.color;
	}

	public void sendColorToServer() {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(8);
		DataOutputStream os = new DataOutputStream(bos);
		try {
			os.writeInt(this.getColor());
			os.writeInt(this.tileEntity.xCoord);
			os.writeInt(this.tileEntity.yCoord);
			os.writeInt(this.tileEntity.zCoord);
			os.writeBoolean(false);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Packet250CustomPayload packet = new Packet250CustomPayload();
		packet.channel = "DyeMachineXYZ";
		packet.data = bos.toByteArray();
		packet.length = bos.size();

		PacketDispatcher.sendPacketToServer(packet);
	}
}