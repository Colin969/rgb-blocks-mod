package net.colin969.rgbwool;

import java.util.ArrayList;
import java.util.List;

import net.colin969.rgbwool.item.ItemBlockInjector;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ModRecipes implements IRecipe{

	@Override
	public boolean matches(InventoryCrafting inventorycrafting, World world) {
		List<Item> stackList = new ArrayList<Item>();
		for(int i = 0; i < inventorycrafting.getSizeInventory();i++){
			ItemStack item = inventorycrafting.getStackInSlot(i);
			if(item != null)
				stackList.add(inventorycrafting.getStackInSlot(i).getItem());
		}
		if(stackList.size() == 2){
			if(stackList.contains(Item.diamond)){
				for(Item i : stackList)
					if(i instanceof ItemBlockInjector) return true;
			}
		}
		return false;
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting inventorycrafting) {
		ItemStack outputStack = null;

		for(int i = 0; i < inventorycrafting.getSizeInventory();i++){
			ItemStack item = inventorycrafting.getStackInSlot(i);
			if(item != null)
				if(item.getItem() instanceof ItemBlockInjector)
					outputStack = item.copy();
		}
		
		if(outputStack != null){
			int version = outputStack.getItem().getMaxDamage() / 2000;
			int leftUses = outputStack.getItem().getMaxDamage() - outputStack.getItemDamage();
			NBTTagCompound nbt = outputStack.getTagCompound();
			switch(version){
			case 1:
				outputStack = new ItemStack(RGBBlock.BlockInjectorExtra2);
				outputStack.setItemDamage(outputStack.getItem().getMaxDamage() - leftUses);
				outputStack.setTagCompound(nbt);
				break;
			case 2:
				outputStack = new ItemStack(RGBBlock.BlockInjectorExtra3);
				outputStack.setItemDamage(outputStack.getItem().getMaxDamage() - leftUses);
				outputStack.setTagCompound(nbt);
				break;
			case 3:
				outputStack = new ItemStack(RGBBlock.BlockInjectorExtra4);
				outputStack.setItemDamage(outputStack.getItem().getMaxDamage() - leftUses);
				outputStack.setTagCompound(nbt);
				break;
			default:
				outputStack = null;
				break;
			}
		}
		return outputStack;
	}

	@Override
	public int getRecipeSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ItemStack getRecipeOutput() {
		return null;
	}

}
