package net.colin969.rgbwool.proxy;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.MinecraftForgeClient;
import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.entity.EntityPaintBomb;
import net.colin969.rgbwool.entity.TileEntityDyeMachine;
import net.colin969.rgbwool.entity.TileEntitySpecialFactory;
import net.colin969.rgbwool.renderer.BlockDyeMachineRenderer;
import net.colin969.rgbwool.renderer.BlockRGBRenderer;
import net.colin969.rgbwool.renderer.ItemBlockInjectorRenderer;
import net.colin969.rgbwool.renderer.ItemPaintBombRenderer;
import net.colin969.rgbwool.renderer.ItemRGBBlockRenderer;
import net.colin969.rgbwool.renderer.RenderEntityPaintBomb;

public class ClientProxy extends CommonProxy {
        
        public static int DyeBlockRenderStage;

		public static void setCustom()
        {
                RenderingRegistry.registerBlockHandler(new BlockRGBRenderer());
                RenderingRegistry.registerBlockHandler(new BlockDyeMachineRenderer());
                ClientRegistry.bindTileEntitySpecialRenderer(TileEntityDyeMachine.class, new TileEntitySpecialFactory());
    	        RenderingRegistry.registerEntityRenderingHandler(EntityPaintBomb.class, new RenderEntityPaintBomb(RGBBlock.PaintBomb));
    	        MinecraftForgeClient.registerItemRenderer(RGBBlock.BlockInjectorID + 256, new ItemBlockInjectorRenderer());
    	        MinecraftForgeClient.registerItemRenderer(RGBBlock.BlockRGBItemID + 256, new ItemRGBBlockRenderer());
    	        MinecraftForgeClient.registerItemRenderer(RGBBlock.PaintBombID + 256, new ItemPaintBombRenderer());

    	        if(RGBBlock.enableExtra){
    	        	MinecraftForgeClient.registerItemRenderer(RGBBlock.BlockInjectorExtra2ID + 256, new ItemBlockInjectorRenderer());
    	        	MinecraftForgeClient.registerItemRenderer(RGBBlock.BlockInjectorExtra3ID + 256, new ItemBlockInjectorRenderer());
    	        	MinecraftForgeClient.registerItemRenderer(RGBBlock.BlockInjectorExtra4ID + 256, new ItemBlockInjectorRenderer());
    	        }
        }

}