package net.colin969.rgbwool.proxy;

import cpw.mods.fml.client.registry.RenderingRegistry;

public class CommonProxy {
        
		public static int woolRenderType;
		public static int dyeRenderType;
	
        public static void registerRenderers() {
        	woolRenderType = RenderingRegistry.getNextAvailableRenderId();
        	dyeRenderType = RenderingRegistry.getNextAvailableRenderId();
        }
}