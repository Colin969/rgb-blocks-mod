package net.colin969.rgbwool.proxy;

import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.container.ContainerDyeMachine;
import net.colin969.rgbwool.entity.TileEntityDyeMachine;
import net.colin969.rgbwool.gui.GuiDyeMachine;
import net.colin969.rgbwool.gui.GuiScreenBlockInjector;
import net.colin969.rgbwool.gui.GuiScreenLibrary;
import net.colin969.rgbwool.item.ItemBlockInjector;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {
        //returns an instance of the Container you made earlier
        @Override
        public Object getServerGuiElement(int id, EntityPlayer player, World world,
                        int x, int y, int z) {
                TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
                if (tileEntity instanceof TileEntityDyeMachine)
                        return new ContainerDyeMachine(player.inventory, (TileEntityDyeMachine) tileEntity, world);
                return null;
        }

        //returns an instance of the Gui you made earlier
        @Override
        public Object getClientGuiElement(int id, EntityPlayer player, World world,
                        int x, int y, int z) {
                TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
                if (tileEntity instanceof TileEntityDyeMachine){
                    return new GuiDyeMachine(player, player.inventory, (TileEntityDyeMachine) tileEntity, world, x, y, z);
                } else if (id == 0 && player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().getItem() instanceof ItemBlockInjector){
                	return new GuiScreenBlockInjector(player,x,y,z);
                } else if (id == 1){
                	return new GuiScreenLibrary(player,x,y,z);
                }
                return null;
        }
}