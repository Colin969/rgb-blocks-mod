package net.colin969.rgbwool.proxy;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.colin969.rgbwool.RGBBlock;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.liquids.LiquidDictionary;

public class LiquidHandler {

	@ForgeSubscribe
	@SideOnly(Side.CLIENT)
	public void textureHook(TextureStitchEvent.Post event){
        LiquidDictionary.getCanonicalLiquid("Dye").setRenderingIcon(RGBBlock.LiquidDyeStill.getBlockTextureFromSide(0)).setTextureSheet("/terrain.png");
	}
}
