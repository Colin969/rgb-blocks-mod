package net.colin969.rgbwool;

import java.awt.image.RescaleOp;
import java.nio.ByteBuffer;
import java.util.EnumSet;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.Texture;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.event.ForgeSubscribe;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

public class TextureLoader implements ITickHandler {
    public TextureLoader() {
    }

    final static RescaleOp op                      = new RescaleOp(1.8F, 0, null);

    static boolean         madeTexture             = false;
    static int             textureId               = -1;
    static int             defaultTerrainTextureID = -1;

    @Override
    public void tickStart(EnumSet<TickType> type, Object... tickData) {
        if (!madeTexture) {
            madeTexture = true;
            try {
                int textureFormat = GL11.GL_RGBA;
                Texture terrainText = Minecraft.getMinecraft().renderEngine.textureMapBlocks.getTexture();
                
                int w = terrainText.getWidth();
                int h = terrainText.getHeight();
                byte[] image = new byte[w * h * 4];
                ByteBuffer terrain = terrainText.getTextureData();
                int posPre = terrain.position();
                terrain.position(0);
                terrain.get(image);
                terrain.position(posPre);
                int[] aint2 = new int[] { 3, 0, 1, 2 };
                int min = 11;
                double scale = (double) (255-min) / 255D;
                for (int y = 0; y < h; ++y) {
                    for (int x = 0; x < w; ++x) {
                        int pos = y * w + x;
                        int idx = pos * 4;
                        double facRed = 0.2126D;
                        double facGreen = 0.7152D;
                        double facBlue = 0.0722D;
                        int red = image[idx + aint2[1]] & 0xFF;
                        int green = image[idx + aint2[2]] & 0xFF;
                        int blue = image[idx + aint2[3]] & 0xFF;
                        	red = (red + 256) / 2;
                        	green = (green + 256) / 2;
                        	blue = (blue + 256) / 2;
                        double dR = facRed * red;
                        double dG = facGreen * green;
                        double dB = facBlue * blue;
                        int val = min+(int) Math.round((dR + dG + dB)*scale);
                        
                        if (val > 255) {
                            System.err.println(">255: "+val);
                            val = 255;
                        }
                        if (val < 0) {
                            System.err.println("<0: "+val);
                            val = 0;
                        }
                        byte result = (byte) val;
                        image[idx + aint2[1]] = result;
                        image[idx + aint2[2]] = result;
                        image[idx + aint2[3]] = result;
                    }
                }
                ByteBuffer textureData = ByteBuffer.allocateDirect(image.length);
                textureData.clear();
                textureData.put(image);
                textureData.limit(image.length);
                textureData.flip();
                if (textureId < 0) {
                    textureId = GL11.glGenTextures();
                }
                GL11.glEnable(GL11.GL_TEXTURE_2D);
                GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);
                GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
                GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
                GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_CLAMP);
                GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_CLAMP);
                GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, textureFormat, w, h, 0, textureFormat, GL11.GL_UNSIGNED_BYTE, textureData);
                System.out.println("Generated GL RGBA texture");
            } catch (Exception e) {
                System.err.println("Failed to grab terrain and convert it to greyscale!");
                e.printStackTrace();
                return;
            }
        }

    }

    @Override
    public void tickEnd(EnumSet<TickType> type, Object... tickData) {
    }

    @Override
    public EnumSet<TickType> ticks() {
        return EnumSet.of(TickType.RENDER);
    }

    @Override
    public String getLabel() {
        return "TextureLoader";
    }

    public static int getGlTextureID() {
        return textureId;
    }

    @ForgeSubscribe
    public void onTextureReload(TextureStitchEvent.Post event) {
        if (event.map == Minecraft.getMinecraft().renderEngine.textureMapBlocks) {
            madeTexture = false;

        }
    }

    public static int getGreyTerrainID() {
        return textureId;
    }

    public static void refresh() {
        madeTexture = false;
    }
}