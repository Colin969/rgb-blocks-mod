package net.colin969.rgbwool;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.Locale.Category;

import org.lwjgl.input.Keyboard;

import net.colin969.rgbwool.block.BlockDyeMachine;
import net.colin969.rgbwool.block.BlockRGB;
import net.colin969.rgbwool.proxy.ClientProxy;
import net.colin969.rgbwool.proxy.GuiHandler;
import net.colin969.rgbwool.proxy.LiquidHandler;
import net.colin969.rgbwool.entity.EntityPaintBomb;
import net.colin969.rgbwool.item.ItemBlockCleanser;
import net.colin969.rgbwool.item.ItemBlockInjector;
import net.colin969.rgbwool.item.ItemInkCartridge;
import net.colin969.rgbwool.item.ItemPaintBomb;
import net.colin969.rgbwool.item.ItemRGBBlock;
import net.colin969.rgbwool.item.ItemBlockDyeMachine;
import net.colin969.rgbwool.lang.LocalizationHandler;
import net.colin969.rgbwool.liquid.LiquidDyeFlowing;
import net.colin969.rgbwool.liquid.LiquidDyeStill;
import net.colin969.rgbwool.proxy.CommonProxy;
import net.colin969.rgbwool.renderer.ItemBlockInjectorRenderer;
import net.colin969.rgbwool.renderer.ItemPaintBombRenderer;
import net.colin969.rgbwool.renderer.ItemRGBBlockRenderer;
import net.colin969.rgbwool.renderer.RenderEntityPaintBomb;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.src.ModLoader;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.Property;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.liquids.LiquidDictionary;
import net.minecraftforge.liquids.LiquidStack;
import net.minecraftforge.oredict.OreDictionary;

import com.google.common.eventbus.EventBus;

import cpw.mods.fml.client.registry.KeyBindingRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.DummyModContainer;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.LoadController;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;

@Mod(modid = "rgbblock", name = "RGB Block")
@NetworkMod(clientSideRequired = true, serverSideRequired = false, channels = { "RenameInjector", "WoolDescription", "WoolInjectorCI", "DyeMachineXYZ" }, packetHandler = net.colin969.rgbwool.network.PacketHandler.class)
public class RGBBlock extends DummyModContainer {
    // Blocks and Some Items
    public static int             RGBBlockID;
    public static int             DyeMachineID;
    public static int             InkCartridgeID;
    public static int             BlockInjectorID;
    public static int			  BlockRGBItemID;
    public static int			  BlockCleanserID;
    public static int			  PaintBombID;
    public static int			  BlockInjectorExtra2ID;
    public static int			  BlockInjectorExtra3ID;
    public static int			  BlockInjectorExtra4ID;
    public static int			  LiquidDyeFlowingID;
    public static int			  LiquidDyeStillID;
    
    // Options
    public static boolean		  isSurvival;
    public static int			  dyeValue;
    public static boolean		  selectFoliage;
    public static boolean		  enableExtra;
    public static boolean		  checkForUpdate;
    public static boolean		  isElectric;
    public static boolean         canBeElectric;

    // Various
    public static BlockRGB        BlockRGB;
    public static LiquidDyeFlowing LiquidDyeFlowing;
    public static LiquidDyeStill  LiquidDyeStill;
    public static BlockDyeMachine DyeMachine;
    public static Item            InkCartridge;
    public static Item            BlockInjector;
    public static Item			  BlockInjectorExtra2;
    public static Item			  BlockInjectorExtra3;
    public static Item			  BlockInjectorExtra4;
    public static Item			  BlockRGBItem;
    public static Item			  BlockCleanser;
    public static Item			  PaintBomb;
    
    public static Configuration			  configUpdateTempStorage;
    
    public final static String MODID = "RGBBlocks";

    public static BufferedImage         missingTextureImage = new BufferedImage(64, 64, 2);
    public final TextureLoader    loader              = new TextureLoader();
    public final LiquidHandler 	  lh				  = new LiquidHandler();
    public ModProperties modProps = new ModProperties();
    
    @Instance("rgbblock")
    public static RGBBlock        instance;

    @SidedProxy(clientSide = "net.colin969.rgbwool.proxy.ClientProxy", serverSide = "net.colin969.rgbwool.proxy.CommonProxy")
    public static CommonProxy     proxy;

    public RGBBlock() { }

    @PreInit
    public void preInit(FMLPreInitializationEvent event) {

        Configuration config = new Configuration(event.getSuggestedConfigurationFile());
        loadConfig(config);
        
        this.configUpdateTempStorage = config;
        
        NetworkRegistry.instance().registerGuiHandler(this, new GuiHandler());

        LocalizationHandler.loadLanguages();
        
        CommonProxy.registerRenderers();
        if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT){
        	try {
            	ModProperties.load();
            	if(checkForUpdate)
            		modProps.checkVersion(event.getModMetadata().version, event.getSourceFile().getAbsolutePath());
                MinecraftForge.EVENT_BUS.register(this.loader);
                TickRegistry.registerTickHandler(this.loader, Side.CLIENT);
        	} catch (Exception e) {
        		System.out.println("Failed to load Mod Properties");
            	e.printStackTrace();
        	}
        }
        
    }

    private void loadConfig(Configuration config) {
        RGBBlockID = config.getBlock("Block RGB Block ID", 701).getInt();
        DyeMachineID = config.getBlock("Block Dye Machine and Factory ID", 702).getInt();
        LiquidDyeFlowingID = config.getBlock("Liquid Dye Flowing ID", 703).getInt();
        LiquidDyeStillID = config.getBlock("Liquid Dye Still ID", 704).getInt();
        InkCartridgeID = config.getItem("Item Ink Cartridge ID", 6501).getInt() - 256;
        BlockInjectorID = config.getItem("Item Block Injector ID", 6502).getInt() - 256;
        BlockRGBItemID = config.getItem("Item RGB Block ID", 6503).getInt() - 256;
        BlockCleanserID = config.getItem("Item Block Cleanser ID", 6504).getInt() - 256;
        PaintBombID = config.get("extra content","Item Paint Bomb ID", 6505).getInt() - 256;
        BlockInjectorExtra2ID = config.get("extra content", "Item Block Injector V2 ID" , 6521).getInt() - 256;
        BlockInjectorExtra3ID = config.get("extra content" , "Item Block Injector V3 ID" , 6522).getInt() - 256;
        BlockInjectorExtra4ID = config.get("extra content" , "Item Block Injector V4 ID" , 6523).getInt() - 256;
        isSurvival = config.get("core", "Is Survival Adapted", true).getBoolean(true);
        dyeValue = config.get("core", "Value per Dye", 20).getInt();
        selectFoliage = config.get("core", "Can Pick from Foliage and Grass", true).getBoolean(true);
        enableExtra = config.get("core", "Enable Extras", true).getBoolean(true);
        checkForUpdate = config.get("core", "Updater Enabled", true).getBoolean(true);
        canBeElectric = config.get("core", "Electrical", true).getBoolean(true);
        config.save();
    }

    @Init
    public void load(FMLInitializationEvent event) throws Exception {
        if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT){
	        proxy.registerRenderers();
	        ClientProxy.setCustom();
        }
    	
        isElectric = false;
        
        if(canBeElectric)
        	if(Loader.isModLoaded("Electric Expansion") || Loader.isModLoaded("Mekanhism"))
        		isElectric = true;
        
    	// Blocks & Items
        BlockRGB = (BlockRGB) (new BlockRGB(RGBBlockID, Material.cloth)).setHardness(0.8F).setStepSound(Block.soundClothFootstep);
        DyeMachine = (BlockDyeMachine) (new BlockDyeMachine(DyeMachineID, Material.rock)).setCreativeTab(CreativeTabs.tabDecorations).setHardness(1.5F).setResistance(10.0F).setStepSound(Block.soundMetalFootstep).setUnlocalizedName("block.dye_machine");
        LiquidDyeFlowing = (LiquidDyeFlowing) (new LiquidDyeFlowing(LiquidDyeFlowingID));
        LiquidDyeStill = (LiquidDyeStill) (new LiquidDyeStill(LiquidDyeStillID));
        InkCartridge = (new ItemInkCartridge(InkCartridgeID)).setCreativeTab(CreativeTabs.tabMisc);
        BlockInjector = (new ItemBlockInjector(BlockInjectorID, 2000)).setCreativeTab(CreativeTabs.tabTools);
        BlockRGBItem = new ItemRGBBlock(BlockRGBItemID);
        BlockCleanser = new ItemBlockCleanser(BlockCleanserID).setCreativeTab(CreativeTabs.tabTools);
        
        LiquidDictionary.getOrCreateLiquid("Dye", new LiquidStack(LiquidDyeStill, 1));
        MinecraftForge.EVENT_BUS.register(this.lh);
        
        // Extras
        if(enableExtra){
        	BlockInjectorExtra2 = new ItemBlockInjector(BlockInjectorExtra2ID, 4000);
        	BlockInjectorExtra3 = new ItemBlockInjector(BlockInjectorExtra3ID, 6000);
        	BlockInjectorExtra4 = new ItemBlockInjector(BlockInjectorExtra4ID, 8000);
        	PaintBomb = new ItemPaintBomb(PaintBombID).setCreativeTab(CreativeTabs.tabBrewing);
        }

        // Registers
        GameRegistry.registerBlock(BlockRGB, "UnobtainableRGB");
        GameRegistry.registerBlock(DyeMachine, ItemBlockDyeMachine.class, "block.dye_machine");
        GameRegistry.registerBlock(LiquidDyeFlowing, "LiquidDyeFlowing");
        GameRegistry.registerBlock(LiquidDyeStill, "LiquidDyeStill");

        // Language
        LanguageRegistry.addName(BlockRGB, "Unobtainable RGB");
        for(int i = 0; i < 7; i++)
        	LanguageRegistry.addName(new ItemStack(DyeMachine, 1, i), "Dye Machine");
        LanguageRegistry.addName(new ItemStack(DyeMachine, 1, 8), "Dye Factory");
        LanguageRegistry.addName(LiquidDyeFlowing, "Liquid Dye");
        LanguageRegistry.addName(LiquidDyeStill, "Liquid Dye Still");
        
        // Tile Entities
        GameRegistry.registerTileEntity(net.colin969.rgbwool.entity.TileEntityRGB.class, "RGBBlock");
        GameRegistry.registerTileEntity(net.colin969.rgbwool.entity.TileEntityDyeMachine.class, "DyeMachine");

        // Recipes
        if(enableExtra){
        	GameRegistry.addRecipe(new ModRecipes());
        }
        GameRegistry.addRecipe(new ItemStack(BlockInjector, 1, 1000 - (3 * dyeValue)), new Object[] { "IC ", " II", "SIR", 'R', Item.redstone, 'I', Item.ingotIron, 'S', Item.stick, 'C', InkCartridge });
        GameRegistry.addRecipe(new ItemStack(InkCartridge), new Object[] { "III", "RGB", "III", 'I', Item.ingotIron, 'R', new ItemStack(Item.dyePowder, 0, 1), 'G', new ItemStack(Item.dyePowder, 0, 2), 'B', new ItemStack(Item.dyePowder, 0, 4) });
        GameRegistry.addRecipe(new ItemStack(DyeMachine, 1, 0), new Object[] { "XDX", "XCX", "XXX", 'X', Item.ingotIron, 'C', Item.diamond, 'D', InkCartridge });
        GameRegistry.addRecipe(new ItemStack(DyeMachine, 1, 8), new Object[] { "XDX", "XCX", "XXX", 'X', Item.ingotIron, 'C', Block.workbench, 'D', InkCartridge });
        GameRegistry.addRecipe(new ItemStack(BlockCleanser), new Object[] { " X ", "XWX", " S ", 'X', Block.cloth, 'W', Item.bucketWater, 'S', Item.stick});

    }

    public boolean registerBus(EventBus bus, LoadController controller) {
        bus.register(this);
        return true;
    }
}
