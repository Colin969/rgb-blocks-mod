package net.colin969.rgbwool;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.google.common.io.Files;

import net.minecraft.client.Minecraft;
import net.minecraft.server.MinecraftServer;

public class ModProperties {

	private static Properties savedProps;

	public static final String SERVER = "colinberry.co.uk";
	public static final String FILENAME = "downloads/various/update.txt";
	public static final String MINECRAFT_VERSION = "1.5.2";
	public static final String PORT = "3306";

	public void checkVersion(String curVer, String path) {
		try {
			String[] info = getVersion(MINECRAFT_VERSION, curVer, path);
			String newVer = info[1];
			String newMcVer = info[0];
			if (Double.valueOf(newVer) > Double.valueOf(curVer)) {
				Logger.log("Opening Updater...");
				Updater frame = new Updater();
				frame.openUpdater(newVer, curVer, newMcVer, path,
						!MINECRAFT_VERSION.equals(newMcVer));
			}
		} catch (Exception e) {
			Logger.log("Failed to fecth Update.txt from SQL Database - Are you offline?");
			e.printStackTrace();
		}
	}

	private String[] getVersion(String mcVer, String curVer, String path)
			throws Exception {

		File json = new File(Minecraft.getMinecraftDir() + "/bin/java-json.jar");
		if (!json.exists()) {
			Logger.log("Downloading JSON Library.");
			URL internal = new URL(
					"http://colinberry.co.uk/downloads/various/java-json.jar");
			json.createNewFile();
			FileOutputStream output = new FileOutputStream(json);
			InputStream input = internal.openStream();
			byte[] buffer = new byte[4096];
			int bytesRead = input.read(buffer);
			while (bytesRead != -1) {
				output.write(buffer, 0, bytesRead);
				bytesRead = input.read(buffer);
			}
			output.close();
			input.close();
		} else {
			Logger.log("JSON Library Found.");
		}
		URL[] classes = { json.toURI().toURL() };

		String baseUrl = "http://colinberry.co.uk/downloads/various/api.php";

		URL url = new URL(baseUrl + "?mcver=" + MINECRAFT_VERSION);
		BufferedReader streamReader = new BufferedReader(new InputStreamReader(
				url.openConnection().getInputStream(), "UTF-8"));
		StringBuilder responseStrBuilder = new StringBuilder();

		String inputStr;

		while ((inputStr = streamReader.readLine()) != null)
			responseStrBuilder.append(inputStr);

		ClassLoader loader = URLClassLoader.newInstance(new URL[] { json
				.toURI().toURL() }, getClass().getClassLoader());
		Class<?> clazz = Class.forName("org.json.JSONObject", true, loader);
		Method method = ((Class<?>) clazz).getDeclaredMethod("get",
				String.class);
		Constructor<?> ctor = clazz.getConstructor(String.class);
		Object jason = ctor.newInstance(responseStrBuilder.toString());

		String[] ToReturn = { "", "" };

		if (method.invoke(jason, "MinecraftVersion").equals(curVer)) {
			ToReturn[0] = MINECRAFT_VERSION;
			ToReturn[1] = (String) method.invoke(ctor, "ModVersion");
		} else {
			URL url2 = new URL(baseUrl);
			BufferedReader streamReader2 = new BufferedReader(
					new InputStreamReader(
							url.openConnection().getInputStream(), "UTF-8"));
			StringBuilder responseStrBuilder2 = new StringBuilder();

			String inputStr2;

			while ((inputStr = streamReader.readLine()) != null)
				responseStrBuilder.append(inputStr);

			ToReturn[0] = (String) method.invoke(jason, "MinecraftVersion");
			ToReturn[1] = (String) method.invoke(jason, "ModVersion");
		}

		Logger.log("Most Relevant Version Found : " + ToReturn[1]
				+ " For Minecraft " + ToReturn[0]);
		return ToReturn;
	}

	public static void load() throws Exception {
		// Check current Config.
		checkFilesExist();
		// Check for old Configs.
		File temp = new File(Minecraft.getMinecraftDir() + "/config/"
				+ RGBBlock.MODID + "/savedProperties.txt");
		if (temp.exists()) {
			savedProps = new Properties();
			FileInputStream in = new FileInputStream(
					Minecraft.getMinecraftDir() + "/config/" + RGBBlock.MODID
							+ "/savedProperties.txt");
			savedProps.load(in);
			in.close();
			saveInfo();
			temp.delete();
		} else {
			savedProps = new Properties();
			FileInputStream in = new FileInputStream(
					Minecraft.getMinecraftDir() + "/config/" + RGBBlock.MODID
							+ "/savedProperties.xml");
			savedProps.loadFromXML(in);
			in.close();
		}
	}

	private static void checkFilesExist() throws Exception {
		File temp = new File(Minecraft.getMinecraftDir() + "/config/"
				+ RGBBlock.MODID + "/savedProperties.xml");

		if (!temp.getParentFile().exists())
			temp.getParentFile().mkdirs();
		if (!temp.exists()) {
			temp.createNewFile();
			BufferedReader in = new BufferedReader(new FileReader(temp));
			String t = in.readLine();
			makeDefaultProperties();
		}
	}

	private static void makeDefaultProperties() throws Exception {
		String white = "16777215";
		savedProps = new Properties();
		for (int i = 1; i <= 10; i++) {
			savedProps.setProperty("recent_" + String.valueOf(i), white);
		}
		saveInfo();
	}

	public static void saveInfo() throws Exception {
		FileOutputStream out = new FileOutputStream(Minecraft.getMinecraftDir()
				+ "/config/" + RGBBlock.MODID + "/savedProperties.xml");
		savedProps.storeToXML(out, "Color Storage");
		out.close();
	}

	public static void addRecentColor(int color) throws Exception {
		String colorStr = String.valueOf(color);
		for (int i = 9; i >= 1; i--) {
			String oldSpot = savedProps.getProperty("recent_"
					+ String.valueOf(i));
			savedProps.setProperty("recent_" + String.valueOf(i + 1), oldSpot);
		}
		savedProps.setProperty("recent_1", colorStr);
		saveInfo();
	}

	public static int getRecentColor(int i) {
		return Integer.valueOf(savedProps.getProperty(
				"recent_" + String.valueOf(i), "16777215"));
	}

	public static void addCustomColor(int color) throws Exception {
		String colorStr = String.valueOf(color);
		boolean isNull = false;

		for (int i = 1; isNull == false; i++) {
			if (getCustomColor(i) == -1) {
				isNull = true;
				savedProps.setProperty("custom_" + String.valueOf(i), colorStr);
			}
		}
		saveInfo();
	}

	public static int getCustomColor(int boxIndex) {
		return Integer.valueOf(savedProps.getProperty(
				"custom_" + String.valueOf(boxIndex), "-1"));
	}

	public static void removeCustomColor(int boxIndex, int indexLimit)
			throws Exception {
		savedProps.remove("custom_" + String.valueOf(boxIndex));
		for (int i = boxIndex; i < indexLimit; i++) {
			if (getCustomColor(i + 1) != -1) {
				savedProps.setProperty("custom_" + String.valueOf(i),
						String.valueOf(getCustomColor(i + 1)));
			} else {
				savedProps.remove("custom_" + String.valueOf(i));
			}
		}
		saveInfo();
	}

	public static void setLastTP(String tp) throws Exception {
		savedProps.setProperty("LastTexturePackName", tp);
		saveInfo();
	}

	public static String getLastTP() {
		return savedProps.getProperty("LastTexturePackName", "none");
	}

	public static void setTransferColor(int color) throws Exception {
		savedProps.setProperty("transfer", String.valueOf(color));
		saveInfo();
	}

	public static int getTransferColor() {
		int color = Integer.valueOf(savedProps.getProperty("transfer",
				"16777215"));
		return color;
	}

	public static void setTransferMode(String mode) throws Exception {
		savedProps.setProperty("mode", mode);
		saveInfo();
	}

	public static String getTransferMode() {
		return savedProps.getProperty("mode", "none");
	}

}
