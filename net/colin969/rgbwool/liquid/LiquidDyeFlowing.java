package net.colin969.rgbwool.liquid;

import net.minecraft.block.BlockFlowing;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;

public class LiquidDyeFlowing extends BlockFlowing{

	public Icon[] blockIcons = new Icon[2];
	
	public LiquidDyeFlowing(int par1) {
		super(par1, Material.lava);
		this.blockHardness = 100.0F;
		this.setLightOpacity(3);
	}
	
	@Override
	public void registerIcons(IconRegister ir){
		this.blockIcons[0] = ir.registerIcon("RGBBlocks:LiquidDye");
		this.blockIcons[1] = ir.registerIcon("RGBBlocks:LiquidDye_Flow");
	}
	
	@Override
	public Icon getIcon(int side, int meta){
		return blockIcons[1];
	}

}
