package net.colin969.rgbwool.liquid;

import net.minecraft.block.BlockStationary;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;

public class LiquidDyeStill extends BlockStationary{

	public Icon[] blockIcons = new Icon[2];
	
	public LiquidDyeStill(int par1) {
		super(par1, Material.lava);
		this.blockHardness = 100.0F;
		this.setLightOpacity(3);
	}
	
	@Override
	public void registerIcons(IconRegister ir){
		this.blockIcons[0] = ir.registerIcon("RGBBlocks:LiquidDye");
		this.blockIcons[1] = ir.registerIcon("RGBBlocks:LiquidDye_Flow");
	}

	@Override
	public Icon getIcon(int side, int meta){
		return blockIcons[0];
	}
}
