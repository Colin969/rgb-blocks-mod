package net.colin969.rgbwool.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelFactoryBlock extends ModelBase
{
    ModelRenderer Cube;
    ModelRenderer SideX;
    ModelRenderer SideZ;
    ModelRenderer SideX2;
    ModelRenderer SideZ2;
    ModelRenderer Top;
    ModelRenderer Bottom;
  
  public ModelFactoryBlock()
  {
    textureWidth = 128;
    textureHeight = 64;
    
      Cube = new ModelRenderer(this, 0, 0);
      Cube.addBox(0F, 0F, 0F, 13, 13, 13);
      Cube.setRotationPoint(0F, 0F, 0F);
      Cube.setTextureSize(128, 64);
      Cube.mirror = true;
      setRotation(Cube, 0F, 0F, 0F);
      SideX = new ModelRenderer(this, 52, 0);
      SideX.addBox(0F, 0F, 0F, 9, 9, 3);
      SideX.setRotationPoint(2F, 2F, -3F);
      SideX.setTextureSize(128, 64);
      SideX.mirror = true;
      setRotation(SideX, 0F, 0F, 0F);
      SideZ = new ModelRenderer(this, 52, 12);
      SideZ.addBox(-3F, 0F, 0F, 3, 9, 9);
      SideZ.setRotationPoint(0F, 2F, 2F);
      SideZ.setTextureSize(128, 64);
      SideZ.mirror = true;
      setRotation(SideZ, 0F, 0F, 0F);
      SideX2 = new ModelRenderer(this, 52, 0);
      SideX2.addBox(0F, 0F, 0F, 9, 9, 3);
      SideX2.setRotationPoint(2F, 2F, 13F);
      SideX2.setTextureSize(128, 64);
      SideX2.mirror = true;
      setRotation(SideX, 0F, 0F, 0F);
      SideZ2 = new ModelRenderer(this, 52, 12);
      SideZ2.addBox(0F, 0F, 0F, 3, 9, 9);
      SideZ2.setRotationPoint(13F, 2F, 2F);
      SideZ2.setTextureSize(128, 64);
      SideZ2.mirror = true;
      setRotation(SideZ, 0F, 0F, 0F);
      Top = new ModelRenderer(this, 0, 26);
      Top.addBox(0F, 0F, 0F, 9, 3, 9);
      Top.setRotationPoint(2F, -3F, 2F);
      Top.setTextureSize(128, 64);
      Top.mirror = true;
      setRotation(Top, 0F, 0F, 0F);
      Bottom = new ModelRenderer(this, 0, 26);
      Bottom.addBox(0F, 0F, 0F, 9, 3, 9);
      Bottom.setRotationPoint(2F, 13F, 2F);
      Bottom.setTextureSize(128, 64);
      Bottom.mirror = true;
      setRotation(Bottom, 0F, 0F, 0F);
  }
  
  public void render(boolean[] sides)
  {
    Cube.render(0.0625F);
    if(sides[4])
    	SideX.render(0.0625F);
    if(sides[3])
    	SideZ.render(0.0625F);
    if(sides[5])
    	SideX2.render(0.0625F);
    if(sides[2])
    	SideZ2.render(0.0625F);
    if(sides[1])
    	Top.render(0.0625F);
    if(sides[0])
    	Bottom.render(0.0625F);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
