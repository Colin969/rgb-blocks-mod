package net.colin969.rgbwool.network;

import java.util.ArrayList;

import net.colin969.rgbwool.entity.TileEntityDyeMachine;
import net.minecraft.world.World;

public class BlockNetwork {

	ArrayList<int[]> positions;
	int networkID;
	
	public BlockNetwork(int networkID, int x, int y, int z){
		this.networkID = networkID;
		positions = new ArrayList<int[]>();
		int[] pos = {x,y,z};
		positions.add(pos);
	}
	
	public void addMachine(int x, int y, int z){
		int[] pos = {x,y,z};
		positions.add(pos);
	}
	
	public void removeMachine(int x, int y, int z){
		int[] pos = {x,y,z};
		positions.remove(pos);
		positions.trimToSize();
	}
	
	public void update(World worldObj){
		for(int[] is : positions){
			TileEntityDyeMachine te = (TileEntityDyeMachine) worldObj.getBlockTileEntity(is[0], is[1], is[2]);
		}
	}
	
}
