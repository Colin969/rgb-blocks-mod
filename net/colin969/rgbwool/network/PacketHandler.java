package net.colin969.rgbwool.network;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.container.ContainerDyeMachine;
import net.colin969.rgbwool.entity.TileEntityDyeMachine;
import net.colin969.rgbwool.entity.TileEntityRGB;
import net.colin969.rgbwool.item.ItemBlockInjector;
import net.minecraft.block.Block;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.common.DummyModContainer;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;

public class PacketHandler implements IPacketHandler {

        @Override
        public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player playerEntity) {
        	if ("WoolDescription".equals(packet.channel)) {
        		//System.out.println("got wool info!");
        		try {

            		DataInputStream input = new DataInputStream(new ByteArrayInputStream(packet.data));

            		int x = input.readInt();
            		int y = input.readInt();
            		int z = input.readInt();
            		int top = input.readInt();
            		int bot = input.readInt();
            		int nor = input.readInt();
            		int sou = input.readInt();
            		int eas = input.readInt();
            		int wes = input.readInt();
            		int blockId = input.readInt();
            		int blockMetadata = input.readInt();
            		//int lightValue = input.readInt();
            		if (playerEntity instanceof EntityClientPlayerMP) {
            			World world = ((EntityClientPlayerMP)playerEntity).worldObj;
            			TileEntity tEnt = world.getBlockTileEntity(x, y, z);
            			if (tEnt instanceof TileEntityRGB) {
            				TileEntityRGB block = (TileEntityRGB) tEnt;
            				block.Top = top;
            				block.Bottom = bot;
            				block.North = nor;
            				block.South = sou;
            				block.East = eas;
            				block.West = wes;
            				block.blockId = blockId;
            				block.blockMetadata = blockMetadata;
            				//block.lightValue = lightValue;
        					world.markBlockForRenderUpdate(x, y, z);
            			}
            		}
        		} catch (Exception e) {
        			e.printStackTrace();
        		}
        	}
        	if("RenameInjector".equals(packet.channel)){
        		try{
        			if(playerEntity instanceof EntityPlayerMP){
        				DataInputStream input = new DataInputStream(new ByteArrayInputStream(packet.data));
        				String rename = input.readUTF();
        				((EntityPlayerMP) playerEntity).getCurrentEquippedItem().getTagCompound().setString("label", rename);
        			}
        		} catch (Exception e){
        			System.out.println(String.format("RGB Blocks - Failed to set %s's tool name!", ((EntityPlayerMP) playerEntity).username));
        		}
        	}
        	if("DyeMachineXYZ".equals(packet.channel)){
           		try {

            		DataInputStream input = new DataInputStream(new ByteArrayInputStream(packet.data));
            		
            		int color = input.readInt();
            		int x = input.readInt();
            		int y = input.readInt();
            		int z = input.readInt();
            		boolean open = input.readBoolean();
            		//System.out.println(String.valueOf(x) + ":" + String.valueOf(y) + ":" + String.valueOf(z));
            		
            		if (playerEntity instanceof EntityPlayerMP) {
            			TileEntityDyeMachine te = (TileEntityDyeMachine) ((EntityPlayerMP) playerEntity).worldObj.getBlockTileEntity(x, y, z);
            			if(te != null)
            				te.color = color;
            			((EntityPlayerMP) playerEntity).worldObj.markBlockForUpdate(x, y, z);
            			te.onInventoryChanged();
            			if(open){
            				((EntityPlayerMP) playerEntity).openGui(RGBBlock.instance, 0, ((EntityPlayerMP) playerEntity).worldObj, x, y, z);
            			}
            		} else if (playerEntity instanceof EntityClientPlayerMP){
            			((EntityClientPlayerMP) playerEntity).worldObj.markBlockForRenderUpdate(x, y, z);
            		}
            		
        		} catch (Exception e) {
        			e.printStackTrace();
        		}
        	}
        	if ("WoolInjectorCI".equals(packet.channel)) {
        		try {

            		DataInputStream input = new DataInputStream(new ByteArrayInputStream(packet.data));

            		int color = input.readInt();
            		int mode = input.readInt();
            		if (playerEntity instanceof EntityPlayerMP) {
            			if (((EntityPlayerMP)playerEntity).getCurrentEquippedItem().getItem() instanceof ItemBlockInjector){
            				
            				ItemStack stackInHand = ((EntityPlayerMP)playerEntity).getCurrentEquippedItem();
            				
            					NBTTagCompound nbt = stackInHand.getTagCompound();
            					if(nbt != null){
            						nbt.setInteger("color", color);
            						nbt.setInteger("Mode", mode);
            						stackInHand.setTagCompound(nbt);
            					} else {
            						nbt = new NBTTagCompound();
            						nbt.setInteger("color", color);
                					nbt.setInteger("Mode", mode);
                					stackInHand.setTagCompound(nbt);
            					}
            				//System.out.println("Packet Taken");
            			}
            		}
        		} catch (Exception e) {
        			e.printStackTrace();
        		}
        	}
        }
        public static String bytesToStringUTFNIO(byte[] bytes) {
        	 CharBuffer cBuffer = ByteBuffer.wrap(bytes).asCharBuffer();
        	 return cBuffer.toString();
        }

	}