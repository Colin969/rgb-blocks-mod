package net.colin969.rgbwool;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import com.google.common.collect.BiMap;

import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.ModContainer;

import net.minecraft.client.Minecraft;

public class Updater extends JFrame implements ActionListener{

    private	String oldVer;
	private String newVer;
	private String newMcVer;
	private String path;
	public static final String FORUM = "http://www.minecraftforum.net/topic/1812981-152-smp-rgb-blocks-v12-any-block-any-color-forge/";

	public void openUpdater(String newVer, String oldVer, String newMcVer, String path, boolean isMcNewer){
		this.newVer = newVer;
		this.oldVer = oldVer;
		this.path = path;
		this.newMcVer = newMcVer;
		openUpdater(new JPanel(),isMcNewer,false);
	}
	
	public void openUpdater(JPanel panel, boolean isMcNewer ,boolean inProgress){
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.setTitle("RGB Blocks Updater");
		this.setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		if(!isMcNewer)
			this.setBounds(dim.width / 2, dim.height / 2, 620, 150);
		else
			this.setBounds(dim.width / 2, dim.height / 2, 550, 150);
		this.setDefaultLookAndFeelDecorated(true);
		
		GridBagLayout layout = new GridBagLayout();
		panel.setLayout(layout);
		GridBagConstraints c = new GridBagConstraints();
		JLabel label;
		if(!inProgress && !isMcNewer)
			label = new JLabel("New Update! - Version " + newVer + " - Apply Update?", SwingConstants.CENTER);
		else if (isMcNewer)
			label = new JLabel("New Update! - Version " + newVer + " For MC " + newMcVer);
		else
			label = new JLabel("Updating. Will Close On End. Restart After.");
		label.setFont(new Font("Arial", Font.BOLD, 28));
		
		
		JButton yes;
		if(isMcNewer){
			yes = new JButton("Cannot Auto Update");
			yes.setEnabled(false);
		}else
			yes = new JButton("Yes - Must Relaunch");
		yes.setActionCommand("update");
		yes.addActionListener(this);
		
		JButton no;
		if(!isMcNewer)
			no = new JButton("Not Now");
		else
			no = new JButton("Close");
		no.setActionCommand("no");
		no.addActionListener(this);
		
		JButton never = new JButton("Never Ask Again");
		never.setActionCommand("never");
		never.addActionListener(this);
		
		JButton forum = new JButton("Open Forum Thread");
		forum.setActionCommand("forum");
		forum.addActionListener(this);
		
		c.insets = new Insets(5,5,5,5);
		
		c.weighty = 1.5;
		c.fill = c.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 1;
		panel.add(yes, c);
		c.fill = c.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 1;
		panel.add(no, c);
		c.fill = c.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 1;
		panel.add(never, c);
		c.fill = c.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 3;
		c.gridy = 1;
		panel.add(forum, c);
		
		
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;       
		c.weighty = 1;   
		c.insets = new Insets(10,0,0,0);  
		c.gridx = 0;       
		c.gridwidth = 4;  
		c.gridy = 0;       
		c.anchor = c.PAGE_START;
		panel.add(label, c);
		
		EmptyBorder eb = new EmptyBorder(4,4,4,4);
		panel.setBorder(eb);
		
		this.setContentPane(panel);
		this.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
		if("update".equals(event.getActionCommand())){
			try {
				Logger.log("Beginning Update...");
				update();
			} catch (Exception e) {
				Logger.log("Failed to Update!");
				e.printStackTrace();
			}
			System.exit(JFrame.EXIT_ON_CLOSE);
		} else if ("never".equals(event.getActionCommand())){
			Logger.log("Disabling Updater...");
			RGBBlock.configUpdateTempStorage.get("core", "Enable Updater", true).set(false);
			RGBBlock.configUpdateTempStorage.save();
			RGBBlock.configUpdateTempStorage = null;
			this.dispose();
		} else if ("no".equals(event.getActionCommand())) {
			RGBBlock.configUpdateTempStorage = null;
			this.dispose();
		} else {
			try {
				Logger.log("Opening Forum Thread...");
				Desktop.getDesktop().browse(new URI(FORUM));
			} catch (Exception e) {
				Logger.log("Failed to open Forum Thread!");
				e.printStackTrace();
				this.dispose();
			}
		}
		
		
	}

	private void update() throws IOException {
		this.dispose();
		openUpdater(new JPanel(),false,true);
		
		URL website = new URL("http://www.colinberry.co.uk/downloads/" + newMcVer + "/RGBBlock%20-%20" + newVer + ".zip");
	    ReadableByteChannel rbc = Channels.newChannel(website.openStream());
	    FileOutputStream fos = new FileOutputStream(Minecraft.getMinecraftDir() + "/mods/RGBBlock - " + newVer + ".zip");
	    fos.getChannel().transferFrom(rbc, 0, 1 << 24);
	    
	    Logger.log("New Version Downloaded! - " + newVer);
	    
	    List<ModContainer> mods = Loader.instance().getModList();
	    BiMap<ModContainer, Object> mods2 = Loader.instance().getModObjectList();
	    
	    new File(path).deleteOnExit();
	    Logger.log("Marked For Deletion! - " + oldVer + " - " + path);
		
	}
	
}
