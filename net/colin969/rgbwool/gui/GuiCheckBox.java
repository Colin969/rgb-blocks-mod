package net.colin969.rgbwool.gui;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.Tessellator;

public class GuiCheckBox extends GuiButton {

	private boolean state;
	private boolean isCheckBox;

	public GuiCheckBox(int i, int j, int k, int l, int m, String string, boolean isCheckBox) {
		super(i,j,k,l,m,string);
		this.isCheckBox = isCheckBox;
	}

	@Override
    public void drawButton(Minecraft par1Minecraft, int par2, int par3)
    {
        if (this.drawButton)
        {
            FontRenderer fontrenderer = par1Minecraft.fontRenderer;
            par1Minecraft.renderEngine.bindTexture("/gui/gui.png");
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.field_82253_i = par2 >= this.xPosition && par3 >= this.yPosition && par2 < this.xPosition + this.width && par3 < this.yPosition + this.height;
            int k = this.getHoverState(this.field_82253_i);
            this.drawTexturedModalRect(this.xPosition, this.yPosition, 0, 46 + k * 20, this.width / 2, this.height);
            this.drawTexturedModalRect(this.xPosition + this.width / 2, this.yPosition, 200 - this.width / 2, 46 + k * 20, this.width / 2, this.height);
            this.mouseDragged(par1Minecraft, par2, par3);
            int l = 16777215;
            if(isCheckBox){
	            if (!state)
	            {
	            	this.displayString = "Magnify";
	            } else {
	                this.displayString = "Normal";
	            }
            }

            this.drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, l);
        }
    }

	public boolean isChecked() {
		return state;
	}
	
	public void toggle() {
		if(state)
			state = false;
		else
			state = true;
	}

}