package net.colin969.rgbwool.gui;

import org.lwjgl.opengl.GL11;

import net.colin969.rgbwool.ModProperties;
import net.colin969.rgbwool.RGBBlock;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSmallButton;
import net.minecraft.entity.player.EntityPlayer;

public class GuiScreenLibrary extends GuiScreen{
	
	private static final int imageWidth = 176;
	private static final int imageHeight = 106;
	private int pageNumber = 1;
	private EntityPlayer editingPlayer;
	private int PosX;
	private int PosY;
	private int PosZ;
	private boolean mode = false;
	
	public GuiScreenLibrary(EntityPlayer player, int x, int y, int z) {
		editingPlayer = player;
		PosX = x;
		PosY = y;
		PosZ = z;
		
	}
	
    @Override
	public void initGui() {
    	addColorButtons();
    }
    public void addColorButtons(){
        int x = (width - imageWidth) / 2;
        int y = (height - imageHeight) / 2;
    	
        String modeStrT;
        if(mode){
        	modeStrT = "Select";
        } else {
        	modeStrT = "Delete";
        }
    	buttonList.clear();
        buttonList.add(new GuiSmallButton(1, x + 4, y + 4,20,10,"<-"));
        buttonList.add(new GuiSmallButton(2, x + 80, y + 4,20,10,"->"));
        buttonList.add(new GuiSmallButton(3, x + 102, y + 4, 40, 10, modeStrT));
        buttonList.add(new GuiSmallButton(4, x + 144, y + 4, 28, 10, "Back"));
        
        int indexLimit = Integer.valueOf(getPageLimit(2));
    	
        boolean flag = false;
        for(int i = 1;flag == false;i++){
        	if(i < indexLimit && i <= 50){
        		double y1 = ((double)i / 10);
        		int x1 = (i % 10) + 1;
            	if(y1 == (int) y1)
            		x1 = 11;
        		if((ModProperties.getCustomColor(i + ((pageNumber - 1) * 50))) != -1)
        				buttonList.add(new GuiColorButton(4 + i, x - 23 + (x1 * 16),(int) (y + 2 + Math.ceil(y1) * 16), 15, 15, ""));
        	} else {
        		flag = true;
        	}
        }
    }
    
	protected void actionPerformed(GuiButton guibutton) {
		int id = guibutton.id;
		if(id == 1){
			if(pageNumber - 1 <= Integer.valueOf(getPageLimit(1)) && pageNumber - 1 > 0){
				pageNumber--;
				addColorButtons();
			}
		}
		if(id == 2){
			if(pageNumber + 1 <= Integer.valueOf(getPageLimit(1)) && pageNumber + 1 < 100){
				pageNumber++;
				addColorButtons();
			}
		}
		if(id == 3){
			if(mode)
				mode = false;
			else
				mode = true;
			addColorButtons();
		}
		if(id == 4){
			mc.displayGuiScreen(new GuiScreenBlockInjector(editingPlayer,PosX,PosY,PosZ));
		}
		if(id > 4){
			if(ModProperties.getCustomColor(id - 4) != -1){
				if(mode){
					try {
						ModProperties.removeCustomColor(id - 4, Integer.valueOf(getPageLimit(2)));
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
					addColorButtons();
				} else {
				try {
			    	int pageInteger = (pageNumber - 1) * 50;
					ModProperties.setTransferMode("setColorFromLibrary");
					ModProperties.setTransferColor(ModProperties.getCustomColor((id - 4) + pageInteger));
					mc.displayGuiScreen(new GuiScreenBlockInjector(editingPlayer, PosX, PosY, PosZ));
				} catch (Exception e) {
					e.printStackTrace();
				}
				}
			}
		}
	}
	
	@Override
    public void drawScreen(int par1, int par2, float par3){
    			int x = (width - imageWidth) / 2;
    			int y = (height - imageHeight) / 2;
    			int indexLimit = Integer.valueOf(getPageLimit(2));
    			
    			GL11.glColor4f(1.0F,1.0F,1.0F, 1.0F);
    			this.mc.renderEngine.bindTexture("/mods/"+RGBBlock.MODID+"/textures/gui/ColorLibraryGUI.png");
                this.drawTexturedModalRect(x, y, 0, 0, imageWidth, imageHeight);
                
                for(int i = 1;i <= indexLimit;i++)
                	drawColorBox(x,y,i);
                
                super.drawScreen(par1, par2, par3);
                
                fontRenderer.drawString("Page " + pageNumber + "/" + getPageLimit(1), x + 26, y + 5, 4210752, false);
	}

	private String getPageLimit(int mode) {
		boolean flag = false;
		double pageLimit = 0;
		int indexLimit = 0;
		
		for(int i = 1;flag == false;i++){
			if(ModProperties.getCustomColor(i) == -1){
				indexLimit = i;
				flag = true;
			}
		}
		pageLimit = (double) indexLimit / 50;
		if(mode == 1)
			return String.valueOf((int)(Math.ceil(pageLimit)));
		if(mode == 2)
			return String.valueOf(indexLimit);
		return "INVALID";
	}

	private void drawColorBox(int x, int y, int boxIndex) {
    	int pageInteger = (pageNumber - 1) * 50;
	    boxIndex += pageInteger;
		int color = ModProperties.getCustomColor(boxIndex);
        float f = (float)(color >> 16 & 255) / 255.0F;
        float f1 = (float)(color >> 8 & 255) / 255.0F;
        float f2 = (float)(color & 255) / 255.0F;
        int indexLimit = Integer.valueOf(getPageLimit(2));
        
        if(boxIndex < indexLimit && boxIndex <= pageNumber * 50 && boxIndex > pageNumber - 1 * 50){

        	boxIndex -= pageInteger;
        	double y1 = ((double)boxIndex / 10);
        	int x1 = (boxIndex % 10) + 1;
        	
        	if(y1 == (int) y1)
        		x1 = 11;
        	
        	GL11.glColor4f(f,f1,f2, 1.0F);
			this.mc.renderEngine.bindTexture("/mods/"+RGBBlock.MODID+"/textures/gui/ColorBox.png");
			this.drawTexturedModalRect(x - 32 + (x1 * 16),(int) (y - 82 + Math.ceil(y1) * 16), 0, 0, imageWidth, imageHeight);
        }
	}
}
