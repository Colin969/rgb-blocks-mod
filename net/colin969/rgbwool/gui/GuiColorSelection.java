package net.colin969.rgbwool.gui;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.List;

import net.colin969.rgbwool.ModProperties;
import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.lang.LocalizationHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.src.ModLoader;
import net.minecraft.util.StringTranslate;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.network.PacketDispatcher;

/**
 *
 * @author Home
 */
public class GuiColorSelection extends GuiScreen {
	
    static final List allowchars = Arrays.asList("0".charAt(0), "1".charAt(0),
            "2".charAt(0), "3".charAt(0), "4".charAt(0), "5".charAt(0),
            "6".charAt(0), "7".charAt(0), "8".charAt(0), "9".charAt(0));
    static int gRed = 100, gGreen = 200, gBlue = 255, tempcolor;
    static BufferedImage finishedTex = null, finishedGradient = null;
    private EntityPlayer player;
    private GuiTextField textFieldRed;
    private GuiTextField textFieldGreen;
    private GuiTextField textFieldBlue;
    private GuiTextField name;
    private int textx, redy, greeny, bluey, namey;
    private int sliderY = -10000;
    private int targetx = -10000;
    private int targety = -10000;
    private boolean magnify;
    GuiCheckBox magCheckBox;
    public int originalGuiScale = -1;
    ItemStack itemStackInjector;
    private int PosX;
    private int PosY;
    private int PosZ;
    private int imageHeight = 102;
    private int imageWidth = 176;
    private boolean returnToServer = false;

    public GuiColorSelection(EntityPlayer player, int x, int y, int z, boolean isServerside, int curColor) {
        super();
        returnToServer = isServerside;
        PosX = x;
        PosY = y;
        PosZ = z;
        buttonList.clear();
        this.player = player;
        textx = (((width - 256) / 2) - 75) + 256 + 24;
        redy = ((height - 256) / 2) + 12;
        greeny = ((height - 256) / 2) + 36;
        bluey = ((height - 256) / 2) + 60;
    	if(!isServerside)
        namey = ((height - 256) / 2) + 84;
        if(!isServerside){
        	itemStackInjector = player.getCurrentEquippedItem();
        	tempcolor = itemStackInjector.getTagCompound().getInteger("color");
        } else {
        	tempcolor = curColor;
        }
        Color color = new Color(tempcolor);
        textFieldRed = new GuiTextField(fontRenderer, textx, redy, 100, 10);
        textFieldGreen = new GuiTextField(fontRenderer, textx, greeny, 100, 10);
        textFieldBlue = new GuiTextField(fontRenderer, textx, bluey, 100, 10);
    	if(!isServerside)
        name = new GuiTextField(fontRenderer, textx, namey, 100, 10);

        textFieldRed.setEnableBackgroundDrawing(true);
        textFieldRed.setFocused(true);
        textFieldRed.setTextColor(Color.red.getRGB());
        textFieldRed.setMaxStringLength(3);
        textFieldRed.setCanLoseFocus(true);
        textFieldRed.setText(String.valueOf(color.getRed()));

        textFieldGreen.setEnableBackgroundDrawing(true);
        textFieldGreen.setFocused(false);
        textFieldGreen.setTextColor(Color.green.getRGB());
        textFieldGreen.setMaxStringLength(3);
        textFieldGreen.setCanLoseFocus(true);
        textFieldGreen.setText(String.valueOf(color.getGreen()));

        textFieldBlue.setEnableBackgroundDrawing(true);
        textFieldBlue.setFocused(false);
        textFieldBlue.setTextColor(Color.blue.getRGB());
        textFieldBlue.setMaxStringLength(3);
        textFieldBlue.setCanLoseFocus(true);
        textFieldBlue.setText(String.valueOf(color.getBlue()));
    	if(!isServerside){
	        name.setEnableBackgroundDrawing(true);
	        name.setFocused(false);
	        name.setTextColor(Color.WHITE.getRGB());
	        name.setMaxStringLength(16);
	        name.setCanLoseFocus(true);
	        if (Minecraft.getMinecraft().thePlayer.inventory.getCurrentItem() != null) {
	            name.setText(Minecraft.getMinecraft().thePlayer.inventory.getCurrentItem().getDisplayName());
	        }
    	}
    }

    @Override
    public void initGui() {
        /*if (mc.gameSettings.guiScale == 0 || mc.gameSettings.guiScale == 3) {
            GuiColorSelection fixed = new GuiColorSelection(player, PosX, PosY, PosZ, returnToServer);
            fixed.originalGuiScale = mc.gameSettings.guiScale;
            if (this.height < 256) {
                mc.gameSettings.guiScale = 1;
            }else{
                mc.gameSettings.guiScale = 2;
            }
            ModLoader.openGUI(player, fixed);
        }*/
        buttonList.clear();
        StringTranslate strTrans = StringTranslate.getInstance();
        this.buttonList.add(new GuiCheckBox(1, (width / 2) + 76,
                this.height / 2 + 100, 100, 20, strTrans
                .translateKey("gui.done"), false));
        this.buttonList.add(new GuiCheckBox(2, (width / 2) + 76, this.height / 2 + 80, 100, 20, "Magnify", true));
        textx = (((width - 256) / 2) - 75) + 256 + 24;
        redy = ((height - 256) / 2) + 12;
        greeny = ((height - 256) / 2) + 36;
        bluey = ((height - 256) / 2) + 60;
        namey = ((height - 256) / 2) + 84;
        if(!returnToServer)
        tempcolor = itemStackInjector.getTagCompound().getInteger("Color");
        Color color = new Color(tempcolor);
        textFieldRed = new GuiTextField(fontRenderer, textx, redy, 97, 10);
        textFieldGreen = new GuiTextField(fontRenderer, textx, greeny, 97, 10);
        textFieldBlue = new GuiTextField(fontRenderer, textx, bluey, 97, 10);
        if(!returnToServer)
        name = new GuiTextField(fontRenderer, textx, namey, 97, 10);

        textFieldRed.setEnableBackgroundDrawing(true);
        textFieldRed.setFocused(true);
        textFieldRed.setTextColor(Color.red.getRGB());
        textFieldRed.setMaxStringLength(3);
        textFieldRed.setCanLoseFocus(true);
        textFieldRed.setText(String.valueOf(color.getRed()));

        textFieldGreen.setEnableBackgroundDrawing(true);
        textFieldGreen.setFocused(false);
        textFieldGreen.setTextColor(Color.green.getRGB());
        textFieldGreen.setMaxStringLength(3);
        textFieldGreen.setCanLoseFocus(true);
        textFieldGreen.setText(String.valueOf(color.getGreen()));

        textFieldBlue.setEnableBackgroundDrawing(true);
        textFieldBlue.setFocused(false);
        textFieldBlue.setTextColor(Color.blue.getRGB());
        textFieldBlue.setMaxStringLength(3);
        textFieldBlue.setCanLoseFocus(true);
        textFieldBlue.setText(String.valueOf(color.getBlue()));
        if(!returnToServer){
	        name.setEnableBackgroundDrawing(true);
	        name.setFocused(false);
	        name.setTextColor(Color.WHITE.getRGB());
	        name.setMaxStringLength(16);
	        name.setCanLoseFocus(true);
	        if (Minecraft.getMinecraft().thePlayer.inventory.getCurrentItem() != null) {
	        	name.setText(Minecraft.getMinecraft().thePlayer.inventory.getCurrentItem().getDisplayName());
	        }
        }
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
    }

    @Override
    protected void keyTyped(char par1, int par2) {
        super.keyTyped(par1, par2);
        try {
        	if(!returnToServer){
	            if (name.isFocused()) {
	                textFieldRed.setFocused(false);
	                textFieldGreen.setFocused(false);
	                textFieldBlue.setFocused(false);
	                name.textboxKeyTyped(par1, par2);
	                return;
	            }
        	}
            if (allowchars.contains(par1) | Character.getNumericValue(par2) == -1) {
                if (textFieldRed.isFocused()) {
                    textFieldRed.textboxKeyTyped(par1, par2);
                    if (textFieldRed.getText().length() == 3) {
                        if (Integer.parseInt(textFieldRed.getText()) > 255) {
                            textFieldRed.setText(String.valueOf(255));
                        }
                    }
                    Color color = new Color(tempcolor);
                    tempcolor = (new Color(parseColor(textFieldRed.getText()), color.getGreen(), color.getBlue())).getRGB();
                }
                if (textFieldGreen.isFocused()) {
                    textFieldGreen.textboxKeyTyped(par1, par2);
                    if (textFieldGreen.getText().length() == 3) {
                        if (Integer.parseInt(textFieldGreen.getText()) > 255) {
                            textFieldGreen.setText(String.valueOf(255));
                        }
                    }
                    Color color = new Color(tempcolor);
                    tempcolor = (new Color(color.getRed(), parseColor(textFieldGreen.getText()), color.getBlue())).getRGB();
                }
                if (textFieldBlue.isFocused()) {
                    textFieldBlue.textboxKeyTyped(par1, par2);
                    if (textFieldRed.getText().length() == 3) {
                        if (Integer.parseInt(textFieldGreen.getText()) > 255) {
                            textFieldBlue.setText(String.valueOf(255));
                        }
                    }
                    Color color = new Color(tempcolor);
                    tempcolor = (new Color(color.getRed(), color.getGreen(), parseColor(textFieldBlue.getText()))).getRGB();
                }
            }
        } catch (Throwable ex) {
        }
    }

    private int parseColor(String string) {
        try {
            if ("".equals(string) || string == null) {
                return 0;
            } else {
                return Integer.parseInt(string);
            }
        } catch (Throwable ex) {
            return 0;
        }
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        textFieldRed.updateCursorCounter();
        textFieldGreen.updateCursorCounter();
        textFieldBlue.updateCursorCounter();
        if(!returnToServer)
        name.updateCursorCounter();
    }

    @Override
    protected void actionPerformed(GuiButton par1GuiButton) {
        switch (par1GuiButton.id) {
            case 1:
                try {
                	if(!returnToServer){
	        			ModProperties.setTransferMode("setColorFromLibrary");
	        			System.out.println(tempcolor);
	        			ModProperties.setTransferColor(tempcolor);
                		ByteArrayOutputStream bos = new ByteArrayOutputStream(8);
                		DataOutputStream os = new DataOutputStream(bos);
                		try {
                			os.writeUTF(name.getText());
                		} catch (IOException e) {
                			e.printStackTrace();
                		}

                		Packet250CustomPayload packet = new Packet250CustomPayload();
                		packet.channel = "RenameInjector";
                		packet.data = bos.toByteArray();
                		packet.length = bos.size();

                		PacketDispatcher.sendPacketToServer(packet);
                		mc.displayGuiScreen(null);
	        			mc.displayGuiScreen(new GuiScreenBlockInjector(player, PosX, PosY, PosZ));
                	} else {
                		ByteArrayOutputStream bos = new ByteArrayOutputStream(8);
                		DataOutputStream os = new DataOutputStream(bos);
                		try {
                			os.writeInt(tempcolor);
                			os.writeInt(PosX);
                			os.writeInt(PosY);
                			os.writeInt(PosZ);
                			os.writeBoolean(true);
                		} catch (IOException e) {
                			e.printStackTrace();
                		}

                		Packet250CustomPayload packet = new Packet250CustomPayload();
                		packet.channel = "DyeMachineXYZ";
                		packet.data = bos.toByteArray();
                		packet.length = bos.size();

                		PacketDispatcher.sendPacketToServer(packet);
                		mc.displayGuiScreen(null);
                	}
        		} catch (Exception e) {
        			e.printStackTrace();
        		}
            case 2:
            	((GuiCheckBox) buttonList.get(1)).toggle();
        }

    }
    
    public static byte[] stringToBytesUTFNIO(String str) {
    	 char[] buffer = str.toCharArray();
    	 byte[] b = new byte[buffer.length << 1];
    	 CharBuffer cBuffer = ByteBuffer.wrap(b).asCharBuffer();
    	 for(int i = 0; i < buffer.length; i++)
    	 cBuffer.put(buffer[i]);
    	 return b;
    }


    @Override
    public boolean doesGuiPauseGame() {
        return true;
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        this.drawWorldBackground(0);
        this.drawContainer();
        this.drawGradient(gRed, gGreen, gBlue);
        this.drawColorbar();
        this.drawString(fontRenderer, "Preview",
                (((width - 256) / 2) - 78) + 256 + 64 - 8,
                ((height - 256) / 2) + 110, Integer.parseInt("FFFFFF", 16));
        this.drawGradientRect((((width - 256) / 2) - 110) + 256 + 64,
                ((height - 256) / 2) + 125,
                (((width - 256) / 2) - 40) + 256 + 80,
                ((height - 256) / 2) + 200, tempcolor, tempcolor);
        int sliderX = (((width - 256) / 2) - 75) + 256 + 8;
        this.mc.renderEngine.bindTexture("/mods/" + RGBBlock.MODID + "/textures/gui/CustomRenderIcons.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.drawTexturedModalRect128(sliderX - 1, sliderY - 2, 8, 0, 20, 9);
        this.mc.renderEngine.bindTexture("/mods/" + RGBBlock.MODID + "/textures/gui/CustomRenderIcons.png");
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.drawTexturedModalRect(targetx - 7, targety - 8, 0, 0, 16, 16);
        if (((GuiCheckBox) this.buttonList.get(1)).isChecked()) {
            try {
                if (((par1 >= (((width - 256) / 2) - 75)) && (par1 <= (((width - 256) / 2) - 75) + 256))
                        && ((par2 >= (height - 256) / 2) && (par2 <= ((height - 256) / 2) + 256))) {
                    int locW = par1 - (((width - 256) / 2) - 75);
                    int locH = par2 - (height - 256) / 2;
                    int color = finishedGradient.getRGB(locW, locH);
                    this.drawGradientRect(par1 - 1, par2 - 1, par1 + 25, par2 + 25, Color.black.getRGB(), Color.black.getRGB());
                    this.drawGradientRect(par1, par2, par1 + 24, par2 + 24, color, color);
                }
            } catch (Throwable ex) {
            }
        }
        super.drawScreen(par1, par2, par3);
    }

    protected void drawContainer() {
        int basedim = 256;
        int borderwidth = 8;
		int w = basedim + (borderwidth * 2) + 38, h = basedim
                + (borderwidth * 2);
        this.drawGradientRect((width - w) / 2 - 54, (height - h) / 2, (width - w) / 2 + w + 25, (height - h) / 2 + h, -6250336, -6250336);
        mc.renderEngine.bindTexture("/gui/gui.png");
        textFieldRed.drawTextBox();
        fontRenderer.drawString(LocalizationHelper.getLocalizedString("color.red"), (((width - 256) / 2) - 75) + 256 + 24,
                ((height - 256) / 2) + 3, 16711680);
        textFieldGreen.drawTextBox();
        fontRenderer.drawString(LocalizationHelper.getLocalizedString("color.green"), (((width - 256) / 2) - 75) + 256 + 24,
                ((height - 256) / 2) + 27, 65280);
        textFieldBlue.drawTextBox();
        fontRenderer.drawString(LocalizationHelper.getLocalizedString("color.blue"),(((width - 256) / 2) - 75) + 256 + 24,
                ((height - 256) / 2) + 51, 255);
        if(!returnToServer){
        	name.drawTextBox();
        	fontRenderer.drawString("Tool Name", (((width - 256) / 2) - 75) + 256 + 24,
                    ((height - 256) / 2) + 75, 14474460);
        }
        this.drawColortext();
    }

    public void drawTexturedModalRectScaled(int par1, int par2, int par3, int par4, int par5, int par6, float scale) {
        float var7 = scale;
        float var8 = scale;
        Tessellator var9 = Tessellator.instance;
        var9.startDrawingQuads();
        var9.addVertexWithUV((double) (par1 + 0), (double) (par2 + par6), (double) this.zLevel, (double) ((float) (par3 + 0) * var7), (double) ((float) (par4 + par6) * var8));
        var9.addVertexWithUV((double) (par1 + par5), (double) (par2 + par6), (double) this.zLevel, (double) ((float) (par3 + par5) * var7), (double) ((float) (par4 + par6) * var8));
        var9.addVertexWithUV((double) (par1 + par5), (double) (par2 + 0), (double) this.zLevel, (double) ((float) (par3 + par5) * var7), (double) ((float) (par4 + 0) * var8));
        var9.addVertexWithUV((double) (par1 + 0), (double) (par2 + 0), (double) this.zLevel, (double) ((float) (par3 + 0) * var7), (double) ((float) (par4 + 0) * var8));
        var9.draw();
    }

    public void drawTexturedModalRect128(int par1, int par2, int par3, int par4, int par5, int par6) {
        float var7 = 0.00390625F * 2F;
        float var8 = 0.00390625F * 2F;
        Tessellator var9 = Tessellator.instance;
        var9.startDrawingQuads();
        var9.addVertexWithUV((double) (par1 + 0), (double) (par2 + par6), (double) this.zLevel, (double) ((float) (par3 + 0) * var7), (double) ((float) (par4 + par6) * var8));
        var9.addVertexWithUV((double) (par1 + par5), (double) (par2 + par6), (double) this.zLevel, (double) ((float) (par3 + par5) * var7), (double) ((float) (par4 + par6) * var8));
        var9.addVertexWithUV((double) (par1 + par5), (double) (par2 + 0), (double) this.zLevel, (double) ((float) (par3 + par5) * var7), (double) ((float) (par4 + 0) * var8));
        var9.addVertexWithUV((double) (par1 + 0), (double) (par2 + 0), (double) this.zLevel, (double) ((float) (par3 + 0) * var7), (double) ((float) (par4 + 0) * var8));
        var9.draw();
    }

    protected void drawColortext() {
        Color color = new Color(tempcolor);
        
    }

    @Override
    protected void mouseClicked(int par1, int par2, int par3) {
        if ((par1 >= (((width - 256) / 2) - 75) + 256 + 8 && par1 <= (((width - 256) / 2) - 75) + 256 + 8 + 8)
                && (par2 >= ((height - 256) / 2) && par2 <= ((height - 256) / 2) + 256)) {
            try {
                int locH = par2 - ((height - 256) / 2);
                Color tmp = new Color(finishedTex.getRGB(2, 1536 - (locH * 6)));
                gRed = tmp.getRed();
                gGreen = tmp.getGreen();
                gBlue = tmp.getBlue();
                sliderY = par2;
                targetx = -10000;
                targety = -10000;
            } catch (Exception ex) {
            }
        }
        if (((par1 >= (((width - 256) / 2) - 75)) && (par1 <= (((width - 256) / 2) - 75) + 256))
                && ((par2 >= (height - 256) / 2) && (par2 <= ((height - 256) / 2) + 256))) {
            try {
                targetx = par1;
                targety = par2;
                int locW = par1 - (((width - 256) / 2) - 75);
                int locH = par2 - (height - 256) / 2;
                tempcolor = finishedGradient.getRGB(locW, locH);
                Color color = new Color(tempcolor);
                textFieldRed.setText(String.valueOf(color.getRed()));
                textFieldGreen.setText(String.valueOf(color.getGreen()));
                textFieldBlue.setText(String.valueOf(color.getBlue()));
            } catch (Exception Ex) {
                // Just to insure invalid coordinates didn't get passed. Just
                // ignore the click.
            }

        }
        if(!returnToServer)
        name.mouseClicked(par1, par2, par3);
        if (par1 >= textx && par1 <= (textx + textFieldRed.getWidth())) {
            if (par2 >= redy && par2 <= redy + 10) {
                textFieldRed.mouseClicked(par1, par2, par3);
                textFieldRed.setFocused(true);
                textFieldGreen.setFocused(false);
                textFieldBlue.setFocused(false);
            } else if (par2 >= greeny && par2 <= greeny + 10) {
                textFieldGreen.mouseClicked(par1, par2, par3);
                textFieldGreen.setFocused(true);
                textFieldRed.setFocused(false);
                textFieldBlue.setFocused(false);
            } else if (par2 >= bluey && par2 <= bluey + 10) {
                textFieldBlue.mouseClicked(par1, par2, par3);
                textFieldBlue.setFocused(true);
                textFieldGreen.setFocused(false);
                textFieldRed.setFocused(false);
            }
        }
        super.mouseClicked(par1, par2, par3);

    }

    protected void drawGradient(int startRed, int startBlue, int startGreen) {
        if (startRed < startBlue) {
            if (startRed < startGreen) {
                startRed = 0;
            } else {
                startGreen = 0;
            }
        } else if (startBlue < startGreen) {
            startBlue = 0;
        } else {
            startGreen = 0;
        }
        int w = 256, h = 256;
        int pix[] = new int[w * h];
        int locx = 0;
        finishedGradient = new BufferedImage(w, h, 1);
        for (int y = 0; y < 256; y++) {
            for (int x = 0; x < 256; x++) {
                pix[locx++] = (startRed + ((255 - startRed) * y / 255)) * x
                        / 255 * 65536
                        | (startGreen + ((255 - startGreen) * y / 255)) * x
                        / 255 * 256
                        | (startBlue + ((255 - startBlue) * y / 255)) * x / 255;
            }
        }
        finishedGradient.setRGB(0, 0, w, h, pix, 0, w);
        int textEnum = this.mc.renderEngine
                .allocateAndSetupTexture(finishedGradient);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        int locH = (height - h) / 2;
        int locW = (((width - 256) / 2) - 75);
        this.drawTexturedModalRect(locW, locH, 0, 0, h, w);
        this.mc.renderEngine.deleteTexture(textEnum);
    }

    protected void drawColorbar() {
        if (finishedTex == null) {
            int pix[] = new int[1536];
            int red = 0, blue = 0, green = 0, locx = 0;
            for (int y = 0; y < 6; y++) {
                for (int x = 0; x < 256; x++) {
                    switch (y) {
                        case 0:
                            red = 255;
                            blue = x;
                            green = 0;
                            break;
                        case 1:
                            blue = 255;
                            red = 255 - x;
                            green = 0;
                            break;
                        case 2:
                            blue = 255;
                            green = x;
                            red = 0;
                            break;
                        case 3:
                            green = 255;
                            blue = 255 - x;
                            red = 0;
                            break;
                        case 4:
                            green = 255;
                            red = x;
                            blue = 0;
                            break;
                        case 5:
                            red = 255;
                            green = 255 - x;
                            blue = 0;
                            break;
                    }
                    pix[locx++] = red * 65536 | green * 256 | blue;
                }
            }
            finishedTex = new BufferedImage(8, 1536, 1);
            // Quick Tiling Fix
            for (int x = 0; x < 8; x++) {
                finishedTex.setRGB(x, 0, 1, 1536, pix, 0, 1);
            }
        }
        int textEnum = this.mc.renderEngine
                .allocateAndSetupTexture(finishedTex);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        int locH = ((height - 256) / 2);
        int locW = (((width - 256) / 2) - 75) + 256 + 8;
        this.drawTexturedModalRect(locW, locH, 0, 0, 8, 256);
        this.mc.renderEngine.deleteTexture(textEnum);
    }
}