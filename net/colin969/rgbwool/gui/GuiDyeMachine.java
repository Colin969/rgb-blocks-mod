package net.colin969.rgbwool.gui;


import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.container.ContainerDyeMachine;
import net.colin969.rgbwool.entity.TileEntityDyeMachine;
import net.colin969.rgbwool.lang.LocalizationHelper;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiSmallButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

public class GuiDyeMachine extends GuiContainer {

		//private GuiTextField field;
		private World world;
		private ContainerDyeMachine dyeContainer;
		private EntityPlayer editingPlayer;
        public int red = 0;
        public int blue = 0;
        public int green = 0;
        private int count = 0;

        public GuiDyeMachine (EntityPlayer player,InventoryPlayer inventoryPlayer,
                        TileEntityDyeMachine tileEntity1, World world, int x, int y, int z) {
                super(new ContainerDyeMachine(inventoryPlayer, tileEntity1, world));
                this.dyeContainer = (ContainerDyeMachine) this.inventorySlots;
                this.editingPlayer = player;
                int color = dyeContainer.tileEntity.color;
                red = (color >> 16) & 0xFF;
                green = (color >> 8) & 0xFF;
                blue = color & 0xFF;
        }

		public void initGui() {
			super.initGui();
			//id, x, y, width, height, text
			buttonList.add(new GuiSmallButton(1, guiLeft+38, guiTop+20, 10, 10, "+"));
			buttonList.add(new GuiSmallButton(2, guiLeft+6, guiTop+20, 10, 10, "-"));
			buttonList.add(new GuiSmallButton(3, guiLeft+38, guiTop+30, 10, 10, "+"));
			buttonList.add(new GuiSmallButton(4, guiLeft+6, guiTop+30, 10, 10, "-"));
			buttonList.add(new GuiSmallButton(5, guiLeft+38, guiTop+40, 10, 10, "+"));
			buttonList.add(new GuiSmallButton(6, guiLeft+6, guiTop+40, 10, 10, "-"));
			buttonList.add(new GuiSmallButton(7, guiLeft+6, guiTop+52, 42, 10, "Pick"));
			//field = new GuiTextField(this.fontRenderer, 8, 56, 39, 10);
			//field.setText(Integer.toHexString((this.dyeContainer.tileEntity.color)));
		}
		int id = -1;
		protected void actionPerformed(GuiButton guibutton) {
            id = guibutton.id;
            if(id == 7){
            	mc.displayGuiScreen(new GuiColorSelection(editingPlayer, dyeContainer.tileEntity.xCoord, dyeContainer.tileEntity.yCoord, dyeContainer.tileEntity.zCoord, true, this.dyeContainer.tileEntity.color));
            }
		}
		@Override
		public void updateScreen() {
			super.updateScreen();
			if (id>=0&&id<=6) {

	            switch(id) {
	            case 1:
	                this.red += 1;
	                break;
	            case 2:
	            	this.red -= 1;
	                break;
	            case 3:
	            	this.green += 1;
	                    break;
	            case 4:
	            	this.green -= 1;
	        		break;
	            case 5:
	            	this.blue += 1;
	                break;
	            case 6:
	            	this.blue -= 1;
	            }
	            this.red = this.red & 0xFF;
	            this.blue = this.blue & 0xFF;
	            this.green = this.green & 0xFF;
	            this.dyeContainer.setCurrentColor(getColor());
	            this.dyeContainer.sendColorToServer();
			}
			count++;
			if(count == 5){
				count = 0;
				updateGui();
			}
		}

		private void updateGui() {
            int color = dyeContainer.tileEntity.color;
            red = (color >> 16) & 0xFF;
            green = (color >> 8) & 0xFF;
            blue = color & 0xFF;
            
		}

		private int getColor() {
			String hex = getTwoCharHex(red) + getTwoCharHex(green) + getTwoCharHex(blue);
			return Integer.parseInt(hex, 16);
		}
		
	    public String getTwoCharHex(int num){
	    	if(num < 16){
	    		return "0" + Integer.toHexString(num);
	    	} else {
	    		return Integer.toHexString(num);
	    	}
	    }

		@Override
		protected void mouseMovedOrUp(int par1, int par2, int par3) {
			super.mouseMovedOrUp(par1, par2, par3);
			id = -1;
		}

        @Override
        protected void drawGuiContainerForegroundLayer(int param1, int param2) {
                //string, x, y, color
                fontRenderer.drawString(LocalizationHelper.getLocalizedString("block.dye_machine"), 8, 6, 4210752);
                fontRenderer.drawString(String.valueOf(this.red), 18, 22, 16711680);
                fontRenderer.drawString(String.valueOf(this.green), 18, 32, 65280);
                fontRenderer.drawString(String.valueOf(this.blue), 18, 42, 255);
                fontRenderer.drawString(String.valueOf(this.dyeContainer.tileEntity.getDye()), 111, 69, 16777215);
                //field.drawTextBox();
                //draws "Inventory" or your regional equivalent
                fontRenderer.drawString(StatCollector.translateToLocal("container.inventory"), 8, ySize - 96 + 2, 4210752);
        }

        @Override
        protected void drawGuiContainerBackgroundLayer(float par1, int par2,
                        int par3) {
                GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                this.mc.renderEngine.bindTexture("/mods/"+RGBBlock.MODID+"/textures/gui/dyemachine.png");
                int x = (width - xSize) / 2;
                int y = (height - ySize) / 2;
                this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
                
    			double fractionOfMax = (double) this.dyeContainer.tileEntity.getDye() / this.dyeContainer.tileEntity.getMaxDye();
    			int i1 = (int) (fractionOfMax * 62);
    			this.drawTexturedModalRect(x + 108, y + 67, xSize + 1, 1, i1, 13);
    			
    			fractionOfMax = (double)this.dyeContainer.tileEntity.getTranferTicks() / this.dyeContainer.tileEntity.getMaxTicks();
    			i1 = (int) (fractionOfMax * 23);
    			this.drawTexturedModalRect(x + 80, y + 34, xSize, 14, i1, 16);
        }
        
        @Override
        public void onGuiClosed(){
            if (this.mc.thePlayer != null)
            {
                this.inventorySlots.onCraftGuiClosed(this.mc.thePlayer);
            }
        }

}