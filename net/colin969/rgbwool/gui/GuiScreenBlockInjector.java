package net.colin969.rgbwool.gui;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.colin969.rgbwool.ModProperties;
import net.colin969.rgbwool.RGBBlock;
import net.colin969.rgbwool.container.ContainerDyeMachine;
import net.colin969.rgbwool.entity.TileEntityDyeMachine;
import net.colin969.rgbwool.lang.LocalizationHelper;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSmallButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.SideOnly;
import cpw.mods.fml.relauncher.Side;

@SideOnly(Side.CLIENT)
public class GuiScreenBlockInjector extends GuiScreen {
	
	private static final int imageWidth = 176;
	private static final int imageHeight = 106;
	private int red = 0;
	private int green = 0;
	private int blue = 0;
	private int mode = 0;
	private int PosX;
	private int PosY;
	private int PosZ;
	private int storedDye;
	private boolean openLibrary = false;
	
	public static RenderItem renderItem = new RenderItem();
    private EntityPlayer editingPlayer;
    private ItemStack itemStackInjector;
    
    public GuiScreenBlockInjector(EntityPlayer par1EntityPlayer, int x, int y, int z) {
    	PosX = x;
    	PosY = y;
    	PosZ = z;
    	editingPlayer = par1EntityPlayer;
    	itemStackInjector = editingPlayer.getCurrentEquippedItem();
    	storedDye = itemStackInjector.getMaxDamage() - itemStackInjector.getItemDamage();
    	setColorAndMode(itemStackInjector);
    }
    
    @Override
    public void onGuiClosed(){
   		ByteArrayOutputStream bos = new ByteArrayOutputStream(8);
   		DataOutputStream os = new DataOutputStream(bos);
   		try {
   			os.writeInt(getColor());
   			os.writeInt(mode);
   		} catch (IOException e) {
   			e.printStackTrace();
    	}

    	Packet250CustomPayload packet = new Packet250CustomPayload();
   		packet.channel = "WoolInjectorCI";
   		packet.data = bos.toByteArray();
    	packet.length = bos.size();
    	
    	PacketDispatcher.sendPacketToServer(packet);
    	//System.out.println("Packet Sent");
    	try{
    	updateRecentColors();
    	} catch (Exception e){
    		e.printStackTrace();
    	}
    }
    
	private int getColor() {
		String hex = getTwoCharHex(red) + getTwoCharHex(green) + getTwoCharHex(blue);
		return Integer.parseInt(hex, 16);
	}

	private void setColorAndMode(ItemStack item) {
		NBTTagCompound nbt = item.getTagCompound();
		if(nbt != null){
			int color = nbt.getInteger("color");
			red = (color >> 16) & 0xFF;
			green = (color >> 8) & 0xFF;
			blue = color & 0xFF;
			mode = nbt.getInteger("Mode");
		} else {
        	nbt = new NBTTagCompound();
        	nbt.setInteger("color", getColor());
        	nbt.setInteger("Mode", 0);
        	itemStackInjector.setTagCompound(nbt);
	        editingPlayer.getCurrentEquippedItem().setTagCompound(nbt);
        }
	}

	int id = -1;
	protected void actionPerformed(GuiButton guibutton) {
		
        id = guibutton.id;
        
        if(id == 8){
    		if(mode == 0)
    			mode = 1;
    		else
    			mode = 0;
    		remakeButton();
        }
        
        if(id >= 9 && id <= 18){
        	setColorFromRecentIndex(id - 8);
        }
        
        if(id == 19){
	        	mc.displayGuiScreen(new GuiScreenLibrary(editingPlayer, PosX, PosY, PosZ));
        }
        
        if(id == 20){
        	try {
	        	ModProperties.addCustomColor(Integer.parseInt(getTwoCharHex(red) + getTwoCharHex(green) + getTwoCharHex(blue), 16));
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
        
        if(id == 7){
        	editingPlayer.closeScreen();
        }
        
        if(id == 21){
        	mc.displayGuiScreen(new GuiColorSelection(editingPlayer, PosX, PosY, PosZ, false, this.getColor()));
        }
	}

	private void remakeButton() {
        int x = (width - imageWidth) / 2;
        int y = (height - imageHeight) / 2;
		buttonList.clear();
		String modeStrT;
        if(mode == 0)
        	modeStrT = LocalizationHelper.getLocalizedString("string.block");
        else
        	modeStrT = LocalizationHelper.getLocalizedString("string.side");
		buttonList.add(new GuiSmallButton(1, x + imageWidth/8 * 3 + 1, y + 17, 10, 10, "+"));
		buttonList.add(new GuiSmallButton(2, x + imageWidth/8 * 2 - 13, y + 17, 10, 10, "-"));
		buttonList.add(new GuiSmallButton(3, x + imageWidth/8 * 3 + 1, y + 27, 10, 10, "+"));
		buttonList.add(new GuiSmallButton(4, x + imageWidth/8 * 2 - 13, y + 27, 10, 10, "-"));
		buttonList.add(new GuiSmallButton(5, x + imageWidth/8 * 3 + 1, y + 37, 10, 10, "+"));
		buttonList.add(new GuiSmallButton(6, x + imageWidth/8 * 2 - 13, y + 37, 10, 10, "-"));
		//buttonList.add(new GuiSmallButton(7, x + imageWidth/8 * 7 + 7, y + 5, 10, 10,"x"));
		buttonList.add(new GuiSmallButton(8, x + imageWidth/8 * 5 + 18, y + 68, 40, 10, modeStrT));
		for(int i = 1;i <= 10;i++){
			buttonList.add(new GuiColorButton(8 + i, x - 7 + (i * 16), y + 83, 15, 15, ""));
		}
		buttonList.add(new GuiSmallButton(19, x - 7 + 16, y + 68, 42, 10, "Library"));
		buttonList.add(new GuiSmallButton(20, x - 7 + 60, y + 68, 58, 10, "Save Color"));
		buttonList.add(new GuiSmallButton(21, x + imageWidth/8 * 2 - 13, y + 49, 47, 10, "Pick"));
	}

	@Override
	protected void mouseMovedOrUp(int par1, int par2, int par3) {
		super.mouseMovedOrUp(par1, par2, par3);
		id = -1;
	}
	
	@Override
	public void updateScreen() {
		super.updateScreen();
		if (id>=0&&id<=6) {

            switch(id) {
            case 1:
                red += 1;
                break;
            case 2:
            	red -= 1;
                break;
            case 3:
            	green += 1;
                    break;
            case 4:
            	green -= 1;
        		break;
            case 5:
            	blue += 1;
                break;
            case 6:
            	blue -= 1;
            	break;
            }
            this.red = this.red & 0xFF;
            this.blue = this.blue & 0xFF;
            this.green = this.green & 0xFF;
		}
	}
    
    @Override
	public void initGui() {
		super.initGui();
		if(ModProperties.getTransferMode().equals("setColorFromLibrary")){
			int color = ModProperties.getTransferColor();
			
	        int f = color >> 16 & 255;
	        int f1 = color >> 8 & 255;
	        int f2 = color & 255;
	        
	        red = f;
	        green = f1;
	        blue = f2;
	        
	        try {
				ModProperties.setTransferMode("none");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		remakeButton();
	}
 
    @Override
    public void drawScreen(int par1, int par2, float par3){
    			int x = (width - imageWidth) / 2;
    			int y = (height - imageHeight) / 2;
    			
    			for(int i = 1; i <= 10; i++){
    				drawColorBox(x,y,i);
    			}
    	    	
    			GL11.glColor4f(((float)red) / 256F ,((float)green) / 256F ,((float)blue) / 256F, 1.0F);
    			this.mc.renderEngine.bindTexture("/mods/"+RGBBlock.MODID+"/textures/gui/WoolInjectorGUIInvert.png");
                this.drawTexturedModalRect(x, y, 0, 0, imageWidth, imageHeight);
    			
                GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
    			this.mc.renderEngine.bindTexture("/mods/"+RGBBlock.MODID+"/textures/gui/WoolInjectorGUI.png");
    			this.drawTexturedModalRect(x, y, 0, 0, imageWidth, imageHeight);
    			
    			double fractionOfMax = (double) storedDye / (double) itemStackInjector.getMaxDamage();
    			int i1 = (int) (fractionOfMax * 62);
    			this.drawTexturedModalRect(x + 103, y + 53, imageWidth + 1, 1, i1, 13);
    			

    			fontRenderer.drawString(String.valueOf(storedDye), x + 106, y + 55, 16777215);
                fontRenderer.drawString(LocalizationHelper.getLocalizedString("gui.block_injector"), x + imageWidth/6, y + 6, 4210752);
                fontRenderer.drawString(String.valueOf(this.red), x + 5 * imageWidth/18 - 4, y + 19, 16711680);
                fontRenderer.drawString(String.valueOf(this.green), x + 5 * imageWidth/18 - 4, y + 29, 65280);
                fontRenderer.drawString(String.valueOf(this.blue), x + 5 * imageWidth/18 - 4, y + 39, 255);
                
    			super.drawScreen(par1, par2, par3);
    			
    }

	private void drawColorBox(int x, int y, int boxIndex) {
		
		int color = ModProperties.getRecentColor(boxIndex);
        float f = (float)(color >> 16 & 255) / 255.0F;
        float f1 = (float)(color >> 8 & 255) / 255.0F;
        float f2 = (float)(color & 255) / 255.0F;
        
        GL11.glColor4f(f,f1,f2, 1.0F);
		this.mc.renderEngine.bindTexture("/mods/"+RGBBlock.MODID+"/textures/gui/ColorBox.png");
		this.drawTexturedModalRect(x - 16 + (boxIndex * 16), y, 0, 0, imageWidth, imageHeight);
		
	}
	
	private void updateRecentColors() throws Exception{
		String hex = getTwoCharHex(red) + getTwoCharHex(green) + getTwoCharHex(blue);
		int color = Integer.parseInt(hex, 16);
		boolean flag = false;
		
		for(int i = 1;i <= 10;i++){
			if(color == ModProperties.getRecentColor(i))
				flag = true;
		}
		
		if(!flag)
			ModProperties.addRecentColor(color);
	}
	
	private void setColorFromRecentIndex(int boxIndex){
		int color = ModProperties.getRecentColor(boxIndex);
		
        int f = color >> 16 & 255;
        int f1 = color >> 8 & 255;
        int f2 = color & 255;
        
        red = f;
        green = f1;
        blue = f2;
	}
	
    public String getTwoCharHex(int num){
    	if(num < 16){
    		return "0" + Integer.toHexString(num);
    	} else {
    		return Integer.toHexString(num);
    	}
    }
}