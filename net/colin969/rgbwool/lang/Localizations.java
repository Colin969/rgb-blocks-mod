package net.colin969.rgbwool.lang;

import net.colin969.rgbwool.RGBBlock;

public class Localizations {

    private static final String LANG_RESOURCE_LOCATION = "/mods/"+RGBBlock.MODID+"/lang/";
    
    public static String[] localeFiles = { LANG_RESOURCE_LOCATION + "en_US.xml" , LANG_RESOURCE_LOCATION + "es_ES.xml"};

}