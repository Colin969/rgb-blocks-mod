package net.colin969.rgbwool.lang;

import cpw.mods.fml.common.registry.LanguageRegistry;

public class LocalizationHandler {

    public static void loadLanguages() {

        for (String localizationFile : Localizations.localeFiles) {
            LanguageRegistry.instance().loadLocalization(localizationFile, LocalizationHelper.getLocaleFromFileName(localizationFile), LocalizationHelper.isXMLLanguageFile(localizationFile));
        }
    }

}